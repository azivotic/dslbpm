package com.azivotic.dsl.bpm.core.diagram.style;

import org.eclipse.graphiti.util.IPredefinedRenderingStyle;

public interface LaneRenderingStyle extends IPredefinedRenderingStyle {

	public static final String LANE_STYLE_ID = "lane-style-id";
	
}
