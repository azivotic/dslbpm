package com.azivotic.dsl.bpm.core.diagram;

import org.eclipse.graphiti.dt.AbstractDiagramTypeProvider;
import org.eclipse.graphiti.tb.IToolBehaviorProvider;

/**
 * @author azivotic
 * 
 * Provides diagram type for domain specific language for business process
 */

public class BpmDiagramTypeProvider extends AbstractDiagramTypeProvider{

	private IToolBehaviorProvider[] toolBehaviorProviders;

	public BpmDiagramTypeProvider(){
		super();
		setFeatureProvider(new BpmFeatureProvider(this));
	}

	@Override
	public IToolBehaviorProvider[] getAvailableToolBehaviorProviders() {
		if (toolBehaviorProviders == null) {
			toolBehaviorProviders = new IToolBehaviorProvider[] { new BpmToolBehaviorProvider(this) };
		}
		return toolBehaviorProviders;
	}
}
