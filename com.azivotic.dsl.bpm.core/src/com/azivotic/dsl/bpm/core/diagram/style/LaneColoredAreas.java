package com.azivotic.dsl.bpm.core.diagram.style;

import org.eclipse.emf.common.util.EList;
import org.eclipse.graphiti.mm.algorithms.styles.AdaptedGradientColoredAreas;
import org.eclipse.graphiti.mm.algorithms.styles.GradientColoredArea;
import org.eclipse.graphiti.mm.algorithms.styles.GradientColoredAreas;
import org.eclipse.graphiti.mm.algorithms.styles.LocationType;
import org.eclipse.graphiti.mm.algorithms.styles.StylesFactory;
import org.eclipse.graphiti.util.IGradientType;
import org.eclipse.graphiti.util.IPredefinedRenderingStyle;
import org.eclipse.graphiti.util.PredefinedColoredAreas;

/**
 * Color styles for lane in different context situations(default, selected, hover...)
 * Help site for colors gradients http://www.maxi-pedia.com/rgb+color+picker
 * @author azivotic
 *
 */

public class LaneColoredAreas extends PredefinedColoredAreas implements LaneRenderingStyle {

	private static GradientColoredAreas getLaneDefaultAreas() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE.createGradientColoredAreas();
		final EList<GradientColoredArea> gcas = gradientColoredAreas.getGradientColor();
		
		addGradientColoredArea(gcas, "FFFFFF", 0,
				LocationType.LOCATION_TYPE_ABSOLUTE_START, "FFFFFF", 0,
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		
		gradientColoredAreas.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT);
		
		return gradientColoredAreas;
	}

	private static GradientColoredAreas getLanePrimarySelectedAreas() {
        final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE.createGradientColoredAreas();
        gradientColoredAreas.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED);
        final EList<GradientColoredArea> gcas = gradientColoredAreas.getGradientColor();
 
        addGradientColoredArea(gcas, "FFFF66", 0,
				LocationType.LOCATION_TYPE_ABSOLUTE_START, "FFFF66", 0,
				LocationType.LOCATION_TYPE_ABSOLUTE_END);

		
		return gradientColoredAreas;
      }
	
	private static GradientColoredAreas getLaneSecondarySelected() {
		final GradientColoredAreas gradientColoredAreas = StylesFactory.eINSTANCE.createGradientColoredAreas();
		gradientColoredAreas.setStyleAdaption(IPredefinedRenderingStyle.STYLE_ADAPTATION_ACTION_ALLOWED);
		final EList<GradientColoredArea> gcas = gradientColoredAreas.getGradientColor();
		
		addGradientColoredArea(gcas, "E7E7F3", 0,
				LocationType.LOCATION_TYPE_ABSOLUTE_START, "E7E7F3", 0,
				LocationType.LOCATION_TYPE_ABSOLUTE_END);
		
		return gradientColoredAreas;
	}


	public static AdaptedGradientColoredAreas getLaneAdaptions() {
		final AdaptedGradientColoredAreas agca = StylesFactory.eINSTANCE.createAdaptedGradientColoredAreas();
		agca.setDefinedStyleId(LANE_STYLE_ID);
		agca.setGradientType(IGradientType.HORIZONTAL);
		
		agca.getAdaptedGradientColoredAreas().add(IPredefinedRenderingStyle.STYLE_ADAPTATION_DEFAULT,
				getLaneDefaultAreas());
		
		agca.getAdaptedGradientColoredAreas().add(IPredefinedRenderingStyle.STYLE_ADAPTATION_PRIMARY_SELECTED,
				getLanePrimarySelectedAreas());
		
		agca.getAdaptedGradientColoredAreas().add(IPredefinedRenderingStyle.STYLE_ADAPTATION_SECONDARY_SELECTED,
				getLaneSecondarySelected());
		
		return agca;
	}
}
