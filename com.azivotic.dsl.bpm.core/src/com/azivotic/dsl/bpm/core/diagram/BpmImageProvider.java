package com.azivotic.dsl.bpm.core.diagram;

import org.eclipse.graphiti.ui.platform.AbstractImageProvider;

/**
 * 
 * @author azivotic
 * Provide platform specific images based on identifier
 */

public class BpmImageProvider extends AbstractImageProvider {

	// The prefix for all identifiers of this image provider
	protected static final String PREFIX =  "com.azivotic.dsl.bpm.core.";

	public static final String IMG_START_EVENT= PREFIX + "start";
	public static final String IMG_STOP_EVENT= PREFIX + "stop";
	public static final String IMG_SERVICE_TASK= PREFIX + "service_task";
	public static final String IMG_USER_TASK= PREFIX + "user_task";
	public static final String IMG_PARALLEL_TASK= PREFIX + "parallel_task";
	public static final String IMG_LOOP_TASK= PREFIX + "loop_task";

	public static final String IMG_PALETTE_START_EVENT= PREFIX + "palette_start";
	public static final String IMG_PALETTE_STOP_EVENT= PREFIX + "palette_stop";
	public static final String IMG_PALETTE_TASK = PREFIX + "palette_task";
	public static final String IMG_PALETTE_GATEWAY = PREFIX + "palette_gateway";
	public static final String IMG_PALETTE_CONNECION = PREFIX + "palette_connection";
	public static final String IMG_PALETTE_LANE = PREFIX + "palette_lane";
	public static final String IMG_PALETTE_POOL = PREFIX + "palette_pool";
	public static final String IMG_PALETTE_COMMENT = PREFIX + "palette_comment";



	@Override
	protected void addAvailableImages() {
		// register the path for each image identifier
		addImageFilePath(IMG_START_EVENT, "icons/process/start.png");
		addImageFilePath(IMG_STOP_EVENT, "icons/process/stop.png");
		addImageFilePath(IMG_SERVICE_TASK, "icons/app/process/service-task.png");
		addImageFilePath(IMG_USER_TASK, "icons/app/process/user-task.png");
		addImageFilePath(IMG_PARALLEL_TASK, "icons/app/process/parallel-task.png");
		addImageFilePath(IMG_LOOP_TASK, "icons/app/process/loop-task.png");

		addImageFilePath(IMG_PALETTE_START_EVENT, "icons/app/palette/start.png");
		addImageFilePath(IMG_PALETTE_STOP_EVENT, "icons/app/palette/stop.png");
		addImageFilePath(IMG_PALETTE_TASK, "icons/app/palette/task.png");
		addImageFilePath(IMG_PALETTE_GATEWAY, "icons/app/palette/gateway.png");
		addImageFilePath(IMG_PALETTE_CONNECION, "icons/app/palette/connection.png");
		addImageFilePath(IMG_PALETTE_LANE, "icons/app/palette/lane.png");
		addImageFilePath(IMG_PALETTE_POOL, "icons/app/palette/pool.png");
		addImageFilePath(IMG_PALETTE_COMMENT, "icons/app/palette/comment.png");
	}
}