package com.azivotic.dsl.bpm.core.diagram;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.IAddFeature;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.IDeleteFeature;
import org.eclipse.graphiti.features.IDirectEditingFeature;
import org.eclipse.graphiti.features.ILayoutFeature;
import org.eclipse.graphiti.features.IMoveShapeFeature;
import org.eclipse.graphiti.features.IReconnectionFeature;
import org.eclipse.graphiti.features.IResizeShapeFeature;
import org.eclipse.graphiti.features.IUpdateFeature;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.context.IDeleteContext;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.context.IReconnectionContext;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.custom.ICustomFeature;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.ui.features.DefaultFeatureProvider;

import com.azivotic.dsl.bpm.core.features.AddCommentFeature;
import com.azivotic.dsl.bpm.core.features.AddEndEventFeature;
import com.azivotic.dsl.bpm.core.features.AddGatewayFeature;
import com.azivotic.dsl.bpm.core.features.AddLaneFeature;
import com.azivotic.dsl.bpm.core.features.AddSequenceFlowFeature;
import com.azivotic.dsl.bpm.core.features.AddStartEventFeature;
import com.azivotic.dsl.bpm.core.features.AddTaskFeature;
import com.azivotic.dsl.bpm.core.features.CreateCommentFeature;
import com.azivotic.dsl.bpm.core.features.CreateEndEventFeature;
import com.azivotic.dsl.bpm.core.features.CreateGatewayFeature;
import com.azivotic.dsl.bpm.core.features.CreateLaneFeature;
import com.azivotic.dsl.bpm.core.features.CreateSequenceFlowFeature;
import com.azivotic.dsl.bpm.core.features.CreateStartEventFeature;
import com.azivotic.dsl.bpm.core.features.CreateTaskFeature;
import com.azivotic.dsl.bpm.core.features.diagram.ChangeColorFeature;
import com.azivotic.dsl.bpm.core.features.diagram.DeleteLaneFeature;
import com.azivotic.dsl.bpm.core.features.diagram.DeleteProcessNodeFeature;
import com.azivotic.dsl.bpm.core.features.diagram.DirectEditFeature;
import com.azivotic.dsl.bpm.core.features.diagram.LayoutCommentFeature;
import com.azivotic.dsl.bpm.core.features.diagram.LayoutLaneFeature;
import com.azivotic.dsl.bpm.core.features.diagram.LayoutTaskFeature;
import com.azivotic.dsl.bpm.core.features.diagram.MoveCommentFeature;
import com.azivotic.dsl.bpm.core.features.diagram.MoveProcessNodeFeature;
import com.azivotic.dsl.bpm.core.features.diagram.ReconnectionFeature;
import com.azivotic.dsl.bpm.core.features.diagram.RenameFeature;
import com.azivotic.dsl.bpm.core.features.diagram.ResizeFeature;
import com.azivotic.dsl.bpm.core.features.diagram.UpdateFeature;
import com.azivotic.dsl.bpm.metamodel.Comment;
import com.azivotic.dsl.bpm.metamodel.EndEvent;
import com.azivotic.dsl.bpm.metamodel.Event;
import com.azivotic.dsl.bpm.metamodel.Gateway;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.ProcessNode;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.StartEvent;
import com.azivotic.dsl.bpm.metamodel.Task;

/**
 * @author azivotic
 * 
 * Provides features for specific diagram type(DSL for BPM)
 */
public class BpmFeatureProvider extends DefaultFeatureProvider {

	public BpmFeatureProvider(IDiagramTypeProvider dtp) {
		super(dtp);
	}

	@Override
	public IAddFeature getAddFeature(IAddContext context) {
		if (context.getNewObject() instanceof Task) {
			return new AddTaskFeature(this);
		} else if (context.getNewObject() instanceof StartEvent) {
			return new AddStartEventFeature(this);
		} else if (context.getNewObject() instanceof EndEvent) {
			return new AddEndEventFeature(this);
		} else if (context.getNewObject() instanceof Gateway) {
			return new AddGatewayFeature(this);
		} else if (context.getNewObject() instanceof SequenceFlow) {
			return new AddSequenceFlowFeature(this);
		} else if (context.getNewObject() instanceof Lane) {
			return new AddLaneFeature(this);
		} else if (context.getNewObject() instanceof Comment) {
			return new AddCommentFeature(this);
		} 
		return super.getAddFeature(context);
	}

	@Override
	public ICreateFeature[] getCreateFeatures() {
		return new ICreateFeature[] { new CreateStartEventFeature(this), 
				new CreateTaskFeature(this), 
				new CreateGatewayFeature(this),
				new CreateEndEventFeature(this),
				new CreateLaneFeature(this),
				new CreateCommentFeature(this) };
	}

	@Override
	public ICreateConnectionFeature[] getCreateConnectionFeatures() {
		return new ICreateConnectionFeature[] { 
				new CreateSequenceFlowFeature (this) };
	}

	@Override
	public IResizeShapeFeature getResizeShapeFeature(IResizeShapeContext context) {
		Shape shape = context.getShape();
		Object bo = getBusinessObjectForPictogramElement(shape);
		if (bo instanceof Event || bo instanceof Gateway) {
			return new ResizeFeature(this);
		}
		return super.getResizeShapeFeature(context);
	}

	@Override
	public ICustomFeature[] getCustomFeatures(ICustomContext context) {
		return new ICustomFeature[] { new RenameFeature(this), 
				new ChangeColorFeature(this) };
	}

	@Override
	public IDirectEditingFeature getDirectEditingFeature(IDirectEditingContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);
		if (bo instanceof Task || bo instanceof SequenceFlow || bo instanceof Comment) {
			return new DirectEditFeature(this);
		}
		return super.getDirectEditingFeature(context);
	}

	@Override
	public IUpdateFeature getUpdateFeature(IUpdateContext context) {
		PictogramElement pictogramElement = context.getPictogramElement();
		if (pictogramElement instanceof ContainerShape || pictogramElement instanceof Connection) {
			Object bo = getBusinessObjectForPictogramElement(pictogramElement);
			if (bo instanceof Task || bo instanceof SequenceFlow || bo instanceof Lane || bo instanceof Comment) {
				return new UpdateFeature(this);
			}
		}
		return super.getUpdateFeature(context);
	}

	@Override
	public ILayoutFeature getLayoutFeature(ILayoutContext context) {
		PictogramElement pictogramElement = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pictogramElement);
		if (bo instanceof Task) {
			return new LayoutTaskFeature(this);
		} else if(bo instanceof Lane){
			return new LayoutLaneFeature(this);
		} else if(bo instanceof Comment){
			return new LayoutCommentFeature(this);
		}
		return super.getLayoutFeature(context);
	}

	@Override
	public IDeleteFeature getDeleteFeature(IDeleteContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);
		if (bo instanceof ProcessNode) {
			return new DeleteProcessNodeFeature(this);
		} else if (bo instanceof Lane){
			return new DeleteLaneFeature(this);
		} else
			return super.getDeleteFeature(context);
	}

	@Override
	public IMoveShapeFeature getMoveShapeFeature(IMoveShapeContext context) {
		Shape shape = context.getShape();
		Object bo = getBusinessObjectForPictogramElement(shape);
		if (bo instanceof ProcessNode) {
			return new MoveProcessNodeFeature(this);
		} else if(bo instanceof Comment){
			return new MoveCommentFeature(this);
		}
		return super.getMoveShapeFeature(context);
	}

	@Override
	public IReconnectionFeature getReconnectionFeature(IReconnectionContext context) {
		return new ReconnectionFeature(this);
	}
} 
