package com.azivotic.dsl.bpm.core.diagram;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.graphiti.dt.IDiagramTypeProvider;
import org.eclipse.graphiti.features.ICreateConnectionFeature;
import org.eclipse.graphiti.features.ICreateFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IPictogramElementContext;
import org.eclipse.graphiti.features.context.impl.CreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.CreateContext;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.palette.IPaletteCompartmentEntry;
import org.eclipse.graphiti.palette.impl.ConnectionCreationToolEntry;
import org.eclipse.graphiti.palette.impl.ObjectCreationToolEntry;
import org.eclipse.graphiti.palette.impl.PaletteCompartmentEntry;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.tb.ContextButtonEntry;
import org.eclipse.graphiti.tb.DefaultToolBehaviorProvider;
import org.eclipse.graphiti.tb.IContextButtonPadData;

import com.azivotic.dsl.bpm.core.features.CreateCommentFeature;
import com.azivotic.dsl.bpm.core.features.CreateEndEventFeature;
import com.azivotic.dsl.bpm.core.features.CreateGatewayFeature;
import com.azivotic.dsl.bpm.core.features.CreateLaneFeature;
import com.azivotic.dsl.bpm.core.features.CreateTaskFeature;
import com.azivotic.dsl.bpm.metamodel.Activity;
import com.azivotic.dsl.bpm.metamodel.Gateway;
import com.azivotic.dsl.bpm.metamodel.StartEvent;

public class BpmToolBehaviorProvider extends DefaultToolBehaviorProvider{

	public BpmToolBehaviorProvider(IDiagramTypeProvider diagramTypeProvider) {
		super(diagramTypeProvider);
	}

	@Override
	public IPaletteCompartmentEntry[] getPalette() {
	    List<IPaletteCompartmentEntry> ret = new ArrayList<IPaletteCompartmentEntry>();
	 
	    PaletteCompartmentEntry connectionsEntry = new PaletteCompartmentEntry("Connections", null);
	    ret.add(connectionsEntry);
	 
	    IFeatureProvider featureProvider = getFeatureProvider();
	    
	    // add all create-connection-features to the new stack-entry
	    ICreateConnectionFeature[] createConnectionFeatures = featureProvider.getCreateConnectionFeatures();
	    for (ICreateConnectionFeature cf : createConnectionFeatures) {
	        ConnectionCreationToolEntry connectionCreationToolEntry = 
	            new ConnectionCreationToolEntry(cf.getCreateName(), cf
	              .getCreateDescription(), cf.getCreateImageId(),
	                cf.getCreateLargeImageId());
	                    connectionCreationToolEntry.addCreateConnectionFeature(cf);
	                    connectionsEntry.addToolEntry(connectionCreationToolEntry);
	    }
	 
	    // add all create-features to the new stack-entry
	    PaletteCompartmentEntry objectsEntry = new PaletteCompartmentEntry("Elements", null);
	    ret.add(objectsEntry);
	    PaletteCompartmentEntry swimlaneEntry = new PaletteCompartmentEntry("Swimlanes", null);
	    ret.add(swimlaneEntry);
	    PaletteCompartmentEntry dataEntry = new PaletteCompartmentEntry("Data", null);
	    ret.add(dataEntry);
	    
	    ICreateFeature[] createFeatures = featureProvider.getCreateFeatures();
	    for (ICreateFeature cf : createFeatures) {
	    	
	        ObjectCreationToolEntry objectCreationToolEntry = 
	               new ObjectCreationToolEntry(cf.getCreateName(),
	                 cf.getCreateDescription(), cf.getCreateImageId(),
	                    cf.getCreateLargeImageId(), cf);
	        if(cf instanceof CreateLaneFeature)
	        	swimlaneEntry.addToolEntry(objectCreationToolEntry);
	        else if(cf instanceof CreateCommentFeature)
	        	dataEntry.addToolEntry(objectCreationToolEntry);
	        else
	        	objectsEntry.addToolEntry(objectCreationToolEntry);
	    }

	    return ret.toArray(new IPaletteCompartmentEntry[ret.size()]);
	}
	
	@Override
	public IContextButtonPadData getContextButtonPad(IPictogramElementContext context) {
	    IContextButtonPadData data = super.getContextButtonPad(context);
	    PictogramElement pe = context.getPictogramElement();
	    
	    // a) create new CreateConnectionContext
	    CreateConnectionContext ccc = new CreateConnectionContext();
	    ccc.setSourcePictogramElement(pe);
	    Anchor anchor = null;
	    if (pe instanceof Anchor) {
	        anchor = (Anchor) pe;
	    } else if (pe instanceof AnchorContainer) {
	        // assume, that our shapes always have chopbox anchors
	        anchor = Graphiti.getPeService().getChopboxAnchor((AnchorContainer) pe);
	    }
	    ccc.setSourceAnchor(anchor);
	        
	    // b) create context button and add all applicable features
	    ContextButtonEntry button = new ContextButtonEntry(null, context);
	    button.setText("Create connection");
	    button.setDescription("Create new connection between two elements");
	    button.setIconId(BpmImageProvider.IMG_PALETTE_CONNECION);
	    ICreateConnectionFeature[] features = getFeatureProvider().getCreateConnectionFeatures();
	    for (ICreateConnectionFeature feature : features) {
	        if (feature.isAvailable(ccc) && feature.canStartConnection(ccc))
	            button.addDragAndDropFeature(feature);
	    }
	 
	    // c) add context button, if it contains at least one feature
	    if (button.getDragAndDropFeatures().size() > 0) {
	       data.getDomainSpecificContextButtons().add(button);
	    }
	 
	    //Add Sequence flow for flow nodes context buttons to connect flow objects
	    CreateConnectionContext connectionContext = new CreateConnectionContext();
	    connectionContext.setSourcePictogramElement(pe);
	    Anchor connectionAnchor = null;
	    if (pe instanceof Anchor) {
	      connectionAnchor = (Anchor) pe;
	    } else if (pe instanceof AnchorContainer) {
	      connectionAnchor = Graphiti.getPeService().getChopboxAnchor((AnchorContainer) pe);
	    }
	    connectionContext.setSourceAnchor(connectionAnchor);

	    if (pe.eContainer() instanceof ContainerShape == false) {
	      return data;
	    }
	    
	    CreateContext newElementContext = new CreateContext();
	    newElementContext.setTargetContainer((ContainerShape) pe.eContainer());
	    //Set data about current element so we can create next element beside this element
	    newElementContext.putProperty("xcoordinate", pe.getGraphicsAlgorithm().getX());
	    newElementContext.putProperty("ycoordinate", pe.getGraphicsAlgorithm().getY());
	    newElementContext.putProperty("contextButtonConnection", connectionContext);
	    
	    Object bo = getFeatureProvider().getBusinessObjectForPictogramElement(pe);
	    if (bo instanceof StartEvent || bo instanceof Activity  || bo instanceof Gateway) {
	    	
	      CreateTaskFeature taskfeature = new CreateTaskFeature(getFeatureProvider());
	      ContextButtonEntry newTaskButton = new ContextButtonEntry(taskfeature, newElementContext);
	      newTaskButton.setText("New Task");
	      newTaskButton.setDescription("Create a new task");
	      newTaskButton.setIconId(BpmImageProvider.IMG_PALETTE_TASK);
	      data.getDomainSpecificContextButtons().add(newTaskButton);

	      CreateGatewayFeature exclusiveGatewayFeature = new CreateGatewayFeature(getFeatureProvider());
	      ContextButtonEntry newGatewayButton = new ContextButtonEntry(exclusiveGatewayFeature, newElementContext);
	      newGatewayButton.setText("New Gateway");
	      newGatewayButton.setDescription("Create a new gateway");
	      newGatewayButton.setIconId(BpmImageProvider.IMG_PALETTE_GATEWAY);
	      data.getDomainSpecificContextButtons().add(newGatewayButton);

	      CreateEndEventFeature endFeature = new CreateEndEventFeature(getFeatureProvider());
	      ContextButtonEntry newEndButton = new ContextButtonEntry(endFeature, newElementContext);
	      newEndButton.setText("New End Event");
	      newEndButton.setDescription("Create a new end event");
	      newEndButton.setIconId(BpmImageProvider.IMG_PALETTE_STOP_EVENT);
	      data.getDomainSpecificContextButtons().add(newEndButton);
	    }
	    
	    return data;
	}
	
}
