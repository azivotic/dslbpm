package com.azivotic.dsl.bpm.core.util;

/**
 * Help class with description for each element
 * to be presented in properties section
 * TODO See how to transfer this to xcore meta-model
 * @author azivotic
 *
 */
public class HelpUtil {

	public static final String START_EVENT_HELP = "\n  An event is something that happens during the course of "
			+ "the process, affecting the process flow and normally has a trigger or result.\n\n  "
			+ "Each business process has some form of initiating event. In BPMN terminology, "
			+ "it is called a Start Event. \n  It is represented with green circle with green filled triangle"
			+ "inside. \uFFFC\n\n  As its name suggests, it represents the starting "
			+ "point of a process. These events do not have any incoming sequence flow.";

	public static final String END_EVENT_HELP = "\n  In business process modeling, every process has an ending. "
			+ "Every path in every process must lead to an end event.\n\n  Every process "
			+ "starts with a start event, moves through activities and gateways, and then "
			+ "reaches an end.\n  End event indicates where a process will end. "
			+ "It is represented with red circle with red filled square "
			+ "inside. \uFFFC";

	public static final String GATEWAY_HELP = "\n  A gateway is represented with a diamond shape \uFFFC and determines forking and merging "
			+ "of paths, depending on the conditions expressed.  They are responsible for controlling how "
			+ "\n\n  a business process flows. Gateway is where conditions are evaluated and the decision is made. Different types of gateways:"
			+ "\n\n\t\uFFFC\tExclusive - Used to create alternative flows in a process because only one of the "
			+ "paths can be taken, it is called exclusive.\n\n\t\uFFFC\tInclusive - Used to create alternative "
			+ "flows where all paths are evaluated.\n\n\t\uFFFC\tParallel - Used to create parallel paths without "
			+ "evaluating any conditions.";

	public static final String SEQUENCE_FLOW_HELP = "\n  Sequence flow is used to connect flow elements. "
			+ "It is shown in solid line with an arrowhead. \uFFFC It shows the order of flow elements. \n\n  " + 
			"You can only use sequence flow to connect flow elements within the same lane/process diagram, or across lanes or process diagrams.\n\n  "
			+ "Gateways can have default sequence flows. \uFFFC";

	public static final String TASK_HELP = "\n  A task is a unit of work � the job to be performed. "
			+ "It is an atomic activity within a process flow.  Task is represnted with a blue rounded rectangle.  \uFFFC "
			+ "\n\n  There are different types of tasks:"
			+ "\n\n\t\uFFFC\tUser task - A User Task is a typical �workflow� Task where a human performer "
			+ "performs the Task with the assistance of a software application and is \n\n\t\t    scheduled through a "
			+ "task list manager of some sort. \n\n\t\uFFFC\tService task - A Service Task is a Task that uses "
			+ "some sort of service, which could be a Web service or an automated application. A service task is "
			+ "an automated activity \n\n\t\t    that starts automatically when the sequence flow arrives. If a person has to "
			+ "click a button then that is a User Task. \n\n  There are also diferent type of task executions:"
			+ "\n\n\t\uFFFC\tParallel execution type - a task that repeats, but multiple �copies� of the task can be happening in parallel."
			+ "\n\n\t\uFFFC\tLoop execution type - A task that repeats sequentially\n";

	public static final String CONTAINER_HELP = "\n  In a swimming pool, there are lanes designated for swimmers. Swimmers get their own lane to swim in, "
			+ "and they won't swim across another. "
			+ "\n\n  The similar concept of swimming lanes also exists in Business process management terminology. "
			+ "\n\n  Lanes are rectangular boxes that represent participants of a business process. \uFFFC"
			+ "\n\n  It can be a specific entity (e.g. department) or a role (e.g. assistant manager, doctor, student, vendor)."
			+ "\n\n  By using lane you can identify how a process is done and which department performs that activity.\n";
	
	public static final String COMMENT_HELP = "\n  Enter some free text to add more details to the process diagram. \uFFFC";
}
