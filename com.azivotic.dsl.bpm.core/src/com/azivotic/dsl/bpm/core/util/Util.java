package com.azivotic.dsl.bpm.core.util;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.graphiti.mm.pictograms.Diagram;

public class Util {

	public static void saveToModelFile(final EObject obj, final Diagram d) throws CoreException, IOException {
		ResourceSet rSet = d.eResource().getResourceSet();

		URI uriModel = formURI(d, "xmi");
		final String platformURI = uriModel.toPlatformString(true);

		if (platformURI != null) {
			final IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
			IResource file = workspaceRoot.findMember(platformURI);
			if (file == null || !file.exists()) {
				Resource createResource = rSet.createResource(uriModel);
				createResource.save(Collections.EMPTY_MAP);
				createResource.setTrackingModification(true);
			}
		}
		final Resource resourceModel = rSet.getResource(uriModel, true);
		resourceModel.getContents().add(obj);
	}

	public static URI formURI(final Diagram d, final String extension) {
		URI uri = EcoreUtil.getURI(d);
		uri = uri.trimFragment();
		uri = uri.trimFileExtension();
		uri = uri.appendFileExtension(extension);
		return uri;
	}

	/**
	 * Reflection attribute value change
	 */
	public static void setStaticValue(final String className, final String fieldName, final Object newValue) throws SecurityException, NoSuchFieldException,
	ClassNotFoundException, IllegalArgumentException, IllegalAccessException {
		// Get the private String field
		final Field field = Class.forName(className).getDeclaredField(fieldName);
		// Allow modification on the field
		field.setAccessible(true);
		// Get
		final Object oldValue = field.get(Class.forName(className));
		// Sets the field to the new value
		field.set(oldValue, newValue);
	}

}
