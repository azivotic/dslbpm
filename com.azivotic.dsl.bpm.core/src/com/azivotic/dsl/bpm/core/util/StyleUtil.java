package com.azivotic.dsl.bpm.core.util;

import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.algorithms.styles.Style;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.util.ColorConstant;
import org.eclipse.graphiti.util.IColorConstant;

import com.azivotic.dsl.bpm.core.diagram.style.LaneColoredAreas;

public class StyleUtil {

	public static final IColorConstant BLACK = IColorConstant.BLACK;

	public static final IColorConstant WHITE = 	IColorConstant.WHITE;
	 
	public static final IColorConstant GATEWAY_EDGE = IColorConstant.DARK_ORANGE;
	    
	public static final IColorConstant GATEWAY_FILL = IColorConstant.ORANGE;
	
	public static final IColorConstant DARK_GRAY =  IColorConstant.DARK_GRAY;
	 
	public static final IColorConstant BLUE = new ColorConstant(187, 218, 247);
	
	public static final IColorConstant DARK_BLUE = new ColorConstant(98, 131, 167);
	
	public static final IColorConstant RED = new ColorConstant(255, 0, 0);
	
	public static final IColorConstant COMMENT_FILL = new ColorConstant(255,160,122);	
	
	public static Style getStyleForTask(Diagram diagram) {
        final String styleId = "TASK-STYLE";
        IGaService gaService = Graphiti.getGaService();
        // Is style already persisted?
        Style style = gaService.findStyle(diagram, styleId);
        // style not found - create new style
        if (style == null) { 
            style = gaService.createPlainStyle(diagram, styleId);
            style.setFilled(true);
            style.setLineWidth(2);
            style.setForeground(gaService.manageColor(diagram, DARK_BLUE));
            style.setBackground(gaService.manageColor(diagram, BLUE));
        }
        return style;
    }
 
    public static Style getStyleForText(Diagram diagram) {
        final String styleId = "TEXT-STYLE";
        IGaService gaService = Graphiti.getGaService();
        Style style = gaService.findStyle(diagram, styleId);
        if (style == null) {
            style = gaService.createPlainStyle(diagram, styleId);
            style.setFilled(false);
            style.setForeground(gaService.manageColor(diagram, StyleUtil.BLACK));
            style.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
            style.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
            style.setFont(gaService.manageDefaultFont(diagram, false, true));
        }
        return style;
    }
    
    public static Style getStyleForComment(Diagram diagram) {
        final String styleId = "COMMENT-STYLE";
        IGaService gaService = Graphiti.getGaService();
        Style style = gaService.findStyle(diagram, styleId);
        if (style == null) {
            style = gaService.createPlainStyle(diagram, styleId);
            style.setFilled(false);
            style.setForeground(gaService.manageColor(diagram, StyleUtil.BLACK));
            style.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
            style.setVerticalAlignment(Orientation.ALIGNMENT_CENTER);
            style.setFont(gaService.manageDefaultFont(diagram, false, false));
        }
        return style;
    }
    
    public static Style getStyleForLane(Diagram diagram){
    	final String styleId = "LANE-STYLE";
        IGaService gaService = Graphiti.getGaService();
        Style style = gaService.findStyle(diagram, styleId);
        if (style == null) {
            style = gaService.createPlainStyle(diagram, styleId);
        	gaService.setRenderingStyle(style, LaneColoredAreas.getLaneAdaptions());
            style.setLineWidth(2);

        }
        return style;
    }
    
}
