package com.azivotic.dsl.bpm.core.rcp.wizard;

import java.util.Vector;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.graphiti.dt.IDiagramType;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

public class NewGraphitiDiagramWizardPage extends WizardPage {

  private Text m_diagramNameText;
  private Combo m_diagramTypes;

  public NewGraphitiDiagramWizardPage(final String p_pageName) {
    super(p_pageName);
    setTitle(p_pageName);
    setDescription("Create new BPM diagram");
  }

  public NewGraphitiDiagramWizardPage(final String p_pageName,
      final String p_title, final ImageDescriptor p_titleImage) {
    super(p_pageName, p_title, p_titleImage);
  }

  @Override
  public void createControl(final Composite p_parent) {
    Composite composite = new Composite(p_parent, SWT.NULL);
    composite.setFont(p_parent.getFont());

    initializeDialogUnits(p_parent);

    composite.setLayout(new GridLayout());
    composite.setLayoutData(new GridData(GridData.FILL_BOTH));

    createWizardContents(composite);

    setPageComplete(validatePage());

    setErrorMessage(null);
    setMessage(null);
    setControl(composite);
  }

  private void createWizardContents(final Composite p_parent) {

    Composite projectGroup = new Composite(p_parent, SWT.NONE);
    GridLayout layout = new GridLayout();
    layout.numColumns = 2;
    projectGroup.setLayout(layout);
    projectGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

    GridData data = new GridData(GridData.FILL_HORIZONTAL);
    data.widthHint = 250;

    // new project label
    Label diagramTypesLabel = new Label(projectGroup, SWT.NONE);
    diagramTypesLabel.setFont(p_parent.getFont());
    diagramTypesLabel.setText("Diagram Type: ");

    // new project name entry field
    m_diagramTypes = new Combo(projectGroup, SWT.READ_ONLY | SWT.BORDER);
    m_diagramTypes.setLayoutData(data);
    m_diagramTypes.setFont(p_parent.getFont());
    m_diagramTypes.setVisibleItemCount(12);

    // set the contents of the Combo-widget
    m_diagramTypes.setItems(getAllAvailableDiagramTypes());

    // new project label
    Label diagramNameLabel = new Label(projectGroup, SWT.NONE);
    diagramNameLabel.setText("Diagram name: ");
    diagramNameLabel.setFont(p_parent.getFont());

    // new project name entry field
    m_diagramNameText = new Text(projectGroup, SWT.BORDER);
    m_diagramNameText.setLayoutData(data);
    m_diagramNameText.setFont(p_parent.getFont());

    // Set the initial value first before listener
    // to avoid handling an event during the creation.
    if (getInitialTextFieldValue() != null) {
      m_diagramNameText.setText(getInitialTextFieldValue());
    }

    m_diagramNameText.addListener(SWT.Modify, nameModifyListener);

  }

  private boolean validatePage() {

    if (!isPageValid()) {
      return false;
    }

    setErrorMessage(null);
    setMessage(null);

    return true;
  }

  private boolean isPageValid() {
    boolean isValid = true;

    isValid &= checkTextInFields(getDiagramNameTextFieldValue());
    isValid &= checkTextInFields(getDiagramTypeComboFieldValue());

    isValid &= validateStatus(getDiagramNameTextFieldValue());
    isValid &= validateStatus(getDiagramTypeComboFieldValue());

    return isValid;
  }

  private boolean checkTextInFields(final String p_textToCheck) {
    String text = p_textToCheck;
    if (text.equals("")) {
      setErrorMessage(null);
      setMessage("");
      return false;
    }

    return true;
  }

  private boolean validateStatus(final String p_status) {
    IWorkspace workspace = ResourcesPlugin.getWorkspace();
    IStatus status = doWorkspaceValidation(workspace, p_status);

    if (!status.isOK()) {
      setErrorMessage(status.getMessage());
      return false;
    }

    return true;
  }

  private IStatus doWorkspaceValidation(final IWorkspace workspace,
      final String text) {
    IStatus ret = workspace.validateName(text, IResource.FILE);
    return ret;
  }

  public String getDiagramNameTextFieldValue() {
    if (m_diagramNameText == null) {
      return "";
    }

    return m_diagramNameText.getText().trim();
  }

  private String getInitialTextFieldValue() {
    return "";
  }

  public String getDiagramTypeComboFieldValue() {
    if (m_diagramTypes == null) {
      return "";
    }

    return m_diagramTypes.getText().trim();
  }

  protected String[] getAllAvailableDiagramTypes() {
    Vector<String> diagramIds = new Vector<String>();
    for (IDiagramType diagramType : GraphitiUi.getExtensionManager().getDiagramTypes()) {
      diagramIds.add(diagramType.getId());
    }

    return diagramIds.toArray(new String[] {});
  }

  private final Listener nameModifyListener = new Listener() {
    @Override
    public void handleEvent(final Event e) {
      boolean valid = validatePage();
      setPageComplete(valid);
    }
  };

  @Override
  public void setVisible(final boolean visible) {
    super.setVisible(visible);
    if (visible) {
      m_diagramTypes.setFocus();
      m_diagramNameText.selectAll();
    }
  }
}
