package com.azivotic.dsl.bpm.core.rcp.perspective;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class DslForBpmPerspective implements IPerspectiveFactory {

	  public static final String PERSPECTIVE_ID = "com.azivotic.dsl.bpm.core.rcp.perspective.main";

	  @Override
	  public void createInitialLayout(final IPageLayout layout) {

	  }
}
