package com.azivotic.dsl.bpm.core.rcp.handler;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.impl.PrintContext;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.features.DefaultPrintFeature;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author azivotic
 * Print business process diagram
 */
public class PrintDiagramCommandHandler implements IHandler {

  @Override
  public Object execute(final ExecutionEvent p_event) throws ExecutionException {
    final IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();

    if (!(editorPart instanceof DiagramEditor)) {
      return false;
    }

    final DiagramEditor diagramEditor = (DiagramEditor) editorPart;
    final IFeatureProvider featureProvider = diagramEditor.getDiagramTypeProvider().getFeatureProvider();
    final DefaultPrintFeature printFeature = new DefaultPrintFeature(featureProvider);

    printFeature.print(new PrintContext());

    return true;
  }

  @Override
  public boolean isEnabled() {
    return checkIfDiagramEditor();
  }

  @Override
  public boolean isHandled() {
    return checkIfDiagramEditor();
  }

  @Override
  public void addHandlerListener(final IHandlerListener p_handlerListener) {
    // no listeners
  }

  @Override
  public void removeHandlerListener(final IHandlerListener p_handlerListener) {
    // no listeners
  }

  @Override
  public void dispose() {
    // nothing
  }

  private IEditorPart getActiveEditor() {
    return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
  }

  private boolean checkIfDiagramEditor() {
    final IEditorPart editorPart = getActiveEditor();

    if (editorPart instanceof DiagramEditor) {
      return true;
    }

    return false;
  }

}
