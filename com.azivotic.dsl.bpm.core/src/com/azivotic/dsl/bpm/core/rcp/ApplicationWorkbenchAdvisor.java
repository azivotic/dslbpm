package com.azivotic.dsl.bpm.core.rcp;

import java.net.URL;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.ide.IDE;
import org.osgi.framework.Bundle;

import com.azivotic.dsl.bpm.core.rcp.perspective.DslForBpmPerspective;

public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	 private static final String ICONS_PATH = "icons/full/";
	  private static final String PATH_OBJECT = ICONS_PATH + "obj16/";
	  private static final String IDE_WORKBENCH_ID = "org.eclipse.ui.ide";

	  @Override
	  public void initialize(final IWorkbenchConfigurer configurer) {
	    super.initialize(configurer);

	    IDE.registerAdapters();

	    Bundle ideBundle = Platform.getBundle(IDE_WORKBENCH_ID);

	    declareWorkbenchImage(configurer, ideBundle, IDE.SharedImages.IMG_OBJ_PROJECT, PATH_OBJECT + "prj_obj.gif", true);
	    declareWorkbenchImage(configurer, ideBundle, IDE.SharedImages.IMG_OBJ_PROJECT_CLOSED, PATH_OBJECT + "cprj_obj.gif", true);

	  }

	  @Override
	  public boolean preShutdown() {
	    try {
	      ResourcesPlugin.getWorkspace().save(Boolean.TRUE, null);
	    } catch (CoreException e) {
	      e.printStackTrace();
	      return Boolean.FALSE;
	    }

	    return Boolean.TRUE;
	  }

	  @Override
	  public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(final IWorkbenchWindowConfigurer configurer) {
	    return new ApplicationWorkbenchWindowAdvisor(configurer);
	  }

	  @Override
	  public String getInitialWindowPerspectiveId() {
	    return DslForBpmPerspective.PERSPECTIVE_ID;
	  }

	  private void declareWorkbenchImage(final IWorkbenchConfigurer configurer, final Bundle ideBundle, final String symbolicName, final String path,
	      final boolean shared) {
	    URL url = ideBundle.getEntry(path);
	    ImageDescriptor desc = ImageDescriptor.createFromURL(url);
	    configurer.declareImage(symbolicName, desc, shared);
	  }

	  @Override
	  public IAdaptable getDefaultPageInput() {
	    // Fixes issue with Project Explorer not refreshing on startup
	    return ResourcesPlugin.getWorkspace().getRoot();
	  }
}
