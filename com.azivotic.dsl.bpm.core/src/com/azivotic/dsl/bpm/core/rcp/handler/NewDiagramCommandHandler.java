package com.azivotic.dsl.bpm.core.rcp.handler;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.PlatformUI;

import com.azivotic.dsl.bpm.core.rcp.wizard.NewGraphitiDiagramWizard;

/**
 * 
 * @author azivotic
 * Create new diagram for business process
 *
 */
public class NewDiagramCommandHandler implements IHandler {

  @Override
  public Object execute(final ExecutionEvent p_event) throws ExecutionException {
    final NewGraphitiDiagramWizard graphitiDiagramWizard = new NewGraphitiDiagramWizard();

    final WizardDialog wizardDialog = new WizardDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell(), graphitiDiagramWizard);

    final int dialogOpenSuccess = wizardDialog.open();

    if (dialogOpenSuccess != WizardDialog.OK) {
      return false;
    }

    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public boolean isHandled() {
    return true;
  }

  @Override
  public void addHandlerListener(final IHandlerListener p_handlerListener) {
    // no listeners
  }

  @Override
  public void removeHandlerListener(final IHandlerListener p_handlerListener) {
    // no listeners
  }

  @Override
  public void dispose() {
    // nothing
  }

}
