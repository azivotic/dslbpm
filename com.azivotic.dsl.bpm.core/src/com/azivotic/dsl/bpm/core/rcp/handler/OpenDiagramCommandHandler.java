package com.azivotic.dsl.bpm.core.rcp.handler;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author azivotic
 * Open .diagram file for business process
 */
public class OpenDiagramCommandHandler implements IHandler {

  @Override
  public Object execute(final ExecutionEvent p_event) throws ExecutionException {
    final FileDialog fileDialog = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);

    fileDialog.setFilterExtensions(new String[] { "*.diagram", "*.*" });
    fileDialog.setFilterIndex(0);
    fileDialog.setText("Open");

    final String fileName = fileDialog.open();

    if (fileName == null) {
      // Cancelled
      return null;
    }

    final URI fileURI = URI.createFileURI(fileName);
    final URIEditorInput editorInput = new URIEditorInput(fileURI);

    try {
      PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(editorInput, DiagramEditor.DIAGRAM_EDITOR_ID);

    } catch (final PartInitException p_partInitException) {
      p_partInitException.printStackTrace();
      return null;
    }

    return null;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public boolean isHandled() {
    return true;
  }

  @Override
  public void addHandlerListener(final IHandlerListener p_handlerListener) {
    // no listeners
  }

  @Override
  public void removeHandlerListener(final IHandlerListener p_handlerListener) {
    // no listeners
  }

  @Override
  public void dispose() {
    // nothing
  }
}
