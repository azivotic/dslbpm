package com.azivotic.dsl.bpm.core.rcp.wizard;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.editor.DiagramEditor;
import org.eclipse.graphiti.ui.editor.DiagramEditorInput;
import org.eclipse.graphiti.ui.services.GraphitiUi;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.azivotic.dsl.bpm.core.rcp.Application;

public class NewGraphitiDiagramWizard extends Wizard implements IWorkbenchWizard {

  private NewGraphitiDiagramWizardPage m_diagramWizardPage;

  public NewGraphitiDiagramWizard() {
    super();
    setNeedsProgressMonitor(true);
  }

  @Override
  public void addPages() {
    m_diagramWizardPage = new NewGraphitiDiagramWizardPage("New BPM diagram");
    setWindowTitle("New BPM Diagram");
    addPage(m_diagramWizardPage);
  }

  @Override
  public void init(final IWorkbench workbench, final IStructuredSelection currentSelection) {
    setWindowTitle("New BPM Diagram");
  }

  @Override
  public boolean performFinish() {
    IProject project = null;
    IFolder diagramFolder = null;

    IStructuredSelection selection = (IStructuredSelection) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
    Object element = selection.getFirstElement();

    if (element instanceof IProject) {
      project = (IProject) element;
    } else if (element instanceof IFolder) {
      diagramFolder = (IFolder) element;
      project = diagramFolder.getProject();
    }

    if (project == null || !project.isAccessible()) {
      String error = "No open project was found for the current selection. Select a project and restart the wizard.";
      IStatus status = new Status(IStatus.ERROR, Application.PLUGIN_ID, error);
      ErrorDialog.openError(getShell(), "No Project Found", null, status);
      return false;
    }

    String diagramTypeId = m_diagramWizardPage.getDiagramTypeComboFieldValue();
    String diagramName = m_diagramWizardPage.getDiagramNameTextFieldValue();

    Diagram diagram = Graphiti.getPeCreateService().createDiagram(diagramTypeId, diagramName, true);

    String editorID = DiagramEditor.DIAGRAM_EDITOR_ID;
    String editorExtension = "diagram";
    String diagramTypeProviderId = GraphitiUi.getExtensionManager().getDiagramTypeProviderId(diagramTypeId);
    String namingConventionID = diagramTypeProviderId + ".editor";
    IEditorDescriptor specificEditor = PlatformUI.getWorkbench().getEditorRegistry().findEditor(namingConventionID);

    // If there is a specific editor get the file extension
    if (specificEditor != null) {
      editorID = namingConventionID;
      IExtensionRegistry extensionRegistry = Platform.getExtensionRegistry();
      IExtensionPoint extensionPoint = extensionRegistry.getExtensionPoint("org.eclipse.ui.editors");
      IExtension[] extensions = extensionPoint.getExtensions();
      for (IExtension ext : extensions) {
        IConfigurationElement[] configurationElements = ext.getConfigurationElements();
        for (IConfigurationElement ce : configurationElements) {
          String id = ce.getAttribute("id");
          if (editorID.equals(id)) {
            String fileExt = ce.getAttribute("extensions");
            if (fileExt != null) {
              editorExtension = fileExt;
              break;
            }
          }

        }
      }
    }

    IFile diagramFile;

    if (diagramFolder == null) {
      diagramFile = project.getFile(diagramName + "." + editorExtension);
    } else {
      diagramFile = diagramFolder.getFile(diagramName + "." + editorExtension);
    }

    URI uri = URI.createPlatformResourceURI(diagramFile.getFullPath().toString(), true);

    createEmfFileForDiagram(uri, diagram);

    String providerId = GraphitiUi.getExtensionManager().getDiagramTypeProviderId(diagram.getDiagramTypeId());
    DiagramEditorInput editorInput = new DiagramEditorInput(EcoreUtil.getURI(diagram), providerId);

    try {
      PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(editorInput, editorID);
    } catch (PartInitException e) {
      String error = "Error while opening diagram editor";
      IStatus status = new Status(IStatus.ERROR, Application.PLUGIN_ID, error, e);
      ErrorDialog.openError(getShell(), "An error occured", null, status);
      return false;
    }

    return true;
  }

  private boolean createEmfFileForDiagram(final URI p_uri, final Diagram p_diagram) {
    final TransactionalEditingDomain editingDomain = GraphitiUi.getEmfService().createResourceSetAndEditingDomain();
    final CommandStack commandStack = editingDomain.getCommandStack();

    final Resource diagramResource = editingDomain.getResourceSet().createResource(p_uri);

    commandStack.execute(new RecordingCommand(editingDomain) {
      @Override
      protected void doExecute() {
        diagramResource.setTrackingModification(true);

        diagramResource.getContents().add(p_diagram);

        try {
          diagramResource.save(Collections.EMPTY_MAP);

        } catch (IOException p_exception) {
          p_exception.printStackTrace();
        }
      }
    });

    return true;
  }

}
