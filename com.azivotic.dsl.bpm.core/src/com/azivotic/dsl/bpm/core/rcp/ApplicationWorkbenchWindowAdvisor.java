package com.azivotic.dsl.bpm.core.rcp;

import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

/**
 * 
 * @author azivotic
 *
 */
public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

    public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        super(configurer);
    }

    @Override
    public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
        return new ApplicationActionBarAdvisor(configurer);
    }
    
    @Override
    public void preWindowOpen() {
      final IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
      configurer.setShowCoolBar(true);
      configurer.setShowStatusLine(true);
    }

    @Override
    public void postWindowOpen() {
      final IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
      configurer.getWindow().getShell().setMaximized(true);
    }
}
