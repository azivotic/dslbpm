package com.azivotic.dsl.bpm.core.properties;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.FreeFormConnection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.ui.platform.GFPropertySection;
import org.eclipse.graphiti.util.IColorConstant;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;

import com.azivotic.dsl.bpm.metamodel.Gateway;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;

public class GatewaySequenceFlowClassSection extends GFPropertySection implements ITabbedPropertyConstants {

	private org.eclipse.swt.widgets.Text nameText;
	private org.eclipse.swt.widgets.Text descriptionText;
	private org.eclipse.swt.widgets.Text conditionText;
	private Button defaultLine;

	IPeCreateService peCreateService = Graphiti.getPeCreateService();
	IGaService gaService = Graphiti.getGaService();

	private final ModifyListener nameTextListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			// zbog transakcije unutar transakcije prilikom poziva create metode
			if (nameText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final SequenceFlow sequenceFlow = (SequenceFlow) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(sequenceFlow);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newText = nameText.getText();
						sequenceFlow.setName(newText);
					}
				});
			}
		}
	};

	private final ModifyListener eventDescriptionListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			if (descriptionText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final SequenceFlow sequenceFlow = (SequenceFlow) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(sequenceFlow);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newDescription = descriptionText.getText();
						sequenceFlow.setDescription(newDescription);
					}
				});
			}

		}
	};

	private final ModifyListener eventConditionListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			if (conditionText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final SequenceFlow sequenceFlow = (SequenceFlow) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(sequenceFlow);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newCondition = conditionText.getText();
						sequenceFlow.setCondition(newCondition);
					}
				});
			}

		}
	};


	private final SelectionListener defaultLineListener = new SelectionListener() {

		@Override
		public void widgetSelected(final SelectionEvent e) {
			final PictogramElement pe = getSelectedPictogramElement();
			final SequenceFlow sequenceFlow = (SequenceFlow) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);


			TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(sequenceFlow);
			editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {

				@Override
				protected void doExecute() {
					boolean defaultLineValue = defaultLine.getSelection();
					FreeFormConnection connection = (FreeFormConnection)pe;

					// Set line decorator if user select true
					// second part of the condition is because of the unknown behavior where doExecute is called
					// twice. We want to add new decorator only if it is the first call(if connections already have only
					// two decorators - arrow head and text)
					if(defaultLineValue && connection.getConnectionDecorators().size()==2){
						//Check all flows from Gateway which is the source for selected flow
						//If any flow is default de-select it
						if(sequenceFlow.getSourceRef() instanceof Gateway){
							for(SequenceFlow flow : sequenceFlow.getSourceRef().getOutgoing()){
								if(flow.isDefaultLine()){
									FreeFormConnection connectionOld = (FreeFormConnection)Graphiti.getLinkService().getPictogramElements(getDiagram(), flow).get(0);
									if(connectionOld.getConnectionDecorators().size()>2){
										connectionOld.getConnectionDecorators().remove(2);
										flow.setDefaultLine(false);
									}
								}
							}
						}
						ConnectionDecorator cd = peCreateService.createConnectionDecorator((Connection)pe, false, 1.0, true);
						Polyline polyline = gaService.createPolyline(cd, new int[] { -15, 7, -15, -7 });
						polyline.setForeground(gaService.manageColor(getDiagram(), IColorConstant.DARK_GRAY));
						polyline.setLineWidth(2);
						sequenceFlow.setDefaultLine(defaultLineValue);
						//Remove line decorator
					} else if(!defaultLineValue) {
						sequenceFlow.setDefaultLine(defaultLineValue);
						//remove third element(first is text decorator, second is arrow decorator and third
						//is if exists default line decorator
						if(connection.getConnectionDecorators().size()>2){
							connection.getConnectionDecorators().remove(2);
						}
					}
				}
			});

		}

		@Override
		public void widgetDefaultSelected(final SelectionEvent e) {
			// TODO Auto-generated method stub
		}

	};


	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		TabbedPropertySheetWidgetFactory factory = getWidgetFactory();
		Composite composite = factory.createFlatFormComposite(parent);
		FormData data;

		//name text
		{
			nameText = factory.createText(composite, "");
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(45, 0);
			data.top = new FormAttachment(0, VSPACE);
			nameText.setLayoutData(data);

			CLabel valueLabel = factory.createCLabel(composite, "Name:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(nameText, HSPACE);
			data.top = new FormAttachment(0, VSPACE);
			valueLabel.setLayoutData(data);
		}

		// description text
		{
			descriptionText = factory.createText(composite, "", SWT.MULTI | SWT.V_SCROLL);
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(45, 0);
			data.top = new FormAttachment(0, 50);
			data.height = 100;
			descriptionText.setLayoutData(data);

			CLabel descriptionLabel = factory.createCLabel(composite, "Description:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(0, 80);
			data.top = new FormAttachment(descriptionText, 0, SWT.TOP);
			descriptionLabel.setLayoutData(data);
		}

		//default line check box
		{
			defaultLine = factory.createButton(composite, null, SWT.CHECK);
			data = new FormData();
			data.left = new FormAttachment(50, 125);
			data.right = new FormAttachment(95, 0);
			data.top = new FormAttachment(0, VSPACE);
			defaultLine.setLayoutData(data);
			
			CLabel defaultLineLabel = factory.createCLabel(composite, "Default flow:");
			data = new FormData();
			data.left = new FormAttachment(50, 30);
			data.right = new FormAttachment(defaultLine, HSPACE);
			data.top = new FormAttachment(defaultLine, 0, SWT.TOP);
			defaultLineLabel.setLayoutData(data);
		}

		// condition text
		{
			conditionText = factory.createText(composite, "", SWT.MULTI | SWT.V_SCROLL);
			data = new FormData();
			data.left = new FormAttachment(50, 125);
			data.right = new FormAttachment(95, 0);
			data.top = new FormAttachment(0, 50);
			data.height = 100;
			conditionText.setLayoutData(data);

			CLabel descriptionLabel = factory.createCLabel(composite, "Condition:");
			data = new FormData();
			data.left = new FormAttachment(50, 30);
			data.right = new FormAttachment(conditionText, HSPACE);
			data.top = new FormAttachment(conditionText, 0, SWT.TOP);
			descriptionLabel.setLayoutData(data);
		}



	}

	@Override
	public void refresh() {
		PictogramElement pe = getSelectedPictogramElement();
		if (pe != null) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			if (bo == null || !(bo instanceof SequenceFlow))
				return;

			String name = ((SequenceFlow) bo).getName();
			nameText.setText(name == null ? "" : name);

			String description = ((SequenceFlow) bo).getDescription();
			descriptionText.setText(description == null ? "" : description);

			String condition = ((SequenceFlow) bo).getCondition();
			conditionText.setText(condition == null ? "" : condition);

			boolean defaultLineValue = ((SequenceFlow) bo).isDefaultLine();
			defaultLine.setSelection(defaultLineValue);
		}
	}

	@Override
	public void aboutToBeShown() {
		if(!nameText.isDisposed()){
			nameText.addModifyListener(nameTextListener);
			descriptionText.addModifyListener(eventDescriptionListener);
			conditionText.addModifyListener(eventConditionListener);
			defaultLine.addSelectionListener(defaultLineListener);
		}
	}

	@Override
	public void aboutToBeHidden() {
		if(!nameText.isDisposed()){
			nameText.removeModifyListener(nameTextListener);
			descriptionText.removeModifyListener(eventDescriptionListener);
			conditionText.removeModifyListener(eventConditionListener);
			defaultLine.removeSelectionListener(defaultLineListener);
		}
	}
}


