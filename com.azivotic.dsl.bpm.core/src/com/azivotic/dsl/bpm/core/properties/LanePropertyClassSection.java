package com.azivotic.dsl.bpm.core.properties;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.platform.GFPropertySection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;

import com.azivotic.dsl.bpm.metamodel.Lane;

public class LanePropertyClassSection extends GFPropertySection implements ITabbedPropertyConstants {

	private org.eclipse.swt.widgets.Text nameText;
	private org.eclipse.swt.widgets.Text descriptionText;

	private final ModifyListener containerNameTextListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			// zbog transakcije unutar transakcije prilikom poziva create metode
			if (nameText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final Lane container = (Lane) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(container);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newText = nameText.getText();
						container.setName(newText);
					}
				});
			}
		}
	};

	private final ModifyListener containerDescriptionListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			if (descriptionText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final Lane container = (Lane) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(container);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newDescription = descriptionText.getText();
						container.setDescription(newDescription);
					}
				});
			}

		}
	};

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		TabbedPropertySheetWidgetFactory factory = getWidgetFactory();
		Composite composite = factory.createFlatFormComposite(parent);
		FormData data;

		{
			nameText = factory.createText(composite, "");
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(50, 0);
			data.top = new FormAttachment(0, VSPACE);
			nameText.setLayoutData(data);

			CLabel valueLabel = factory.createCLabel(composite, "Name:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(nameText, HSPACE);
			data.top = new FormAttachment(0, VSPACE);
			valueLabel.setLayoutData(data);
		}

		// description text
		{
			descriptionText = factory.createText(composite, "", SWT.MULTI | SWT.V_SCROLL);
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(50, 0);
			data.top = new FormAttachment(0, 50);
			data.height = 100;
			descriptionText.setLayoutData(data);

			CLabel descriptionLabel = factory.createCLabel(composite, "Description:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(0, 80);
			data.top = new FormAttachment(descriptionText, 0, SWT.TOP);
			descriptionLabel.setLayoutData(data);
		}
	}

	@Override
	public void refresh() {
		PictogramElement pe = getSelectedPictogramElement();
		if (pe != null) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			if (bo == null || !(bo instanceof Lane))
				return;

			String name = ((Lane) bo).getName();
			nameText.setText(name == null ? "" : name);

			String description = ((Lane) bo).getDescription();
			descriptionText.setText(description == null ? "" : description);
		}
	}

	@Override
	public void aboutToBeShown() {
		if(!nameText.isDisposed()){
			nameText.addModifyListener(containerNameTextListener);
			descriptionText.addModifyListener(containerDescriptionListener);
		}
	}

	@Override
	public void aboutToBeHidden() {
		if(!nameText.isDisposed()){
			nameText.removeModifyListener(containerNameTextListener);
			descriptionText.removeModifyListener(containerDescriptionListener);
		}
	}
}
