package com.azivotic.dsl.bpm.core.properties;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.platform.GFPropertySection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;

import com.azivotic.dsl.bpm.metamodel.Comment;

public class CommentPropertyClassSection extends GFPropertySection implements ITabbedPropertyConstants {

	private org.eclipse.swt.widgets.Text commentText;


	private final ModifyListener commentTextListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			if (commentText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final Comment comment = (Comment) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(comment);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newDescription = commentText.getText();
						comment.setDescription(newDescription);
					}
				});
			}

		}
	};

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		TabbedPropertySheetWidgetFactory factory = getWidgetFactory();
		Composite composite = factory.createFlatFormComposite(parent);
		FormData data;

		{
			commentText = factory.createText(composite, "", SWT.MULTI | SWT.V_SCROLL);
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(50, 0);
			data.top = new FormAttachment(0, VSPACE);
			data.height = 150;
			commentText.setLayoutData(data);

			CLabel valueLabel = factory.createCLabel(composite, "Comment:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(commentText, HSPACE);
			data.top = new FormAttachment(0, VSPACE);
			valueLabel.setLayoutData(data);
		}

	}

	@Override
	public void refresh() {
		PictogramElement pe = getSelectedPictogramElement();
		if (pe != null) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			if (bo == null)
				return;

			String comment = ((Comment) bo).getDescription();
			commentText.setText(comment == null ? "" : comment);
		}
	}

	@Override
	public void aboutToBeShown() {
		if(!commentText.isDisposed()){
			commentText.addModifyListener(commentTextListener);
		}
	}

	@Override
	public void aboutToBeHidden() {
		if(!commentText.isDisposed()){
			commentText.removeModifyListener(commentTextListener);
		}
	}
}
