package com.azivotic.dsl.bpm.core.properties;

import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.ui.platform.GFPropertySection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.PaintObjectEvent;
import org.eclipse.swt.custom.PaintObjectListener;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GlyphMetrics;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;

import com.azivotic.dsl.bpm.core.util.HelpUtil;
import com.azivotic.dsl.bpm.metamodel.Comment;
import com.azivotic.dsl.bpm.metamodel.EndEvent;
import com.azivotic.dsl.bpm.metamodel.Gateway;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.StartEvent;
import com.azivotic.dsl.bpm.metamodel.Task;

public class HelpPropertyClassSection extends GFPropertySection implements ITabbedPropertyConstants {

	private StyledText helpText;
	protected Font descriptionFont = null;

	private VerifyListener verifyListener = new VerifyListener()  {
		@Override
		public void verifyText(VerifyEvent event) {
			if (event.start == event.end) return;
			String text = helpText.getText(event.start, event.end - 1);
			int index = text.indexOf('\uFFFC');
			while (index != -1) {
				StyleRange style = helpText.getStyleRangeAtOffset(event.start + index);
				if (style != null) {
					Image image = (Image)style.data;
					if (image != null) image.dispose();
				}
				index = text.indexOf('\uFFFC', index + 1);
			}
		}
	};

	private PaintObjectListener paintObjectListener = new PaintObjectListener() {
		@Override
		public void paintObject(PaintObjectEvent event) {
			StyleRange style = event.style;
			Image image = (Image)style.data;
			if (!image.isDisposed()) {
				int x = event.x;
				int y = event.y + event.ascent - style.metrics.ascent;						
				event.gc.drawImage(image, x, y);
			}
		}
	};

	private Listener listener = new Listener() {
		@Override
		public void handleEvent(Event event) {
			StyleRange[] styles = helpText.getStyleRanges();
			for (int i = 0; i < styles.length; i++) {
				StyleRange style = styles[i];
				if (style.data != null) {
					Image image = (Image)style.data;
					if (image != null) image.dispose();
				}
			}
		}
	};

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);
		helpText = new StyledText(parent, SWT.MULTI | SWT.READ_ONLY);
	}

	public Font getDescriptionFont() {
		if (descriptionFont==null) {
			Display display = Display.getCurrent();
			FontData data = display.getSystemFont().getFontData()[0];
			descriptionFont = new Font(display, data.getName(), data.getHeight() + 1, SWT.NONE);
		}
		return descriptionFont;
	}

	@Override
	public void refresh() {
		PictogramElement pe = getSelectedPictogramElement();
		int[] offsets;
		Image[] images;

		if (pe != null) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			if (bo == null)
				return;

			if(bo instanceof StartEvent) {
				String text = HelpUtil.START_EVENT_HELP;
				helpText.setText(text);
				Image start = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/start.png"));
				int offset = text.indexOf("\uFFFC", 0);
				addImage(start, offset);
			} else if (bo instanceof EndEvent){
				String text = HelpUtil.END_EVENT_HELP;
				helpText.setText(text);
				Image start = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/stop.png"));
				int offset = text.indexOf("\uFFFC", 0);
				addImage(start, offset);
			} else if (bo instanceof Gateway){
				String text = HelpUtil.GATEWAY_HELP;
				helpText.setText(text);
				Image gateway = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/gateway.png"));
				Image exclusive = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/exclusive.png"));
				Image inclusive = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/inclusive.png"));
				Image parallel = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/parallel.png"));
				images = new Image[] { gateway, exclusive, inclusive, parallel };
				offsets = new int[images.length];
				int lastOffset = 0;
				for (int i = 0; i < images.length; i++) {
					int offset = text.indexOf("\uFFFC", lastOffset);
					offsets[i] = offset;
					addImage(images[i], offset);
					lastOffset = offset + 1;
				}
			} else if (bo instanceof Task){
				String text = HelpUtil.TASK_HELP;
				helpText.setText(text);
				Image task = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/task.png"));
				Image user = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/user-task.png"));
				Image service = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/service-task.png"));
				Image parallel = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/parallel-task.png"));
				Image loop = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/loop-task.png"));
				images = new Image[] { task, user, service, parallel, loop };
				offsets = new int[images.length];
				int lastOffset = 0;
				for (int i = 0; i < images.length; i++) {
					int offset = text.indexOf("\uFFFC", lastOffset);
					offsets[i] = offset;
					addImage(images[i], offset);
					lastOffset = offset + 1;
				}
			} else if (bo instanceof SequenceFlow){
				String text = HelpUtil.SEQUENCE_FLOW_HELP;
				helpText.setText(text);
				Image line = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/line.png"));
				Image defaultLine = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/default-line.png"));
				images = new Image[] { line, defaultLine };
				offsets = new int[images.length];
				int lastOffset = 0;
				for (int i = 0; i < images.length; i++) {
					int offset = text.indexOf("\uFFFC", lastOffset);
					offsets[i] = offset;
					addImage(images[i], offset);
					lastOffset = offset + 1;
				}
			} else if (bo instanceof Lane){
				String text = HelpUtil.CONTAINER_HELP;
				helpText.setText(text);
				Image lane = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/lane.png"));

				images = new Image[] { lane };
				offsets = new int[images.length];
				int lastOffset = 0;
				for (int i = 0; i < images.length; i++) {
					int offset = text.indexOf("\uFFFC", lastOffset);
					offsets[i] = offset;
					addImage(images[i], offset);
					lastOffset = offset + 1;
				}
			} else if (bo instanceof Comment){
				String text = HelpUtil.COMMENT_HELP;
				helpText.setText(text);
				Image start = new Image(helpText.getDisplay(),getClass().getResourceAsStream("/icons/app/help/comment.png"));
				int offset = text.indexOf("\uFFFC", 0);
				addImage(start, offset);
			}
			
		}
	}

	@Override
	public void aboutToBeShown() {
		if(!helpText.isDisposed()){
			helpText.addVerifyListener(verifyListener);
			helpText.addPaintObjectListener(paintObjectListener);
			helpText.addListener(SWT.Dispose, listener);
		}
	}

	@Override
	public void aboutToBeHidden() {
		if(!helpText.isDisposed()){
			helpText.removeVerifyListener(verifyListener);;
			helpText.removePaintObjectListener(paintObjectListener);;
			helpText.removeListener(SWT.Dispose, listener);
		}
	}

	private void addImage(Image image, int offset) {
		StyleRange style = new StyleRange ();
		style.start = offset;
		style.length = 1;
		style.data = image;
		Rectangle rect = image.getBounds();
		style.metrics = new GlyphMetrics(rect.height, 0, rect.width);
		helpText.setStyleRange(style);		
	}

}
