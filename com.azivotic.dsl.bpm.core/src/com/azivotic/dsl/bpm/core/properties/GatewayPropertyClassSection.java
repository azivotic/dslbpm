package com.azivotic.dsl.bpm.core.properties;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.ui.platform.GFPropertySection;
import org.eclipse.graphiti.util.IColorConstant;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;

import com.azivotic.dsl.bpm.metamodel.Gateway;
import com.azivotic.dsl.bpm.metamodel.GatewayType;

public class GatewayPropertyClassSection extends GFPropertySection implements ITabbedPropertyConstants {

	private org.eclipse.swt.widgets.Text nameText;
	private org.eclipse.swt.widgets.Text descriptionText;
	private CCombo gatewayType;

	private IGaService gaService = Graphiti.getGaService();

	private final ModifyListener nameTextListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			// Because transaction call inside transaction in create metode
			if (nameText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final Gateway gateway = (Gateway) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(gateway);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newText = nameText.getText();
						gateway.setName(newText);
					}
				});
			}
		}
	};

	private final ModifyListener eventDescriptionListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			if (descriptionText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final Gateway gateway = (Gateway) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(gateway);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newDescription = descriptionText.getText();
						gateway.setDescription(newDescription);
					}
				});
			}

		}
	};

	private final SelectionListener taskTypeListener = new SelectionListener() {

		@Override
		public void widgetSelected(final SelectionEvent e) {
			final PictogramElement pe = getSelectedPictogramElement();
			final Gateway gateway = (Gateway) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);


			TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(gateway);
			editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {

				@Override
				protected void doExecute() {
					String key = gatewayType.getText();
					GatewayType type = (GatewayType) gatewayType.getData(key);
					gateway.setGatewayType(type);
					//uklonimo sve prethodne elemente koji su predstavljali tip gateway-a
					EList<GraphicsAlgorithm> invisibleRectangleChildren = pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren();
					if(invisibleRectangleChildren.size() != 0){
						//prvo smo morali do�i do dece invisible polygona, pa tek onda od polygon-a uzeti decu
						EList<GraphicsAlgorithm>  polygonChildren = invisibleRectangleChildren.get(0).getGraphicsAlgorithmChildren();
						if(polygonChildren.size() != 0){
							polygonChildren.removeAll(polygonChildren);
						}
					}
					GraphicsAlgorithm polygon = getSelectedPictogramElement().getGraphicsAlgorithm().getGraphicsAlgorithmChildren().get(0);
					if(polygon == null)
						return;
					
					switch(type){
						case DEFAULT:
							break;
							
						case INCLUSIVE :
							Ellipse ellipse = gaService.createEllipse(polygon);
							ellipse.setLineWidth(2);
							ellipse.setFilled(false);
							ellipse.setForeground(gaService.manageColor(getDiagram(), IColorConstant.DARK_ORANGE));
							gaService.setLocationAndSize(ellipse, 12, 12, 25, 25);
							break;
							
						case EXCLUSIVE:
							Polyline line1 = gaService.createPolyline(polygon, new int[] {15, 15, 34, 34});
							line1.setLineWidth(2);
							line1.setForeground(gaService.manageColor(getDiagram(), IColorConstant.DARK_ORANGE));
							Polyline line2 = gaService.createPolyline(polygon, new int[] {34, 15, 15, 34});
							line2.setLineWidth(2);
							line2.setForeground(gaService.manageColor(getDiagram(), IColorConstant.DARK_ORANGE));
							break;
	
						case PARALLEL:
							line1 = gaService.createPolyline(polygon, new int[] {24, 12, 24, 38});
							line1.setLineWidth(2);
							line1.setForeground(gaService.manageColor(getDiagram(), IColorConstant.DARK_ORANGE));
							line2 = gaService.createPolyline(polygon, new int[] {12, 24, 38, 24});
							line2.setLineWidth(2);
							line2.setForeground(gaService.manageColor(getDiagram(), IColorConstant.DARK_ORANGE));
							break;
					}
				}
			});

		}

		@Override
		public void widgetDefaultSelected(final SelectionEvent e) {
			// TODO Auto-generated method stub
		}

	};

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		TabbedPropertySheetWidgetFactory factory = getWidgetFactory();
		Composite composite = factory.createFlatFormComposite(parent);
		FormData data;

		// name text
		{
			nameText = factory.createText(composite, "");
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(50, 0);
			data.top = new FormAttachment(0, VSPACE);
			nameText.setLayoutData(data);

			CLabel valueLabel = factory.createCLabel(composite, "Name:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(nameText, HSPACE);
			data.top = new FormAttachment(0, VSPACE);
			valueLabel.setLayoutData(data);
		}

		// description text
		{
			descriptionText = factory.createText(composite, "", SWT.MULTI | SWT.V_SCROLL);
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(50, 0);
			data.top = new FormAttachment(0, 50);
			data.height = 100;
			descriptionText.setLayoutData(data);

			CLabel descriptionLabel = factory.createCLabel(composite, "Description:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(0, 80);
			data.top = new FormAttachment(descriptionText, 0, SWT.TOP);
			descriptionLabel.setLayoutData(data);
		}

		// Gateway type
		{
			gatewayType = factory.createCCombo(composite, SWT.DROP_DOWN |SWT.VERTICAL | SWT.READ_ONLY);
			gatewayType.add(GatewayType.DEFAULT.getName(), GatewayType.DEFAULT_VALUE);
			gatewayType.setData(GatewayType.DEFAULT.getName(), GatewayType.DEFAULT);
			gatewayType.add(GatewayType.EXCLUSIVE.getName(), GatewayType.EXCLUSIVE_VALUE);
			gatewayType.setData(GatewayType.EXCLUSIVE.getName(), GatewayType.EXCLUSIVE);
			gatewayType.add(GatewayType.INCLUSIVE.getName(), GatewayType.INCLUSIVE_VALUE);
			gatewayType.setData(GatewayType.INCLUSIVE.getName(), GatewayType.INCLUSIVE);
			gatewayType.add(GatewayType.PARALLEL.getName(), GatewayType.PARALLEL_VALUE);
			gatewayType.setData(GatewayType.PARALLEL.getName(), GatewayType.PARALLEL);

			data = new FormData();
			data.left = new FormAttachment(50, 95);
			data.right = new FormAttachment(95, 0);
			data.top = new FormAttachment(0, VSPACE);
			gatewayType.setLayoutData(data);

			CLabel nameLabel = factory.createCLabel(composite, "Type:");
			data = new FormData();
			data.left = new FormAttachment(50, 50);
			data.right = new FormAttachment(gatewayType, -HSPACE);
			data.top = new FormAttachment(gatewayType, 0, SWT.CENTER);
			nameLabel.setLayoutData(data);
		}
	}

	@Override
	public void refresh() {
		PictogramElement pe = getSelectedPictogramElement();
		if (pe != null) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			if (bo == null)
				return;

			String name = ((Gateway) bo).getName();
			nameText.setText(name == null ? "" : name);

			String description = ((Gateway) bo).getDescription();
			descriptionText.setText(description == null ? "" : description);

			GatewayType type = ((Gateway) bo).getGatewayType();
			gatewayType.setText(type.getName());
		}
	}

	@Override
	public void aboutToBeShown() {
		if(!nameText.isDisposed()){
			nameText.addModifyListener(nameTextListener);
			descriptionText.addModifyListener(eventDescriptionListener);
			gatewayType.addSelectionListener(taskTypeListener);
		}
	}

	@Override
	public void aboutToBeHidden() {
		if(!nameText.isDisposed()){
			nameText.removeModifyListener(nameTextListener);
			descriptionText.removeModifyListener(eventDescriptionListener);
			gatewayType.removeSelectionListener(taskTypeListener);
		}
	}
}
