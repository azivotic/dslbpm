package com.azivotic.dsl.bpm.core.properties;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.ui.platform.GFPropertySection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;

import com.azivotic.dsl.bpm.core.diagram.BpmImageProvider;
import com.azivotic.dsl.bpm.metamodel.Task;
import com.azivotic.dsl.bpm.metamodel.TaskExecutionType;
import com.azivotic.dsl.bpm.metamodel.TaskType;

public class TaskPropertyClassSection extends GFPropertySection implements ITabbedPropertyConstants {

	private org.eclipse.swt.widgets.Text nameText;
	private org.eclipse.swt.widgets.Text descriptionText;
	private org.eclipse.swt.widgets.Text assigneeText;
	private CCombo taskType;
	private CCombo taskExecution;

	private IGaService gaService = Graphiti.getGaService();

	private final ModifyListener nameTextListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			// zbog transakcije unutar transakcije prilikom poziva create metode
			if (nameText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final Task task = (Task) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(task);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newText = nameText.getText();
						task.setName(newText);
					}
				});
			}
		}
	};

	private final ModifyListener descriptionTextListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			if (descriptionText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final Task task = (Task) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(task);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newDescription = descriptionText.getText();
						task.setDescription(newDescription);
					}
				});
			}

		}
	};

	private final ModifyListener assigneeTextListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			if (assigneeText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final Task task = (Task) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(task);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newAssignee= assigneeText.getText();
						task.setAssignee(newAssignee);
					}
				});
			}

		}
	};

	private final SelectionListener taskTypeListener = new SelectionListener() {

		@Override
		public void widgetSelected(final SelectionEvent e) {
			final PictogramElement pe = getSelectedPictogramElement();
			final Task task = (Task) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

			TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(task);
			editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {

				@Override
				protected void doExecute() {
					
					String key = taskType.getText();
					TaskType type = (TaskType) taskType.getData(key);
					task.setTaskType(type);

					//Remove existing task type icons
					EList<GraphicsAlgorithm> children = pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren();
					Iterator<GraphicsAlgorithm> it = children.iterator();
					while(it.hasNext()){
						GraphicsAlgorithm child = it.next();
						if(child instanceof Image && 
								(((Image) child).getId().equals(BpmImageProvider.IMG_SERVICE_TASK) || ((Image) child).getId().equals(BpmImageProvider.IMG_USER_TASK))){
							it.remove();
						}
					}
	        		
					switch(type){

					case DEFAULT:
						break;

					case SERVICE:
//						Shape shape = peCreateService.createShape((ContainerShape)pe, false);
						Image image = gaService.createImage(pe.getGraphicsAlgorithm(), BpmImageProvider.IMG_SERVICE_TASK);
						gaService.setLocationAndSize(image, 3, 23, 16, 16);
						break;

					case USER:
//						shape = peCreateService.createShape((ContainerShape)pe, false);
						image = gaService.createImage(pe.getGraphicsAlgorithm(), BpmImageProvider.IMG_USER_TASK);
						gaService.setLocationAndSize(image, 3, 23, 16, 16);
						break;
					}
				}
			});

		}

		@Override
		public void widgetDefaultSelected(final SelectionEvent e) {
			// TODO Auto-generated method stub
		}

	};
	
	private final SelectionListener executionTypeListener = new SelectionListener() {

	    @Override
	    public void widgetSelected(final SelectionEvent e) {
	      final PictogramElement pe = getSelectedPictogramElement();
	      final Task task = (Task) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

	      TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(task);
	      editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {

	        @Override
	        protected void doExecute() {
	        	String key = taskExecution.getText();
	        	TaskExecutionType type = (TaskExecutionType) taskExecution.getData(key);
	        	task.setTaskExecutionType(type);
	        	
	        	//Remove existing task type icons
	        	EList<GraphicsAlgorithm> children = pe.getGraphicsAlgorithm().getGraphicsAlgorithmChildren();
	        	Iterator<GraphicsAlgorithm> it = children.iterator();
	        	while(it.hasNext()){
	        		GraphicsAlgorithm child = it.next();
	        		if(child instanceof Image && 
	        				(((Image) child).getId().equals(BpmImageProvider.IMG_PARALLEL_TASK) || ((Image) child).getId().equals(BpmImageProvider.IMG_LOOP_TASK))){
						it.remove();
					}
	        	}
	        	//calculate x coordinate - find task width - edge offset(3) - icon width(16)
	        	int x = pe.getGraphicsAlgorithm().getWidth() - 3 - 16;

	        	switch(type){
	        	
	        	case DEFAULT:
	        		break;
	        		
	        	case PARALLEL:
//	        		Shape shape = peCreateService.createShape((ContainerShape)pe, false);
		        	Image image = gaService.createImage(pe.getGraphicsAlgorithm(), BpmImageProvider.IMG_PARALLEL_TASK);
		        	gaService.setLocationAndSize(image, x, 23, 16, 16);
	        		break;
	        		
	        	case LOOP:
//	        		shape = peCreateService.createShape((ContainerShape)pe, false);
		        	image = gaService.createImage(pe.getGraphicsAlgorithm(), BpmImageProvider.IMG_LOOP_TASK);
			        gaService.setLocationAndSize(image, x, 23, 16, 16);
	        		break;
	        	}
	        }
	      });

	    }

	    @Override
	    public void widgetDefaultSelected(final SelectionEvent e) {
	      // TODO Auto-generated method stub
	    }

	  };

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		TabbedPropertySheetWidgetFactory factory = getWidgetFactory();
		Composite composite = factory.createFlatFormComposite(parent);
		FormData data;

		// Name text
		{
			nameText = factory.createText(composite, "");
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(45, 0);
			data.top = new FormAttachment(0, VSPACE);
			nameText.setLayoutData(data);

			CLabel nameLabel = factory.createCLabel(composite, "Name:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(nameText, HSPACE);
			data.top = new FormAttachment(0, VSPACE);
			nameLabel.setLayoutData(data);
		}
		// Description text
		{
			descriptionText = factory.createText(composite, "", SWT.MULTI | SWT.V_SCROLL);
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(45, 0);
			data.top = new FormAttachment(0, 50);
			data.height = 100;
			descriptionText.setLayoutData(data);

			CLabel descriptionLabel = factory.createCLabel(composite, "Description:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(0, 80);
			data.top = new FormAttachment(descriptionText, 0, SWT.TOP);
			descriptionLabel.setLayoutData(data);
		}
		//Assignee text
		{
			assigneeText = factory.createText(composite, "");
			data = new FormData();
			data.left = new FormAttachment(50, 110);
			data.right = new FormAttachment(95, 0);
			data.top = new FormAttachment(0, VSPACE);
			assigneeText.setLayoutData(data);

			CLabel assigneeLabel = factory.createCLabel(composite, "Assignee:");
			data = new FormData();
			data.left = new FormAttachment(45, 45);
			data.right = new FormAttachment(assigneeText, HSPACE);
			data.top = new FormAttachment(0, VSPACE);
			assigneeLabel.setLayoutData(data);
		}
		// Task type
		{
			taskType = factory.createCCombo(composite, SWT.DROP_DOWN |SWT.VERTICAL | SWT.READ_ONLY);
			taskType.add(TaskType.DEFAULT.getName(), TaskType.DEFAULT_VALUE);
			taskType.setData(TaskType.DEFAULT.getName(), TaskType.DEFAULT);
			taskType.add(TaskType.SERVICE.getName(), TaskType.SERVICE_VALUE);
			taskType.setData(TaskType.SERVICE.getName(), TaskType.SERVICE);
			taskType.add(TaskType.USER.getName(), TaskType.USER_VALUE);
			taskType.setData(TaskType.USER.getName(), TaskType.USER);

			data = new FormData();
			data.left = new FormAttachment(50, 110);
			data.right = new FormAttachment(95, 0);
			data.top = new FormAttachment(0, 50);
			taskType.setLayoutData(data);

			CLabel typeLabel = factory.createCLabel(composite, "Task type:");
			data = new FormData();
			data.left = new FormAttachment(45, 45);
			data.right = new FormAttachment(taskType, HSPACE);
			data.top = new FormAttachment(taskType, 0, SWT.CENTER);
			typeLabel.setLayoutData(data);
		}
		// Task execution type
		{
			taskExecution = factory.createCCombo(composite, SWT.DROP_DOWN |SWT.VERTICAL | SWT.READ_ONLY);
			taskExecution.add(TaskExecutionType.DEFAULT.getName(), TaskExecutionType.DEFAULT_VALUE);
			taskExecution.setData(TaskExecutionType.DEFAULT.getName(), TaskExecutionType.DEFAULT);
			taskExecution.add(TaskExecutionType.PARALLEL.getName(), TaskExecutionType.PARALLEL_VALUE);
			taskExecution.setData(TaskExecutionType.PARALLEL.getName(), TaskExecutionType.PARALLEL);
			taskExecution.add(TaskExecutionType.LOOP.getName(), TaskExecutionType.LOOP_VALUE);
			taskExecution.setData(TaskExecutionType.LOOP.getName(), TaskExecutionType.LOOP);

			data = new FormData();
			data.left = new FormAttachment(50, 110);
			data.right = new FormAttachment(95, 0);
			data.top = new FormAttachment(0, 100);
			taskExecution.setLayoutData(data);

			CLabel executionTypeLabel = factory.createCLabel(composite, "Execution type:");
			data = new FormData();
			data.left = new FormAttachment(45, 45);
			data.right = new FormAttachment(taskExecution, HSPACE);
			data.top = new FormAttachment(taskExecution, 0, SWT.CENTER);
			executionTypeLabel.setLayoutData(data);
		}
	}

	@Override
	public void refresh() {
		PictogramElement pe = getSelectedPictogramElement();
		if (pe != null) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			if (bo == null)
				return;

			String name = ((Task) bo).getName();
			nameText.setText(name == null ? "" : name);

			String description = ((Task) bo).getDescription();
			descriptionText.setText(description == null ? "" : description);

			String assignee = ((Task) bo).getAssignee();
			assigneeText.setText(assignee == null ? "" : assignee);

			TaskType type = ((Task) bo).getTaskType();
			taskType.setText(type.getName());

			TaskExecutionType executionType = ((Task) bo).getTaskExecutionType();
			taskExecution.setText(executionType.getName());
		}
	}

	@Override
	public void aboutToBeShown() {
		if(!nameText.isDisposed()){
			nameText.addModifyListener(nameTextListener);
			descriptionText.addModifyListener(descriptionTextListener);
			assigneeText.addModifyListener(assigneeTextListener);
			taskType.addSelectionListener(taskTypeListener);
			taskExecution.addSelectionListener(executionTypeListener);
		}
	}

	@Override
	public void aboutToBeHidden() {
		if(!nameText.isDisposed()){
			nameText.removeModifyListener(nameTextListener);
			descriptionText.removeModifyListener(descriptionTextListener);
			assigneeText.addModifyListener(assigneeTextListener);
			taskType.removeSelectionListener(taskTypeListener);
			taskExecution.removeSelectionListener(executionTypeListener);
		}
	}
}

