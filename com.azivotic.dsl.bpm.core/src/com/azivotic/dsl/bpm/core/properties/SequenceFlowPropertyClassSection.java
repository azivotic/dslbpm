package com.azivotic.dsl.bpm.core.properties;

import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;
import org.eclipse.graphiti.ui.platform.GFPropertySection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertyConstants;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetWidgetFactory;

import com.azivotic.dsl.bpm.metamodel.SequenceFlow;

public class SequenceFlowPropertyClassSection  extends GFPropertySection implements ITabbedPropertyConstants {

	private org.eclipse.swt.widgets.Text nameText;
	private org.eclipse.swt.widgets.Text descriptionText;

	IPeCreateService peCreateService = Graphiti.getPeCreateService();
	IGaService gaService = Graphiti.getGaService();

	private final ModifyListener nameTextListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			// zbog transakcije unutar transakcije prilikom poziva create metode
			if (nameText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final SequenceFlow sequenceFlow = (SequenceFlow) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(sequenceFlow);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newText = nameText.getText();
						sequenceFlow.setName(newText);
					}
				});
			}
		}
	};

	private final ModifyListener eventDescriptionListener = new ModifyListener() {

		@Override
		public void modifyText(final ModifyEvent e) {
			if (descriptionText.isFocusControl()) {
				final PictogramElement pe = getSelectedPictogramElement();
				final SequenceFlow sequenceFlow = (SequenceFlow) Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);

				TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(sequenceFlow);
				editingDomain.getCommandStack().execute(new RecordingCommand(editingDomain) {
					@Override
					protected void doExecute() {
						String newDescription = descriptionText.getText();
						sequenceFlow.setDescription(newDescription);
					}
				});
			}

		}
	};

	@Override
	public void createControls(Composite parent, TabbedPropertySheetPage tabbedPropertySheetPage) {
		super.createControls(parent, tabbedPropertySheetPage);

		TabbedPropertySheetWidgetFactory factory = getWidgetFactory();
		Composite composite = factory.createFlatFormComposite(parent);
		FormData data;

		//name text
		{
			nameText = factory.createText(composite, "");
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(50, 0);
			data.top = new FormAttachment(0, VSPACE);
			nameText.setLayoutData(data);

			CLabel valueLabel = factory.createCLabel(composite, "Name:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(nameText, HSPACE);
			data.top = new FormAttachment(0, VSPACE);
			valueLabel.setLayoutData(data);
		}
		// description text
		{
			descriptionText = factory.createText(composite, "", SWT.MULTI | SWT.V_SCROLL);
			data = new FormData();
			data.left = new FormAttachment(0, STANDARD_LABEL_WIDTH);
			data.right = new FormAttachment(50, 0);
			data.top = new FormAttachment(0, 50);
			data.height = 100;
			descriptionText.setLayoutData(data);

			CLabel descriptionLabel = factory.createCLabel(composite, "Description:");
			data = new FormData();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment(0, 80);
			data.top = new FormAttachment(descriptionText, 0, SWT.TOP);
			descriptionLabel.setLayoutData(data);
		}
	}

	@Override
	public void refresh() {
		PictogramElement pe = getSelectedPictogramElement();
		if (pe != null) {
			Object bo = Graphiti.getLinkService().getBusinessObjectForLinkedPictogramElement(pe);
			if (bo == null)
				return;

			String name = ((SequenceFlow) bo).getName();
			nameText.setText(name == null ? "" : name);

			String description = ((SequenceFlow) bo).getDescription();
			descriptionText.setText(description == null ? "" : description);
			
		}
	}

	@Override
	public void aboutToBeShown() {
		if(!nameText.isDisposed()){
			nameText.addModifyListener(nameTextListener);
			descriptionText.addModifyListener(eventDescriptionListener);
		}
	}

	@Override
	public void aboutToBeHidden() {
		if(!nameText.isDisposed()){
			nameText.removeModifyListener(nameTextListener);
			descriptionText.removeModifyListener(eventDescriptionListener);
		}
	}
}

