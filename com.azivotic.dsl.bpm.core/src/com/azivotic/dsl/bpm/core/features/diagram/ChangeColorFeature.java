package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.graphiti.examples.common.ExampleUtil;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.styles.Color;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.util.ColorConstant;

import com.azivotic.dsl.bpm.metamodel.Task;

public class ChangeColorFeature extends AbstractCustomFeature {

	public ChangeColorFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public String getName() {
		return "Change Task color";
	}

	@Override
	public String getDescription() {
		return "Change elemnt color color";
	}

	@Override
	public boolean canExecute(ICustomContext context) {
		PictogramElement[] pes = context.getPictogramElements();
		if (pes == null || pes.length == 0) { // nothing selected
			return false;
		}
		// return true, if all elements are Task
		for (PictogramElement pe : pes) {
			final Object bo = getBusinessObjectForPictogramElement(pe);
			if (!(bo instanceof Task)) {
				return false;
			}
		}
		return true;
	}

	public void execute(ICustomContext context) {
		PictogramElement[] pes = context.getPictogramElements();
		Color currentColor = pes[0].getGraphicsAlgorithm().getBackground();
		Color newBackgroundColor = ExampleUtil.editColor(currentColor);
	
		if (newBackgroundColor == null) { // user did not choose new color
			return;
		}
		
		//calculate color for outline
		int r = newBackgroundColor.getRed() - 89 > 0 ? newBackgroundColor.getRed() - 89 : 0;
		int g = newBackgroundColor.getGreen() - 87 > 0 ? newBackgroundColor.getGreen() - 89 : 0;
		int b = newBackgroundColor.getBlue() - 80 > 0 ? newBackgroundColor.getBlue() - 89 : 0;
		ColorConstant newForegroundColor = new ColorConstant(r, g, b);
		
		//iterate over all selected elements
		for (PictogramElement pe : pes) {
			final Object bo = getBusinessObjectForPictogramElement(pe);
			if (bo instanceof Task) {
				pe.getGraphicsAlgorithm().setBackground(newBackgroundColor);
				pe.getGraphicsAlgorithm().setForeground(manageColor(newForegroundColor));
				//change color of name line
				ContainerShape container = (ContainerShape) pe;
				if(container != null && container.getChildren().size() > 0){
					for(Shape shape : container.getChildren()){
						if(shape.getGraphicsAlgorithm() instanceof Polyline){
							shape.getGraphicsAlgorithm().setForeground(manageColor(newForegroundColor));
						}
					}
				}
			}
		}
		
	}
}
