package com.azivotic.dsl.bpm.core.features;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.context.impl.CreateConnectionContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.AnchorContainer;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;

import com.azivotic.dsl.bpm.core.diagram.BpmImageProvider;
import com.azivotic.dsl.bpm.core.util.Util;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.MetamodelFactory;
import com.azivotic.dsl.bpm.metamodel.Task;

public class CreateTaskFeature extends AbstractCreateFeature{

	private final static String DEFAULT_NAME = "Task";
	
	public CreateTaskFeature(IFeatureProvider fp) {
		super(fp, "Task", "Create Task");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		return (container instanceof Lane || context.getTargetContainer() instanceof Diagram);
	}

	@Override
	public Object[] create(ICreateContext context) {
		Task newClass = MetamodelFactory.eINSTANCE.createTask();
		newClass.setName(DEFAULT_NAME);
		
		try {
			Util.saveToModelFile(newClass, getDiagram());
		} catch (CoreException | IOException e) {
			e.printStackTrace();
		}
		
		//Creating task from context menu button with sequence flow
		if(context.getProperty("contextButtonConnection")!=null){
			context.putProperty("xcoordinate", context.getProperty("xcoordinate"));
			context.putProperty("ycoordinate", context.getProperty("ycoordinate"));
			
			PictogramElement element = addGraphicalRepresentation(context, newClass);
			CreateConnectionContext connectionContext = (CreateConnectionContext) context.getProperty("contextButtonConnection");
			connectionContext.setTargetPictogramElement(element);
			connectionContext.setTargetAnchor(Graphiti.getPeService().getChopboxAnchor((AnchorContainer) element));
			CreateSequenceFlowFeature sequenceFeature = new CreateSequenceFlowFeature(getFeatureProvider());
			sequenceFeature.create(connectionContext);
			
		} else {
			addGraphicalRepresentation(context, newClass);
		}
 
        getFeatureProvider().getDirectEditingInfo().setActive(true);
		// return newly created business object(s)
        return new Object[] { newClass };
	}
	
	@Override
	public String getCreateImageId() {
		return BpmImageProvider.IMG_PALETTE_TASK;
	}

}
