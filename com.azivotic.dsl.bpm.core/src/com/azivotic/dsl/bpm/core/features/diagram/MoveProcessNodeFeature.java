package com.azivotic.dsl.bpm.core.features.diagram;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.impl.DefaultMoveShapeFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.Shape;

import com.azivotic.dsl.bpm.core.util.Util;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.ProcessNode;

public class MoveProcessNodeFeature extends DefaultMoveShapeFeature {

	public MoveProcessNodeFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canMoveShape(IMoveShapeContext context) {
		boolean canMove = super.canMoveShape(context);
		Shape shape = context.getShape();
		Object bo = getBusinessObjectForPictogramElement(shape);
		if (bo instanceof ProcessNode) {
			canMove = true;
		}

		return canMove;
	}

	@Override
	protected void postMoveShape(IMoveShapeContext context) {
		final Shape shape = context.getShape();
		final ProcessNode object = (ProcessNode) getBusinessObjectForPictogramElement(shape);

		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		// if process node is moved to lane
		if(container != null && container instanceof Lane){
			Lane current = object.getLane();
			//if new lane is equal old lane
			layoutPictogramElement(context.getTargetContainer());
			if(current != null && current.equals(container)){
				return;
			} else {
				object.setLane((Lane)container);
			}
		//if process node is moved to diagram
		} else if(context.getTargetContainer() instanceof Diagram && object.getLane() != null){
			//u ovom trenutku obri�e element iz modela pa zato pozivamo save to model file ispod
			object.setLane(null);
			try {
				Util.saveToModelFile(object, getDiagram());
			} catch (CoreException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
