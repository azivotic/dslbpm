package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.graphiti.features.IDeleteFeature;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IDeleteContext;
import org.eclipse.graphiti.features.context.impl.DeleteContext;
import org.eclipse.graphiti.features.context.impl.MultiDeleteInfo;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IPeService;
import org.eclipse.graphiti.ui.features.DefaultDeleteFeature;

import com.azivotic.dsl.bpm.metamodel.ProcessNode;

public class DeleteLaneFeature extends DefaultDeleteFeature {

	public DeleteLaneFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public void preDelete(IDeleteContext context) {
		super.preDelete(context);

		IPeService peService = Graphiti.getPeService();
		PictogramElement pe = context.getPictogramElement();
		if (!(pe instanceof ContainerShape))
			return;
		
		for (PictogramElement child : peService.getPictogramElementChildren(pe)) {
			if(getBusinessObjectForPictogramElement(child) instanceof ProcessNode){
				DeleteContext ctx = new DeleteContext(child);
				ctx.setMultiDeleteInfo(new MultiDeleteInfo(false, false, 1));
				IDeleteFeature deleteFeature = getFeatureProvider().getDeleteFeature(ctx);
				if (deleteFeature != null) {
					deleteFeature.delete(ctx);
				}
			}
		}
	}

}
