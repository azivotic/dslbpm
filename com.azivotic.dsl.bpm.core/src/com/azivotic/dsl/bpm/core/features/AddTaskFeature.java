package com.azivotic.dsl.bpm.core.features;

import org.eclipse.graphiti.features.IDirectEditingInfo;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;

import com.azivotic.dsl.bpm.core.util.StyleUtil;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.Task;

public class AddTaskFeature extends AbstractAddShapeFeature {

	public AddTaskFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canAdd(IAddContext context) {
		Object newObject = context.getNewObject();
		if (newObject instanceof Task) {
			Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
			if (container instanceof Lane || context.getTargetContainer() instanceof Diagram) {
				return true;
			}
		}
		return false;
	}

	@Override
	public PictogramElement add(IAddContext context) {
		IPeCreateService peCreateService = Graphiti.getPeCreateService();
		IGaService gaService = Graphiti.getGaService();

		Task addedClass = (Task) context.getNewObject();
		//Set model structure if Lane is the parent for the task
		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		if(container instanceof Lane){
			Lane lane = (Lane)container;
			lane.getProcessNodeRefs().add(addedClass);
		}

		ContainerShape targetShape = (ContainerShape) context.getTargetContainer();
		ContainerShape containerShape = peCreateService.createContainerShape(targetShape, true);

		final int width = context.getWidth() <= 0 ? 100 : context.getWidth();
		final int height = context.getHeight() <= 0 ? 50 : context.getHeight();
		//check if task is created from context button, if so create it beside previous element | add 25 offset for lane name
		final int x = context.getProperty("xcoordinate")!=null ? (Integer)context.getProperty("xcoordinate") + 150 : context.getX() + 25;
		final int y = context.getProperty("ycoordinate")!=null ? (Integer)context.getProperty("ycoordinate") : context.getY();
		//SHAPE WITH ROUNDED RECTANGLE
		RoundedRectangle roundedRectangle; 
		{
			roundedRectangle = gaService.createPlainRoundedRectangle(containerShape, 5, 5);
			roundedRectangle.setFilled(true);
			roundedRectangle.setLineWidth(2);
			roundedRectangle.setForeground(gaService.manageColor(getDiagram(), StyleUtil.DARK_BLUE));
			roundedRectangle.setBackground(gaService.manageColor(getDiagram(), StyleUtil.BLUE));
			gaService.setLocationAndSize(roundedRectangle, x, y, width, height);
			// create link and wire it
			link(containerShape, addedClass);
		}
		// SHAPE WITH LINE
		{
			Shape shape = peCreateService.createShape(containerShape, false);
			Polyline polyline = gaService.createPolyline(shape, new int[] { 0, 20, width, 20 });
			polyline.setForeground(manageColor(StyleUtil.DARK_BLUE));
			polyline.setLineWidth(2);
		}
		// SHAPE WITH TEXT
		{
			Shape shape = peCreateService.createShape(containerShape, false);
			Text text = gaService.createPlainText(shape, addedClass.getName());
			text.setStyle(StyleUtil.getStyleForText(getDiagram()));
			gaService.setLocationAndSize(text, 0, 0, width, 20);
			// create link and wire it
			link(shape, addedClass);
			//direct editing feature
			IDirectEditingInfo directEditingInfo = getFeatureProvider().getDirectEditingInfo();
			// set container shape for direct editing after object creation
			directEditingInfo.setMainPictogramElement(containerShape);
			// set shape and graphics algorithm where the editor for direct editing shall be opened after object creation
			directEditingInfo.setPictogramElement(shape);
			directEditingInfo.setGraphicsAlgorithm(text);
		}
		// add a chopbox anchor to the shape 
		peCreateService.createChopboxAnchor(containerShape);
		// call the layout feature
		layoutPictogramElement(containerShape);
		return containerShape;
	}
}
