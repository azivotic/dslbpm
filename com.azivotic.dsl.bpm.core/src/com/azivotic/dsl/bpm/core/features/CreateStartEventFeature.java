package com.azivotic.dsl.bpm.core.features;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import com.azivotic.dsl.bpm.core.diagram.BpmImageProvider;
import com.azivotic.dsl.bpm.core.util.Util;
import com.azivotic.dsl.bpm.metamodel.Event;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.MetamodelFactory;

public class CreateStartEventFeature extends AbstractCreateFeature{

	public CreateStartEventFeature(IFeatureProvider fp) {
		super(fp, "Start Event", "Create Start Event");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		return (container instanceof Lane || context.getTargetContainer() instanceof Diagram);
	}

	@Override
	public Object[] create(ICreateContext context) {
		Event newClass = MetamodelFactory.eINSTANCE.createStartEvent();

		try {
			Util.saveToModelFile(newClass, getDiagram());
		} catch (CoreException | IOException e) {
			e.printStackTrace();
		}
		// do the add
		addGraphicalRepresentation(context, newClass);

		// return newly created business object(s)
		return new Object[] { newClass };
	}

	@Override
	public String getCreateImageId() {
		return BpmImageProvider.IMG_PALETTE_START_EVENT;
	}

}