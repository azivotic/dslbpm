package com.azivotic.dsl.bpm.core.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;

import com.azivotic.dsl.bpm.core.diagram.BpmImageProvider;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.StartEvent;

public class AddStartEventFeature extends AbstractAddShapeFeature{
	
    public static final int IMAGE_SIZE = 48;

	public AddStartEventFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canAdd(IAddContext context) {
		Object newObject = context.getNewObject();
		if (newObject instanceof StartEvent) {
			Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
			if (container instanceof Lane || context.getTargetContainer() instanceof Diagram) {
				return true;
			}
		}
		return false;
	}

	@Override
	public PictogramElement add(IAddContext context) {
		
		IPeCreateService peCreateService = Graphiti.getPeCreateService();
		IGaService gaService = Graphiti.getGaService();
		
		StartEvent addedClass = (StartEvent) context.getNewObject();
		//Set model structure, Lane is the parent for the task
		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		if(container instanceof Lane){
			Lane lane = (Lane)container;
			lane.getProcessNodeRefs().add(addedClass);
		}
		
		ContainerShape targetShape = context.getTargetContainer();
        ContainerShape containerShape = peCreateService.createContainerShape(targetShape, true);
        
        final int width = IMAGE_SIZE;
		final int height = IMAGE_SIZE;
		
		{
			//ICON
			final Rectangle invisibleRectangle = gaService.createInvisibleRectangle(containerShape);
	        gaService.setLocationAndSize(invisibleRectangle, context.getX() + 25, context.getY(), width, height);
	        
	        final Shape imageShape = peCreateService.createShape(containerShape, false);
	        final Image image = gaService.createImage(imageShape, BpmImageProvider.IMG_START_EVENT);
	        gaService.setLocationAndSize(image, 0, 0, IMAGE_SIZE, IMAGE_SIZE);
	        link(containerShape, addedClass);
		}
		// add a chopbox anchor to the shape - center of the shape
        peCreateService.createChopboxAnchor(containerShape);
  
        // call the layout feature
        layoutPictogramElement(containerShape);
        //layout Lane
        if(container instanceof Lane){
        	layoutPictogramElement(context.getTargetContainer());
        }
		return containerShape;
	}
}
