package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IResizeShapeContext;
import org.eclipse.graphiti.features.impl.DefaultResizeShapeFeature;
import org.eclipse.graphiti.mm.pictograms.Shape;

import com.azivotic.dsl.bpm.metamodel.Event;
import com.azivotic.dsl.bpm.metamodel.Gateway;

public class ResizeFeature extends DefaultResizeShapeFeature {
    
    public ResizeFeature(IFeatureProvider fp) {
        super(fp);
    }
 
    @Override
    public boolean canResizeShape(IResizeShapeContext context) {
        boolean canResize = super.canResizeShape(context);
        // perform further check only if move allowed by default feature
        if (canResize) {
            Shape shape = context.getShape();
            Object bo = getBusinessObjectForPictogramElement(shape);
            if (bo instanceof Event || bo instanceof Gateway) {
            	canResize = false;
            }
        }
        return canResize;
    }
}