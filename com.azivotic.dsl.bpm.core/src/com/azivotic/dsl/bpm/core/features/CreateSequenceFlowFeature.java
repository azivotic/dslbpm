package com.azivotic.dsl.bpm.core.features;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateConnectionContext;
import org.eclipse.graphiti.features.context.impl.AddConnectionContext;
import org.eclipse.graphiti.features.impl.AbstractCreateConnectionFeature;
import org.eclipse.graphiti.mm.pictograms.Anchor;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import com.azivotic.dsl.bpm.core.diagram.BpmImageProvider;
import com.azivotic.dsl.bpm.core.util.Util;
import com.azivotic.dsl.bpm.metamodel.EndEvent;
import com.azivotic.dsl.bpm.metamodel.MetamodelFactory;
import com.azivotic.dsl.bpm.metamodel.ProcessNode;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.StartEvent;

public class CreateSequenceFlowFeature extends AbstractCreateConnectionFeature {
	
	private final static String DEFAULT_NAME = "";

	public CreateSequenceFlowFeature (IFeatureProvider fp) {
		super(fp, "Sequence flow", "Create sequence flow");
	}

	/**
	 * check if element are not the same(start!=end) and if type is ProcessNode
	 */
	public boolean canCreate(ICreateConnectionContext context) {
		ProcessNode source = (ProcessNode) getBusinessObject(context.getSourceAnchor());
		ProcessNode target = (ProcessNode) getBusinessObject(context.getTargetAnchor());
		if (source != null && target != null && source != target && !(getBusinessObject(context.getTargetAnchor()) instanceof StartEvent)) {
			return true;
		}
		return false;
	}

	public boolean canStartConnection(ICreateConnectionContext context) {
		Object ob = getBusinessObject(context.getSourceAnchor());
		if (ob!=null && !(ob instanceof EndEvent)) {
			return true;
		}
		return false;
	}

	public Connection create(ICreateConnectionContext context) {
		Connection newConnection = null;

		ProcessNode source = (ProcessNode) getBusinessObject(context.getSourceAnchor());
		ProcessNode target = (ProcessNode) getBusinessObject(context.getTargetAnchor());

		if (source != null && target != null) {
			// create new business object 
			SequenceFlow sequenceFlow = createSequenceFlow(source, target);
			
			// add connection for business object
			AddConnectionContext addContext = new AddConnectionContext(context.getSourceAnchor(), context.getTargetAnchor());
			addContext.setNewObject(sequenceFlow);
			
			newConnection = (Connection) getFeatureProvider().addIfPossible(addContext);
			
			try {
				Util.saveToModelFile(sequenceFlow, getDiagram());
			} catch (CoreException | IOException e) {
				e.printStackTrace();
			}
		}

		return newConnection;
	}

	/**
	 * Returns the ProcessNode belonging to the anchor, or null if not available.
	 */
	private Object getBusinessObject(Anchor sourceAnchor) {
		if (sourceAnchor != null) {
		    PictogramElement peSrc = sourceAnchor.getParent();
		    Object bo = getFeatureProvider().getBusinessObjectForPictogramElement(peSrc);
		    if (bo instanceof ProcessNode)
		    	return bo;
		    }
		return null;
	  }

	/**
	 * Create connections between to elements
	 * set source and target (references are automaticly added to ProcessNodes - xcore opposite feature)
	 * @param source
	 * @param target
	 * @return
	 */
	private SequenceFlow createSequenceFlow(ProcessNode source, ProcessNode target) {
		SequenceFlow sequenceFlow = MetamodelFactory.eINSTANCE.createSequenceFlow();
		sequenceFlow.setName(DEFAULT_NAME);
		sequenceFlow.setSourceRef(source);
		sequenceFlow.setTargetRef(target);;
		getDiagram().eResource().getContents().add(sequenceFlow);
		return sequenceFlow;
	}
	
	@Override
	public String getCreateImageId() {
		return BpmImageProvider.IMG_PALETTE_CONNECION;
	}
}
