package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.datatypes.IDimension;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Image;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;

import com.azivotic.dsl.bpm.core.diagram.BpmImageProvider;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.Task;

public class LayoutTaskFeature extends AbstractLayoutFeature {

	private static final int MIN_HEIGHT = 50;

	private static final int MIN_WIDTH = 100;

	public LayoutTaskFeature(IFeatureProvider fp) {
		super(fp);
	}

	public boolean canLayout(ILayoutContext context) {
		PictogramElement pe = context.getPictogramElement();
		if (!(pe instanceof ContainerShape))
			return false;

		EList<EObject> businessObjects = pe.getLink().getBusinessObjects();
		return businessObjects.size() == 1
				&& businessObjects.get(0) instanceof Task;
	}

	public boolean layout(ILayoutContext context) {
		boolean anythingChanged = false;
		ContainerShape containerShape = (ContainerShape) context.getPictogramElement();
		GraphicsAlgorithm containerGa = containerShape.getGraphicsAlgorithm();
		IGaService gaService = Graphiti.getGaService();

		// height
		if (containerGa.getHeight() < MIN_HEIGHT) {
			containerGa.setHeight(MIN_HEIGHT);
			anythingChanged = true;
		}

		// width
		if (containerGa.getWidth() < MIN_WIDTH) {
			containerGa.setWidth(MIN_WIDTH);
			anythingChanged = true;
		}

		int containerWidth = containerGa.getWidth();

		for (Shape shape : containerShape.getChildren()){
			GraphicsAlgorithm graphicsAlgorithm = shape.getGraphicsAlgorithm();


			IDimension size = gaService.calculateSize(graphicsAlgorithm);
			if (containerWidth != size.getWidth()) {
				if (graphicsAlgorithm instanceof Polyline) {
					Polyline polyline = (Polyline) graphicsAlgorithm;
					Point secondPoint = polyline.getPoints().get(1);
					Point newSecondPoint = gaService.createPoint(containerWidth, secondPoint.getY()); 
					polyline.getPoints().set(1, newSecondPoint);
					anythingChanged = true;
				} else {
					gaService.setWidth(graphicsAlgorithm, containerWidth);
					anythingChanged = true;
				}
			}
		}

		for(GraphicsAlgorithm ga : containerShape.getGraphicsAlgorithm().getGraphicsAlgorithmChildren()){
			if(ga instanceof Image && 
					(((Image) ga).getId().equals(BpmImageProvider.IMG_PARALLEL_TASK) || ((Image) ga).getId().equals(BpmImageProvider.IMG_LOOP_TASK))){
				Image image = (Image) ga;
				gaService.setLocationAndSize(image, containerWidth - 3 - image.getWidth(), image.getY(), image.getWidth(), image.getHeight());
				anythingChanged = true;
			} else if(ga instanceof Image && 
					(((Image) ga).getId().equals(BpmImageProvider.IMG_SERVICE_TASK) || ((Image) ga).getId().equals(BpmImageProvider.IMG_USER_TASK))){
				Image image = (Image) ga;
				gaService.setLocationAndSize(image, 3, 23, image.getWidth(), image.getHeight());
				anythingChanged = true;
			}
		}	
		//ako smo radili ne�to sa taskom a on je u lane uraditi layout tog lane-a
		Lane lane = ((Task)getBusinessObjectForPictogramElement(containerShape)).getLane();
		if(lane != null){
			layoutPictogramElement(getFeatureProvider().getPictogramElementForBusinessObject(lane));
		}
		
		return anythingChanged;
	}
}
