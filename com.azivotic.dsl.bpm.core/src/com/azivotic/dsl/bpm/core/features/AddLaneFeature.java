package com.azivotic.dsl.bpm.core.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;

import com.azivotic.dsl.bpm.core.util.StyleUtil;
import com.azivotic.dsl.bpm.metamodel.Lane;

public class AddLaneFeature extends AbstractAddShapeFeature {

	protected static final int HEIGHT = 150;
	
	protected static final int WIDTH = 800;
	
	public AddLaneFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canAdd(IAddContext context) {
		Object newObject = context.getNewObject();
		if (newObject instanceof Lane && context.getTargetContainer() instanceof Diagram) {
				return true;
		}
		return false;
	}

	@Override
	public PictogramElement add(IAddContext context) {
		IPeCreateService peCreateService = Graphiti.getPeCreateService();
		IGaService gaService = Graphiti.getGaService();

		Lane addedClass = (Lane) context.getNewObject();

		ContainerShape targetShape = (ContainerShape) context.getTargetContainer();
		ContainerShape containerShape = peCreateService.createContainerShape(targetShape, true);

		int x = context.getX();
		int y = context.getY();
		int width = context.getWidth() <= 0 ? WIDTH : context.getWidth();
		int height = context.getHeight() <= 0 ? HEIGHT : context.getHeight();
		
		//SHAPE WITH ROUNDED RECTANGLE
		RoundedRectangle rectangle; 
		{
			rectangle = gaService.createPlainRoundedRectangle(containerShape, 5, 5);
			rectangle.setStyle(StyleUtil.getStyleForLane(getDiagram()));
			gaService.setLocationAndSize(rectangle, x, y, width, height);
			// create link and wire it
			link(containerShape, addedClass);
		}
		// SHAPE WITH LINE
		{
			Shape shape = peCreateService.createShape(containerShape, false);
			Polyline polyline = gaService.createPolyline(shape, new int[] { 20, 0, 20, HEIGHT });
			polyline.setForeground(manageColor(StyleUtil.BLACK));
		}
		// SHAPE WITH TEXT
		{
			Shape shape = peCreateService.createShape(containerShape, false);
			Text text = gaService.createText(shape, addedClass.getName());
			text.setForeground(manageColor(StyleUtil.BLACK));
			text.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER ); 
			text.setFont(gaService.manageDefaultFont(getDiagram(), false, true));
			text.setAngle(270);
			gaService.setLocationAndSize(text, 0, 0, 20, HEIGHT);
			// create link and wire it
			link(shape, addedClass);
		}
		// call the layout feature
		layoutPictogramElement(containerShape);
		return containerShape;
	}
}

