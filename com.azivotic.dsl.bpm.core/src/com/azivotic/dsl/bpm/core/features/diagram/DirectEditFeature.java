package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IDirectEditingContext;
import org.eclipse.graphiti.features.impl.AbstractDirectEditingFeature;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.MultiText;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;

import com.azivotic.dsl.bpm.metamodel.Comment;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.Task;

public class DirectEditFeature extends AbstractDirectEditingFeature {

	public DirectEditFeature(IFeatureProvider fp) {
		super(fp);
	}

	public int getEditingType() {
		// there are several possible editor-types supported: text-field, checkbox, color-chooser, combobox...
		return TYPE_TEXT;
	}

	@Override
	public boolean canDirectEdit(IDirectEditingContext context) {
		PictogramElement pe = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pe);
		GraphicsAlgorithm ga = context.getGraphicsAlgorithm();
		// support direct editing, if it is a Task or Sequence Flow, and the user clicked directly on the text and not somewhere else in the rectangle
		if (bo instanceof Task && ga instanceof Text) {
			return true;
		}
		if(bo instanceof SequenceFlow){
			return true;
		}
		if(bo instanceof Comment && ga instanceof MultiText){
			return true;
		}
			
		// direct editing not supported in all other cases
		return false;
	}

	public String getInitialValue(IDirectEditingContext context) {
		// return the current name of the Task or SequenceFlow
		PictogramElement pe = context.getPictogramElement();
		Object ob = getBusinessObjectForPictogramElement(pe);
		if(ob instanceof Task){
			Task task = (Task)getBusinessObjectForPictogramElement(pe);
			return task.getName();
		}
		if(ob instanceof SequenceFlow){
			SequenceFlow sequenceFlow = (SequenceFlow)getBusinessObjectForPictogramElement(pe);
			return sequenceFlow.getName();
		}
		if(ob instanceof Comment){
			Comment comment = (Comment)getBusinessObjectForPictogramElement(pe);
			return comment.getDescription();
		}
		return "value";
		
	}

	@Override
	public String checkValueValid(String value, IDirectEditingContext context) {
		if (value.length() < 1)
			return "Please enter any text as Object name.";
		if (value.contains("\n"))
			return "Line breakes are not allowed in Object names.";
		// null means, that the value is valid
		return null;
	}

	public void setValue(String value, IDirectEditingContext context) {
		// set the new name for the Task
		PictogramElement pe = context.getPictogramElement();
		Object ob = getBusinessObjectForPictogramElement(pe);
		if(ob instanceof Task){
			Task task = (Task)getBusinessObjectForPictogramElement(pe);
			task.setName(value);
		}
		if(ob instanceof SequenceFlow){
			SequenceFlow sequenceFlow = (SequenceFlow)getBusinessObjectForPictogramElement(pe);
			sequenceFlow.setName(value);
		}
		if(ob instanceof Comment){
			Comment comment = (Comment)getBusinessObjectForPictogramElement(pe);
			comment.setDescription(value);
		}
		// we know, that pe is the Shape of the Text, so its container is the main shape of the Task
		updatePictogramElement(((Shape) pe).getContainer());
	}
}
