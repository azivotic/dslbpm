package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.graphiti.datatypes.IDimension;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ILayoutContext;
import org.eclipse.graphiti.features.impl.AbstractLayoutFeature;
import org.eclipse.graphiti.mm.algorithms.GraphicsAlgorithm;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.Point;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;

import com.azivotic.dsl.bpm.metamodel.Lane;

public class LayoutLaneFeature extends AbstractLayoutFeature {

	private static final int MIN_HEIGHT = 150;

	private static final int MIN_WIDTH = 600;

	private static final int LANE_OFFSET = 25;
	
	private static final int OFFSET = 5;

	private IGaService gaService = Graphiti.getGaService();

	public LayoutLaneFeature(IFeatureProvider fp) {
		super(fp);
	}

	public boolean canLayout(ILayoutContext context) {
		PictogramElement pe = context.getPictogramElement();
		if (!(pe instanceof ContainerShape))
			return false;

		EList<EObject> businessObjects = pe.getLink().getBusinessObjects();
		return (businessObjects.size() == 1 && businessObjects.get(0) instanceof Lane);
	}

	public boolean layout(ILayoutContext context) {
		boolean anythingChanged = false;
		ContainerShape containerShape = (ContainerShape) context.getPictogramElement();
		GraphicsAlgorithm containerGa = containerShape.getGraphicsAlgorithm();

		// height
		int newHeight = calculateMinHeight(containerShape);
		if (containerGa.getHeight() < newHeight) {
			containerGa.setHeight(newHeight);
			anythingChanged = true;
		}

		// width
		int newWidth = calculateMinWidth(containerShape);
		if (containerGa.getWidth() < newWidth) {
			containerGa.setWidth(newWidth);
			anythingChanged = true;
		}

		//For lane layout/resize important is height because of pool name test and pool line
		int containerHeight = containerGa.getHeight();
		//TODO Paziti kada dodju elementi u lane da li �e ostati isto
		for (Shape shape : containerShape.getChildren()){
			GraphicsAlgorithm graphicsAlgorithm = shape.getGraphicsAlgorithm();
			if(graphicsAlgorithm != null && (graphicsAlgorithm instanceof Polyline || graphicsAlgorithm instanceof Text)){
				IDimension size = gaService.calculateSize(graphicsAlgorithm);
				if (containerHeight != size.getHeight()) {
					if (graphicsAlgorithm instanceof Polyline && !(graphicsAlgorithm instanceof Polygon)) {
						Polyline polyline = (Polyline) graphicsAlgorithm;
						Point secondPoint = polyline.getPoints().get(1);
						Point newSecondPoint = gaService.createPoint(secondPoint.getX(), containerHeight); 
						polyline.getPoints().set(1, newSecondPoint);
						anythingChanged = true;
					} else {
						gaService.setHeight(graphicsAlgorithm, containerHeight);
						anythingChanged = true;
					}
				}
			}
		}
		return anythingChanged;
	}

	public int calculateMinHeight(ContainerShape containerShape){
		GraphicsAlgorithm containerGa = containerShape.getGraphicsAlgorithm();
		int minHeight = containerGa.getHeight();

		for (Shape shape : containerShape.getChildren()){
			//Line ne u�estvuje u layout prora�unu(hemija u if-u zbog gateway grafi�kog algoritma(Polygon extenda Polyline)
			if(!(shape.getGraphicsAlgorithm() instanceof Polyline && ((Polyline) shape.getGraphicsAlgorithm()).getPoints().size() == 2) && !(shape.getGraphicsAlgorithm() instanceof Text)){
				
				//Ako je neko dodao element na gornju ivicu a deo je ostao van lane-a spustimo ga u lane
				if(shape.getGraphicsAlgorithm().getY() < 0)
					shape.getGraphicsAlgorithm().setY(OFFSET);
				//Y je u odnosu na parenta ne u odnosu na (0,0). Na nju dodamo samo height i dobijemo visinu elementa
				int size = shape.getGraphicsAlgorithm().getY() + shape.getGraphicsAlgorithm().getHeight();
				//Ako je minHeight manje od nove izra�unate vrednosti
				if(minHeight < size){
					minHeight = size + OFFSET;	
				}
			}
		}
		//ako je ipak manje od najmanje dimenzije ili ako nema elemenata u sebi(tj. ima samo polyline i text)
		if(minHeight < MIN_HEIGHT || containerShape.getChildren().size() == 2)
			return MIN_HEIGHT;

		return minHeight;
	}

	public int calculateMinWidth(ContainerShape containerShape){
		GraphicsAlgorithm containerGa = containerShape.getGraphicsAlgorithm();
		int minWidth = containerGa.getWidth();

		for (Shape shape : containerShape.getChildren()){
			//Line ne u�estvuje u layout prora�unu
			if(!(shape.getGraphicsAlgorithm() instanceof Polyline && ((Polyline) shape.getGraphicsAlgorithm()).getPoints().size() == 2) && !(shape.getGraphicsAlgorithm() instanceof Text)){
				
				//Ako je neko dodao element na bo�nu ivicu a deo je ostao van lane-a spustimo ga u lane
				if(shape.getGraphicsAlgorithm().getX() < 0)
					shape.getGraphicsAlgorithm().setX(LANE_OFFSET);
				//Ako je minWidth manje od nove izra�unate vrednosti
				//X je u odnosu na parenta ne u odnosu na (0,0). Na nju dodamo samo height i dobijemo visinu elementa
				int size = shape.getGraphicsAlgorithm().getX() + shape.getGraphicsAlgorithm().getWidth();
				if(minWidth < size){
					minWidth = size + OFFSET;
				}
			}
		}
		//ako je ipak manje od najmanje dimenzije
		if(minWidth < MIN_WIDTH || containerShape.getChildren().size() == 2)
			return MIN_WIDTH;

		return minWidth;
	}
}
