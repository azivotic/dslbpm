package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.graphiti.examples.common.ExampleUtil;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICustomContext;
import org.eclipse.graphiti.features.custom.AbstractCustomFeature;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;

import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.Task;

public class RenameFeature extends AbstractCustomFeature {
 
	private boolean hasDoneChanges = false;
	
    public RenameFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public String getName() {
		return "Rename";
	}
	 
	public String getDescriptionTask() {
		return "Change the name of the Task";
	}
	
	public String getDescriptionSequenceFlow() {
		return "Change the name of the Sequence Flow";
	}
	 
	@Override
	public boolean canExecute(ICustomContext context) {
		// allow rename if exactly one pictogram element representing a Task or Sequence flow is selected
		boolean ret = false;
		PictogramElement[] pes = context.getPictogramElements();
		if (pes != null && pes.length == 1) {
			Object bo = getBusinessObjectForPictogramElement(pes[0]);
		    if (bo instanceof Task || bo instanceof SequenceFlow) {
		    	ret = true;
		    }
		}
	    return ret;
	}
	
	
	@Override
	public void execute(ICustomContext context) {
		PictogramElement[] pes = context.getPictogramElements();
        if (pes != null && pes.length == 1) {
            Object bo = getBusinessObjectForPictogramElement(pes[0]);
            if (bo instanceof Task) {
                Task task = (Task) bo;
                String currentName = task.getName();
                // ask user for a new class name
                String newName = ExampleUtil.askString(getName(), getDescriptionTask(), currentName);
                if (newName != null && !newName.equals(currentName)) {
                    this.hasDoneChanges = true;
                    task.setName(newName);
                    updatePictogramElement(pes[0]);
                }
            }
            if (bo instanceof SequenceFlow) {
            	SequenceFlow sequenceFlow = (SequenceFlow) bo;
                String currentName = sequenceFlow.getName();
                // ask user for a new class name
                String newName = ExampleUtil.askString(getName(), getDescriptionSequenceFlow(), currentName);
                if (newName != null && !newName.equals(currentName)) {
                    this.hasDoneChanges = true;
                    sequenceFlow.setName(newName);
                    updatePictogramElement(pes[0]);
                }
            }
        }
		
	}
	
	@Override
    public boolean hasDoneChanges() {
           return this.hasDoneChanges;
    }

}
