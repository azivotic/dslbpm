package com.azivotic.dsl.bpm.core.features;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import com.azivotic.dsl.bpm.core.diagram.BpmImageProvider;
import com.azivotic.dsl.bpm.core.util.Util;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.MetamodelFactory;

public class CreateLaneFeature extends AbstractCreateFeature{

	private final static String DEFAULT_NAME = "Lane";

	public CreateLaneFeature(IFeatureProvider fp) {
		super(fp, "Lane", "Create Lane");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		
		return (context.getTargetContainer() instanceof Diagram);

	}

	@Override
	public Object[] create(ICreateContext context) {
		Lane newClass = MetamodelFactory.eINSTANCE.createLane();
		newClass.setName(DEFAULT_NAME);

		addGraphicalRepresentation(context, newClass);
		try {
			Util.saveToModelFile(newClass, getDiagram());
		} catch (CoreException | IOException e) {
			e.printStackTrace();
		}

		getFeatureProvider().getDirectEditingInfo().setActive(true);

		// return newly created business object(s)
		return new Object[] { newClass };
	}

	@Override
	public String getCreateImageId() {
		return BpmImageProvider.IMG_PALETTE_LANE;
	}

}

