package com.azivotic.dsl.bpm.core.features;

import org.eclipse.graphiti.features.IDirectEditingInfo;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.algorithms.MultiText;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;

import com.azivotic.dsl.bpm.core.util.StyleUtil;
import com.azivotic.dsl.bpm.metamodel.Comment;
import com.azivotic.dsl.bpm.metamodel.Lane;

public class AddCommentFeature extends AbstractAddShapeFeature {

	public AddCommentFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canAdd(IAddContext context) {
		Object newObject = context.getNewObject();
		if (newObject instanceof Comment) {
			Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
			if (container instanceof Lane || context.getTargetContainer() instanceof Diagram) {
				return true;
			}
		}
		return false;
	}

	@Override
	public PictogramElement add(IAddContext context) {
		IPeCreateService peCreateService = Graphiti.getPeCreateService();
		IGaService gaService = Graphiti.getGaService();

		Comment addedClass = (Comment) context.getNewObject();
		
		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		ContainerShape targetShape = (ContainerShape) context.getTargetContainer();
		ContainerShape containerShape = peCreateService.createContainerShape(targetShape, true);

		final int width = context.getWidth() <= 0 ? 160 : context.getWidth();
		final int height = context.getHeight() <= 0 ? 80 : context.getHeight();

		final int x = context.getX();
		final int y = context.getY();

		//SHAPE WITH ROUNDED RECTANGLE
		RoundedRectangle roundedRectangle; 
		{
			roundedRectangle = gaService.createPlainRoundedRectangle(containerShape, 30, 30);
			roundedRectangle.setFilled(true);
			roundedRectangle.setBackground(gaService.manageColor(getDiagram(), StyleUtil.COMMENT_FILL));
			gaService.setLocationAndSize(roundedRectangle, x, y, width, height);
			// create link and wire it
			link(containerShape, addedClass);
		}

		// SHAPE WITH TEXT
		{
			Shape shape = peCreateService.createShape(containerShape, false);
			MultiText text = gaService.createPlainMultiText(shape, addedClass.getDescription());
			text.setStyle(StyleUtil.getStyleForComment(getDiagram()));
			gaService.setLocationAndSize(text, 5, 5, width-5, height-5);
			// create link and wire it
			link(shape, addedClass);
			//direct editing feature
			IDirectEditingInfo directEditingInfo = getFeatureProvider().getDirectEditingInfo();
			// set container shape for direct editing after object creation
			directEditingInfo.setMainPictogramElement(containerShape);
			// set shape and graphics algorithm where the editor for direct editing shall be opened after object creation
			directEditingInfo.setPictogramElement(shape);
			directEditingInfo.setGraphicsAlgorithm(text);
		}

		// call the layout feature
		layoutPictogramElement(containerShape);
		//layout Lane
		if(container instanceof Lane){
			layoutPictogramElement(context.getTargetContainer());
		}
		return containerShape;
	}
}

