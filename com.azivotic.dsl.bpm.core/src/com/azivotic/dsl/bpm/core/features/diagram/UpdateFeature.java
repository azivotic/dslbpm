package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.IReason;
import org.eclipse.graphiti.features.context.IUpdateContext;
import org.eclipse.graphiti.features.impl.AbstractUpdateFeature;
import org.eclipse.graphiti.features.impl.Reason;
import org.eclipse.graphiti.mm.algorithms.MultiText;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;

import com.azivotic.dsl.bpm.metamodel.Comment;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.Task;

public class UpdateFeature extends AbstractUpdateFeature {

	public UpdateFeature(IFeatureProvider fp) {
		super(fp);
	}

	public boolean canUpdate(IUpdateContext context) {
		// return true, if linked business object is a Task or Sequence Flow
		Object bo = getBusinessObjectForPictogramElement(context.getPictogramElement());
		return (bo instanceof Task || bo instanceof SequenceFlow || bo instanceof Lane || bo instanceof Comment);
	}

	public IReason updateNeeded(IUpdateContext context) {
		// retrieve name from pictogram model
		String pictogramName = null;
		//check for Task, Lane or Comments
		PictogramElement pictogramElement = context.getPictogramElement();
		if (pictogramElement instanceof ContainerShape) {
			ContainerShape cs = (ContainerShape) pictogramElement;
			for (Shape shape : cs.getChildren()) {
				if (shape.getGraphicsAlgorithm() instanceof Text) {
					Text text = (Text) shape.getGraphicsAlgorithm();
					pictogramName = text.getValue();
				} else if(shape.getGraphicsAlgorithm() instanceof MultiText){
					MultiText text = (MultiText) shape.getGraphicsAlgorithm();
					pictogramName = text.getValue();
				}
			}
		}

		// check for Sequence flow
		if (pictogramElement instanceof Connection) {
			Connection con = (Connection) pictogramElement;
			for (ConnectionDecorator cd : con.getConnectionDecorators()) {
				if (cd.getGraphicsAlgorithm() instanceof Text) {
					Text text = (Text) cd.getGraphicsAlgorithm();
					pictogramName = text.getValue();
				}
			}
		}

		// retrieve name from business model
		String businessName = null;
		Object bo = getBusinessObjectForPictogramElement(pictogramElement);
		if (bo instanceof Task) {
			Task task = (Task) bo;
			businessName = task.getName();
		} else if (bo instanceof SequenceFlow) {
			SequenceFlow sequenceFlow = (SequenceFlow) bo;
			businessName = sequenceFlow.getName();
		} else if(bo instanceof Lane){
			Lane lane = (Lane) bo;
			businessName = lane.getName();
		} else if(bo instanceof Comment){
			Comment comment = (Comment) bo;
			businessName = comment.getDescription();
		}

		// update needed, if names are different
		boolean updateNameNeeded = 
		((pictogramName == null && businessName != null) || (pictogramName != null && !pictogramName.equals(businessName)));
		if (updateNameNeeded) {
			return Reason.createTrueReason("Name is out of date");
		} else {
			return Reason.createFalseReason();
		}
	}

	public boolean update(IUpdateContext context) {
		// retrieve name from business model
		String businessName = null;
		PictogramElement pictogramElement = context.getPictogramElement();
		Object bo = getBusinessObjectForPictogramElement(pictogramElement);

		if (bo instanceof Task) {
			Task task = (Task) bo;
			businessName = task.getName();
		} else if (bo instanceof SequenceFlow) {
			SequenceFlow sequenceFlow = (SequenceFlow) bo;
			businessName = sequenceFlow.getName();
		} else if(bo instanceof Lane){
			Lane lane = (Lane) bo;
			businessName = lane.getName();
		} else if(bo instanceof Comment){
			Comment comment = (Comment) bo;
			businessName = comment.getDescription();
		}

		// Set name in pictogram model
		if (pictogramElement instanceof ContainerShape) {
			ContainerShape cs = (ContainerShape) pictogramElement;
			for (Shape shape : cs.getChildren()) {
				if (shape.getGraphicsAlgorithm() instanceof Text) {
					Text text = (Text) shape.getGraphicsAlgorithm();
					text.setValue(businessName);
					return true;
				} else if(shape.getGraphicsAlgorithm() instanceof MultiText){
					MultiText text = (MultiText) shape.getGraphicsAlgorithm();
					text.setValue(businessName);
				}
			}
		}

		if (pictogramElement instanceof Connection) {
			Connection con = (Connection) pictogramElement;
			for (ConnectionDecorator cd : con.getConnectionDecorators()) {
				if (cd.getGraphicsAlgorithm() instanceof Text) {
					Text text = (Text) cd.getGraphicsAlgorithm();
					text.setValue(businessName);
					return true;
				}
			}
		}

		return false;
	}
}
