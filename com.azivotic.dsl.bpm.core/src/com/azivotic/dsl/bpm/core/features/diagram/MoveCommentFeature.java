package com.azivotic.dsl.bpm.core.features.diagram;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IMoveShapeContext;
import org.eclipse.graphiti.features.impl.DefaultMoveShapeFeature;
import org.eclipse.graphiti.mm.pictograms.Shape;

import com.azivotic.dsl.bpm.metamodel.Comment;
import com.azivotic.dsl.bpm.metamodel.Lane;

public class MoveCommentFeature extends DefaultMoveShapeFeature {

	public MoveCommentFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canMoveShape(IMoveShapeContext context) {
		boolean canMove = super.canMoveShape(context);
		Shape shape = context.getShape();
		Object bo = getBusinessObjectForPictogramElement(shape);
		if (bo instanceof Comment) {
			canMove = true;
		}

		return canMove;
	}

	@Override
	protected void postMoveShape(IMoveShapeContext context) {
		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		// if comment is moved to lane
		if(container != null && container instanceof Lane){
			layoutPictogramElement(context.getTargetContainer());
		}
	}
}
