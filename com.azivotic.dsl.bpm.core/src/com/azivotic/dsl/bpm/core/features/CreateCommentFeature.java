package com.azivotic.dsl.bpm.core.features;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.ICreateContext;
import org.eclipse.graphiti.features.impl.AbstractCreateFeature;
import org.eclipse.graphiti.mm.pictograms.Diagram;

import com.azivotic.dsl.bpm.core.diagram.BpmImageProvider;
import com.azivotic.dsl.bpm.core.util.Util;
import com.azivotic.dsl.bpm.metamodel.Comment;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.MetamodelFactory;

public class CreateCommentFeature extends AbstractCreateFeature{

	private final static String DEFAULT_COMMENT = "Add some coment here";

	public CreateCommentFeature(IFeatureProvider fp) {
		super(fp, "Comment", "Add new comment");
	}

	@Override
	public boolean canCreate(ICreateContext context) {
		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		return (container instanceof Lane || context.getTargetContainer() instanceof Diagram);
	}

	@Override
	public Object[] create(ICreateContext context) {
		Comment newClass = MetamodelFactory.eINSTANCE.createComment();
		newClass.setDescription(DEFAULT_COMMENT);

		try {
			Util.saveToModelFile(newClass, getDiagram());
		} catch (CoreException | IOException e) {
			e.printStackTrace();
		}

		addGraphicalRepresentation(context, newClass);

		getFeatureProvider().getDirectEditingInfo().setActive(true);
		// return newly created business object(s)
		return new Object[] { newClass };
	}

	@Override
	public String getCreateImageId() {
		return BpmImageProvider.IMG_PALETTE_COMMENT;
	}

}

