package com.azivotic.dsl.bpm.core.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;

import com.azivotic.dsl.bpm.core.util.StyleUtil;
import com.azivotic.dsl.bpm.metamodel.Gateway;
import com.azivotic.dsl.bpm.metamodel.Lane;

public class AddGatewayFeature extends AbstractAddShapeFeature{

	public AddGatewayFeature(IFeatureProvider fp) {
		super(fp);
	}

	@Override
	public boolean canAdd(IAddContext context) {
		Object newObject = context.getNewObject();
		if (newObject instanceof Gateway) {
			Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
			if (container instanceof Lane || context.getTargetContainer() instanceof Diagram) {
				return true;
			}
		}
		return false;
	}

	@Override
	public PictogramElement add(IAddContext context) {
		IPeCreateService peCreateService = Graphiti.getPeCreateService();
		IGaService gaService = Graphiti.getGaService();

		Gateway addedClass = (Gateway) context.getNewObject();
		//Set model structure, Lane is the parent for the task
		Object container = getBusinessObjectForPictogramElement(context.getTargetContainer());
		if(container instanceof Lane){
			Lane lane = (Lane)container;
			lane.getProcessNodeRefs().add(addedClass);
		}
		
		ContainerShape targetShape = context.getTargetContainer();
		ContainerShape containerShape = peCreateService.createContainerShape(targetShape, true);

		final int width = 50;
		final int height = 50;

		final int x = context.getProperty("xcoordinate")!=null ? (Integer)context.getProperty("xcoordinate") + 150 : context.getX();
		final int y = context.getProperty("ycoordinate")!=null ? (Integer)context.getProperty("ycoordinate") : context.getY();

		Polygon polygon; 
		{
			
			final Rectangle invisibleRectangle = gaService.createInvisibleRectangle(containerShape);
	        gaService.setLocationAndSize(invisibleRectangle, x, y, width, height);
	        
			polygon = gaService.createPolygon(invisibleRectangle, new int[] { 25, 0, 50, 25, 25, 50, 0, 25 });
			polygon.setForeground(manageColor(StyleUtil.GATEWAY_EDGE));
			polygon.setBackground(manageColor(StyleUtil.GATEWAY_FILL));
			polygon.setLineWidth(2);
			gaService.setLocationAndSize(polygon, 0, 0, width, height);
			// create link and wire it
			link(containerShape, addedClass);
		}

		// add a chopbox anchor to the shape 
		peCreateService.createChopboxAnchor(containerShape);

		// call the layout feature
		layoutPictogramElement(containerShape);
		 //layout Lane
        if(container instanceof Lane){
        	layoutPictogramElement(context.getTargetContainer());
        }
		return containerShape;
	}
}

