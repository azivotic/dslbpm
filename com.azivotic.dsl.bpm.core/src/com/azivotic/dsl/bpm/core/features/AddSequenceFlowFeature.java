package com.azivotic.dsl.bpm.core.features;

import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddConnectionContext;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddFeature;
import org.eclipse.graphiti.mm.GraphicsAlgorithmContainer;
import org.eclipse.graphiti.mm.algorithms.Polygon;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.pictograms.Connection;
import org.eclipse.graphiti.mm.pictograms.ConnectionDecorator;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;

import com.azivotic.dsl.bpm.core.util.StyleUtil;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;

public class AddSequenceFlowFeature extends AbstractAddFeature {

	public AddSequenceFlowFeature (IFeatureProvider fp) {
		super(fp);
	}

	public boolean canAdd(IAddContext context) {
		if (context instanceof IAddConnectionContext && context.getNewObject() instanceof SequenceFlow) {
			return true;
		}
		return false;
	}

	public PictogramElement add(IAddContext context) {
		IPeCreateService peCreateService = Graphiti.getPeCreateService();
		IGaService gaService = Graphiti.getGaService();


		IAddConnectionContext addConContext = (IAddConnectionContext) context;
		SequenceFlow addedEReference = (SequenceFlow) context.getNewObject();

		// Connection with polyline
		Connection connection = peCreateService.createFreeFormConnection(getDiagram());
		connection.setStart(addConContext.getSourceAnchor());
		connection.setEnd(addConContext.getTargetAnchor());
		Polyline polyline = gaService.createPolyline(connection);
		polyline.setLineWidth(2);
		polyline.setForeground(manageColor(StyleUtil.DARK_GRAY));

		// create link and wire it
		link(connection, addedEReference);

		//Connection text
		ConnectionDecorator textDecorator = peCreateService.createConnectionDecorator(connection, true, 0.5, true);
		Text text = gaService.createDefaultText(getDiagram(),textDecorator);
		text.setForeground(manageColor(StyleUtil.DARK_GRAY));
		gaService.setLocation(text, 10, 0);
		text.setValue(addedEReference.getName());

		// add static graphical decorator (composition and navigable)
		ConnectionDecorator cd = peCreateService.createConnectionDecorator(connection, false, 1.0, true);
		createArrow(cd);
		
		return connection;
	}

	private Polygon createArrow(GraphicsAlgorithmContainer gaContainer) {
		IGaService gaService = Graphiti.getGaService();
		int[] points = new int[] { -10, -5, 0, 0, -10, 5, -10, -5 };
		Polygon polygon = gaService.createPolygon(gaContainer, points);
		polygon.setBackground(manageColor(StyleUtil.DARK_GRAY));
		polygon.setForeground(manageColor(StyleUtil.DARK_GRAY));
		polygon.setLineWidth(2);
		return polygon;
	}
}
