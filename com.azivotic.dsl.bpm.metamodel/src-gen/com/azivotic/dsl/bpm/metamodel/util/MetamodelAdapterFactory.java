/**
 */
package com.azivotic.dsl.bpm.metamodel.util;

import com.azivotic.dsl.bpm.metamodel.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage
 * @generated
 */
public class MetamodelAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static MetamodelPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MetamodelAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = MetamodelPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MetamodelSwitch<Adapter> modelSwitch =
    new MetamodelSwitch<Adapter>()
    {
      @Override
      public Adapter caseGraphicalElement(GraphicalElement object)
      {
        return createGraphicalElementAdapter();
      }
      @Override
      public Adapter caseLane(Lane object)
      {
        return createLaneAdapter();
      }
      @Override
      public Adapter caseProcessNode(ProcessNode object)
      {
        return createProcessNodeAdapter();
      }
      @Override
      public Adapter caseSequenceFlow(SequenceFlow object)
      {
        return createSequenceFlowAdapter();
      }
      @Override
      public Adapter caseActivity(Activity object)
      {
        return createActivityAdapter();
      }
      @Override
      public Adapter caseEvent(Event object)
      {
        return createEventAdapter();
      }
      @Override
      public Adapter caseGateway(Gateway object)
      {
        return createGatewayAdapter();
      }
      @Override
      public Adapter caseStartEvent(StartEvent object)
      {
        return createStartEventAdapter();
      }
      @Override
      public Adapter caseEndEvent(EndEvent object)
      {
        return createEndEventAdapter();
      }
      @Override
      public Adapter caseTask(Task object)
      {
        return createTaskAdapter();
      }
      @Override
      public Adapter caseComment(Comment object)
      {
        return createCommentAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement <em>Graphical Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.GraphicalElement
   * @generated
   */
  public Adapter createGraphicalElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.Lane <em>Lane</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.Lane
   * @generated
   */
  public Adapter createLaneAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode <em>Process Node</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode
   * @generated
   */
  public Adapter createProcessNodeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow <em>Sequence Flow</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.SequenceFlow
   * @generated
   */
  public Adapter createSequenceFlowAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.Activity <em>Activity</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.Activity
   * @generated
   */
  public Adapter createActivityAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.Event <em>Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.Event
   * @generated
   */
  public Adapter createEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.Gateway <em>Gateway</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.Gateway
   * @generated
   */
  public Adapter createGatewayAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.StartEvent <em>Start Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.StartEvent
   * @generated
   */
  public Adapter createStartEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.EndEvent <em>End Event</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.EndEvent
   * @generated
   */
  public Adapter createEndEventAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.Task <em>Task</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.Task
   * @generated
   */
  public Adapter createTaskAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link com.azivotic.dsl.bpm.metamodel.Comment <em>Comment</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see com.azivotic.dsl.bpm.metamodel.Comment
   * @generated
   */
  public Adapter createCommentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //MetamodelAdapterFactory
