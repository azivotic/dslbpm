/**
 */
package com.azivotic.dsl.bpm.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>End Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getEndEvent()
 * @model
 * @generated
 */
public interface EndEvent extends Event
{
} // EndEvent
