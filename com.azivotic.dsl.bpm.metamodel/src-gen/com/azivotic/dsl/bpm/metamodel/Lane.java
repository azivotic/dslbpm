/**
 */
package com.azivotic.dsl.bpm.metamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lane</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.Lane#getProcessNodeRefs <em>Process Node Refs</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getLane()
 * @model
 * @generated
 */
public interface Lane extends GraphicalElement
{
  /**
   * Returns the value of the '<em><b>Process Node Refs</b></em>' containment reference list.
   * The list contents are of type {@link com.azivotic.dsl.bpm.metamodel.ProcessNode}.
   * It is bidirectional and its opposite is '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getLane <em>Lane</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Process Node Refs</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Process Node Refs</em>' containment reference list.
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getLane_ProcessNodeRefs()
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode#getLane
   * @model opposite="lane" containment="true"
   * @generated
   */
  EList<ProcessNode> getProcessNodeRefs();

} // Lane
