/**
 */
package com.azivotic.dsl.bpm.metamodel.impl;

import com.azivotic.dsl.bpm.metamodel.MetamodelPackage;
import com.azivotic.dsl.bpm.metamodel.ProcessNode;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sequence Flow</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl#getSourceRef <em>Source Ref</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl#getTargetRef <em>Target Ref</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl#isDefaultLine <em>Default Line</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl#getCondition <em>Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SequenceFlowImpl extends MinimalEObjectImpl.Container implements SequenceFlow
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getSourceRef() <em>Source Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSourceRef()
   * @generated
   * @ordered
   */
  protected ProcessNode sourceRef;

  /**
   * The cached value of the '{@link #getTargetRef() <em>Target Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTargetRef()
   * @generated
   * @ordered
   */
  protected ProcessNode targetRef;

  /**
   * The default value of the '{@link #isDefaultLine() <em>Default Line</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDefaultLine()
   * @generated
   * @ordered
   */
  protected static final boolean DEFAULT_LINE_EDEFAULT = false;

  /**
   * The cached value of the '{@link #isDefaultLine() <em>Default Line</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isDefaultLine()
   * @generated
   * @ordered
   */
  protected boolean defaultLine = DEFAULT_LINE_EDEFAULT;

  /**
   * The default value of the '{@link #getCondition() <em>Condition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCondition()
   * @generated
   * @ordered
   */
  protected static final String CONDITION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getCondition() <em>Condition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCondition()
   * @generated
   * @ordered
   */
  protected String condition = CONDITION_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SequenceFlowImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MetamodelPackage.Literals.SEQUENCE_FLOW;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.SEQUENCE_FLOW__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription)
  {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.SEQUENCE_FLOW__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcessNode getSourceRef()
  {
    if (sourceRef != null && sourceRef.eIsProxy())
    {
      InternalEObject oldSourceRef = (InternalEObject)sourceRef;
      sourceRef = (ProcessNode)eResolveProxy(oldSourceRef);
      if (sourceRef != oldSourceRef)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF, oldSourceRef, sourceRef));
      }
    }
    return sourceRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcessNode basicGetSourceRef()
  {
    return sourceRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSourceRef(ProcessNode newSourceRef, NotificationChain msgs)
  {
    ProcessNode oldSourceRef = sourceRef;
    sourceRef = newSourceRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF, oldSourceRef, newSourceRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSourceRef(ProcessNode newSourceRef)
  {
    if (newSourceRef != sourceRef)
    {
      NotificationChain msgs = null;
      if (sourceRef != null)
        msgs = ((InternalEObject)sourceRef).eInverseRemove(this, MetamodelPackage.PROCESS_NODE__OUTGOING, ProcessNode.class, msgs);
      if (newSourceRef != null)
        msgs = ((InternalEObject)newSourceRef).eInverseAdd(this, MetamodelPackage.PROCESS_NODE__OUTGOING, ProcessNode.class, msgs);
      msgs = basicSetSourceRef(newSourceRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF, newSourceRef, newSourceRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcessNode getTargetRef()
  {
    if (targetRef != null && targetRef.eIsProxy())
    {
      InternalEObject oldTargetRef = (InternalEObject)targetRef;
      targetRef = (ProcessNode)eResolveProxy(oldTargetRef);
      if (targetRef != oldTargetRef)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MetamodelPackage.SEQUENCE_FLOW__TARGET_REF, oldTargetRef, targetRef));
      }
    }
    return targetRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ProcessNode basicGetTargetRef()
  {
    return targetRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetTargetRef(ProcessNode newTargetRef, NotificationChain msgs)
  {
    ProcessNode oldTargetRef = targetRef;
    targetRef = newTargetRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodelPackage.SEQUENCE_FLOW__TARGET_REF, oldTargetRef, newTargetRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTargetRef(ProcessNode newTargetRef)
  {
    if (newTargetRef != targetRef)
    {
      NotificationChain msgs = null;
      if (targetRef != null)
        msgs = ((InternalEObject)targetRef).eInverseRemove(this, MetamodelPackage.PROCESS_NODE__INCOMING, ProcessNode.class, msgs);
      if (newTargetRef != null)
        msgs = ((InternalEObject)newTargetRef).eInverseAdd(this, MetamodelPackage.PROCESS_NODE__INCOMING, ProcessNode.class, msgs);
      msgs = basicSetTargetRef(newTargetRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.SEQUENCE_FLOW__TARGET_REF, newTargetRef, newTargetRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isDefaultLine()
  {
    return defaultLine;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDefaultLine(boolean newDefaultLine)
  {
    boolean oldDefaultLine = defaultLine;
    defaultLine = newDefaultLine;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.SEQUENCE_FLOW__DEFAULT_LINE, oldDefaultLine, defaultLine));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getCondition()
  {
    return condition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCondition(String newCondition)
  {
    String oldCondition = condition;
    condition = newCondition;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.SEQUENCE_FLOW__CONDITION, oldCondition, condition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF:
        if (sourceRef != null)
          msgs = ((InternalEObject)sourceRef).eInverseRemove(this, MetamodelPackage.PROCESS_NODE__OUTGOING, ProcessNode.class, msgs);
        return basicSetSourceRef((ProcessNode)otherEnd, msgs);
      case MetamodelPackage.SEQUENCE_FLOW__TARGET_REF:
        if (targetRef != null)
          msgs = ((InternalEObject)targetRef).eInverseRemove(this, MetamodelPackage.PROCESS_NODE__INCOMING, ProcessNode.class, msgs);
        return basicSetTargetRef((ProcessNode)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF:
        return basicSetSourceRef(null, msgs);
      case MetamodelPackage.SEQUENCE_FLOW__TARGET_REF:
        return basicSetTargetRef(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MetamodelPackage.SEQUENCE_FLOW__NAME:
        return getName();
      case MetamodelPackage.SEQUENCE_FLOW__DESCRIPTION:
        return getDescription();
      case MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF:
        if (resolve) return getSourceRef();
        return basicGetSourceRef();
      case MetamodelPackage.SEQUENCE_FLOW__TARGET_REF:
        if (resolve) return getTargetRef();
        return basicGetTargetRef();
      case MetamodelPackage.SEQUENCE_FLOW__DEFAULT_LINE:
        return isDefaultLine();
      case MetamodelPackage.SEQUENCE_FLOW__CONDITION:
        return getCondition();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MetamodelPackage.SEQUENCE_FLOW__NAME:
        setName((String)newValue);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF:
        setSourceRef((ProcessNode)newValue);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__TARGET_REF:
        setTargetRef((ProcessNode)newValue);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__DEFAULT_LINE:
        setDefaultLine((Boolean)newValue);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__CONDITION:
        setCondition((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MetamodelPackage.SEQUENCE_FLOW__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF:
        setSourceRef((ProcessNode)null);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__TARGET_REF:
        setTargetRef((ProcessNode)null);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__DEFAULT_LINE:
        setDefaultLine(DEFAULT_LINE_EDEFAULT);
        return;
      case MetamodelPackage.SEQUENCE_FLOW__CONDITION:
        setCondition(CONDITION_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MetamodelPackage.SEQUENCE_FLOW__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MetamodelPackage.SEQUENCE_FLOW__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF:
        return sourceRef != null;
      case MetamodelPackage.SEQUENCE_FLOW__TARGET_REF:
        return targetRef != null;
      case MetamodelPackage.SEQUENCE_FLOW__DEFAULT_LINE:
        return defaultLine != DEFAULT_LINE_EDEFAULT;
      case MetamodelPackage.SEQUENCE_FLOW__CONDITION:
        return CONDITION_EDEFAULT == null ? condition != null : !CONDITION_EDEFAULT.equals(condition);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", description: ");
    result.append(description);
    result.append(", defaultLine: ");
    result.append(defaultLine);
    result.append(", condition: ");
    result.append(condition);
    result.append(')');
    return result.toString();
  }

} //SequenceFlowImpl
