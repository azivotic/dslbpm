/**
 */
package com.azivotic.dsl.bpm.metamodel.impl;

import com.azivotic.dsl.bpm.metamodel.Activity;
import com.azivotic.dsl.bpm.metamodel.Comment;
import com.azivotic.dsl.bpm.metamodel.EndEvent;
import com.azivotic.dsl.bpm.metamodel.Event;
import com.azivotic.dsl.bpm.metamodel.Gateway;
import com.azivotic.dsl.bpm.metamodel.GatewayType;
import com.azivotic.dsl.bpm.metamodel.GraphicalElement;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.MetamodelFactory;
import com.azivotic.dsl.bpm.metamodel.MetamodelPackage;
import com.azivotic.dsl.bpm.metamodel.ProcessNode;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.StartEvent;
import com.azivotic.dsl.bpm.metamodel.Task;
import com.azivotic.dsl.bpm.metamodel.TaskExecutionType;
import com.azivotic.dsl.bpm.metamodel.TaskType;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MetamodelPackageImpl extends EPackageImpl implements MetamodelPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass graphicalElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass laneEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass processNodeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sequenceFlowEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass activityEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass eventEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass gatewayEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass startEventEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass endEventEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass taskEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass commentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum gatewayTypeEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum taskTypeEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum taskExecutionTypeEEnum = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private MetamodelPackageImpl()
  {
    super(eNS_URI, MetamodelFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link MetamodelPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static MetamodelPackage init()
  {
    if (isInited) return (MetamodelPackage)EPackage.Registry.INSTANCE.getEPackage(MetamodelPackage.eNS_URI);

    // Obtain or create and register package
    MetamodelPackageImpl theMetamodelPackage = (MetamodelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MetamodelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MetamodelPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    EcorePackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theMetamodelPackage.createPackageContents();

    // Initialize created meta-data
    theMetamodelPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theMetamodelPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(MetamodelPackage.eNS_URI, theMetamodelPackage);
    return theMetamodelPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGraphicalElement()
  {
    return graphicalElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGraphicalElement_Name()
  {
    return (EAttribute)graphicalElementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGraphicalElement_Description()
  {
    return (EAttribute)graphicalElementEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLane()
  {
    return laneEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLane_ProcessNodeRefs()
  {
    return (EReference)laneEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getProcessNode()
  {
    return processNodeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessNode_Incoming()
  {
    return (EReference)processNodeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessNode_Outgoing()
  {
    return (EReference)processNodeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getProcessNode_Lane()
  {
    return (EReference)processNodeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSequenceFlow()
  {
    return sequenceFlowEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSequenceFlow_SourceRef()
  {
    return (EReference)sequenceFlowEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSequenceFlow_TargetRef()
  {
    return (EReference)sequenceFlowEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSequenceFlow_DefaultLine()
  {
    return (EAttribute)sequenceFlowEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSequenceFlow_Condition()
  {
    return (EAttribute)sequenceFlowEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getActivity()
  {
    return activityEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEvent()
  {
    return eventEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGateway()
  {
    return gatewayEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGateway_GatewayType()
  {
    return (EAttribute)gatewayEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getStartEvent()
  {
    return startEventEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getEndEvent()
  {
    return endEventEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTask()
  {
    return taskEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTask_Assignee()
  {
    return (EAttribute)taskEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTask_TaskType()
  {
    return (EAttribute)taskEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getTask_TaskExecutionType()
  {
    return (EAttribute)taskEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComment()
  {
    return commentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getGatewayType()
  {
    return gatewayTypeEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTaskType()
  {
    return taskTypeEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getTaskExecutionType()
  {
    return taskExecutionTypeEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MetamodelFactory getMetamodelFactory()
  {
    return (MetamodelFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    graphicalElementEClass = createEClass(GRAPHICAL_ELEMENT);
    createEAttribute(graphicalElementEClass, GRAPHICAL_ELEMENT__NAME);
    createEAttribute(graphicalElementEClass, GRAPHICAL_ELEMENT__DESCRIPTION);

    laneEClass = createEClass(LANE);
    createEReference(laneEClass, LANE__PROCESS_NODE_REFS);

    processNodeEClass = createEClass(PROCESS_NODE);
    createEReference(processNodeEClass, PROCESS_NODE__INCOMING);
    createEReference(processNodeEClass, PROCESS_NODE__OUTGOING);
    createEReference(processNodeEClass, PROCESS_NODE__LANE);

    sequenceFlowEClass = createEClass(SEQUENCE_FLOW);
    createEReference(sequenceFlowEClass, SEQUENCE_FLOW__SOURCE_REF);
    createEReference(sequenceFlowEClass, SEQUENCE_FLOW__TARGET_REF);
    createEAttribute(sequenceFlowEClass, SEQUENCE_FLOW__DEFAULT_LINE);
    createEAttribute(sequenceFlowEClass, SEQUENCE_FLOW__CONDITION);

    activityEClass = createEClass(ACTIVITY);

    eventEClass = createEClass(EVENT);

    gatewayEClass = createEClass(GATEWAY);
    createEAttribute(gatewayEClass, GATEWAY__GATEWAY_TYPE);

    startEventEClass = createEClass(START_EVENT);

    endEventEClass = createEClass(END_EVENT);

    taskEClass = createEClass(TASK);
    createEAttribute(taskEClass, TASK__ASSIGNEE);
    createEAttribute(taskEClass, TASK__TASK_TYPE);
    createEAttribute(taskEClass, TASK__TASK_EXECUTION_TYPE);

    commentEClass = createEClass(COMMENT);

    // Create enums
    gatewayTypeEEnum = createEEnum(GATEWAY_TYPE);
    taskTypeEEnum = createEEnum(TASK_TYPE);
    taskExecutionTypeEEnum = createEEnum(TASK_EXECUTION_TYPE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    laneEClass.getESuperTypes().add(this.getGraphicalElement());
    processNodeEClass.getESuperTypes().add(this.getGraphicalElement());
    sequenceFlowEClass.getESuperTypes().add(this.getGraphicalElement());
    activityEClass.getESuperTypes().add(this.getProcessNode());
    eventEClass.getESuperTypes().add(this.getProcessNode());
    gatewayEClass.getESuperTypes().add(this.getProcessNode());
    startEventEClass.getESuperTypes().add(this.getEvent());
    endEventEClass.getESuperTypes().add(this.getEvent());
    taskEClass.getESuperTypes().add(this.getActivity());
    commentEClass.getESuperTypes().add(this.getGraphicalElement());

    // Initialize classes, features, and operations; add parameters
    initEClass(graphicalElementEClass, GraphicalElement.class, "GraphicalElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getGraphicalElement_Name(), theEcorePackage.getEString(), "name", null, 0, 1, GraphicalElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getGraphicalElement_Description(), theEcorePackage.getEString(), "description", null, 0, 1, GraphicalElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(laneEClass, Lane.class, "Lane", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLane_ProcessNodeRefs(), this.getProcessNode(), this.getProcessNode_Lane(), "processNodeRefs", null, 0, -1, Lane.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(processNodeEClass, ProcessNode.class, "ProcessNode", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getProcessNode_Incoming(), this.getSequenceFlow(), this.getSequenceFlow_TargetRef(), "incoming", null, 0, -1, ProcessNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProcessNode_Outgoing(), this.getSequenceFlow(), this.getSequenceFlow_SourceRef(), "outgoing", null, 0, -1, ProcessNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getProcessNode_Lane(), this.getLane(), this.getLane_ProcessNodeRefs(), "lane", null, 0, 1, ProcessNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(sequenceFlowEClass, SequenceFlow.class, "SequenceFlow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSequenceFlow_SourceRef(), this.getProcessNode(), this.getProcessNode_Outgoing(), "sourceRef", null, 0, 1, SequenceFlow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSequenceFlow_TargetRef(), this.getProcessNode(), this.getProcessNode_Incoming(), "targetRef", null, 0, 1, SequenceFlow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSequenceFlow_DefaultLine(), theEcorePackage.getEBoolean(), "defaultLine", null, 0, 1, SequenceFlow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSequenceFlow_Condition(), theEcorePackage.getEString(), "condition", null, 0, 1, SequenceFlow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(activityEClass, Activity.class, "Activity", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(eventEClass, Event.class, "Event", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(gatewayEClass, Gateway.class, "Gateway", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getGateway_GatewayType(), this.getGatewayType(), "gatewayType", null, 0, 1, Gateway.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(startEventEClass, StartEvent.class, "StartEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(endEventEClass, EndEvent.class, "EndEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(taskEClass, Task.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getTask_Assignee(), theEcorePackage.getEString(), "assignee", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTask_TaskType(), this.getTaskType(), "taskType", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getTask_TaskExecutionType(), this.getTaskExecutionType(), "taskExecutionType", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(commentEClass, Comment.class, "Comment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    // Initialize enums and add enum literals
    initEEnum(gatewayTypeEEnum, GatewayType.class, "GatewayType");
    addEEnumLiteral(gatewayTypeEEnum, GatewayType.DEFAULT);
    addEEnumLiteral(gatewayTypeEEnum, GatewayType.EXCLUSIVE);
    addEEnumLiteral(gatewayTypeEEnum, GatewayType.INCLUSIVE);
    addEEnumLiteral(gatewayTypeEEnum, GatewayType.PARALLEL);

    initEEnum(taskTypeEEnum, TaskType.class, "TaskType");
    addEEnumLiteral(taskTypeEEnum, TaskType.DEFAULT);
    addEEnumLiteral(taskTypeEEnum, TaskType.SERVICE);
    addEEnumLiteral(taskTypeEEnum, TaskType.USER);

    initEEnum(taskExecutionTypeEEnum, TaskExecutionType.class, "TaskExecutionType");
    addEEnumLiteral(taskExecutionTypeEEnum, TaskExecutionType.DEFAULT);
    addEEnumLiteral(taskExecutionTypeEEnum, TaskExecutionType.PARALLEL);
    addEEnumLiteral(taskExecutionTypeEEnum, TaskExecutionType.LOOP);

    // Create resource
    createResource(eNS_URI);
  }

} //MetamodelPackageImpl
