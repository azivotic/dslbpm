/**
 */
package com.azivotic.dsl.bpm.metamodel.impl;

import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.MetamodelPackage;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.StartEvent;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Start Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl#getLane <em>Lane</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StartEventImpl extends MinimalEObjectImpl.Container implements StartEvent
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getIncoming() <em>Incoming</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIncoming()
   * @generated
   * @ordered
   */
  protected EList<SequenceFlow> incoming;

  /**
   * The cached value of the '{@link #getOutgoing() <em>Outgoing</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutgoing()
   * @generated
   * @ordered
   */
  protected EList<SequenceFlow> outgoing;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StartEventImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MetamodelPackage.Literals.START_EVENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.START_EVENT__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription)
  {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.START_EVENT__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SequenceFlow> getIncoming()
  {
    if (incoming == null)
    {
      incoming = new EObjectWithInverseResolvingEList<SequenceFlow>(SequenceFlow.class, this, MetamodelPackage.START_EVENT__INCOMING, MetamodelPackage.SEQUENCE_FLOW__TARGET_REF);
    }
    return incoming;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SequenceFlow> getOutgoing()
  {
    if (outgoing == null)
    {
      outgoing = new EObjectWithInverseResolvingEList<SequenceFlow>(SequenceFlow.class, this, MetamodelPackage.START_EVENT__OUTGOING, MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF);
    }
    return outgoing;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Lane getLane()
  {
    if (eContainerFeatureID() != MetamodelPackage.START_EVENT__LANE) return null;
    return (Lane)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLane(Lane newLane, NotificationChain msgs)
  {
    msgs = eBasicSetContainer((InternalEObject)newLane, MetamodelPackage.START_EVENT__LANE, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLane(Lane newLane)
  {
    if (newLane != eInternalContainer() || (eContainerFeatureID() != MetamodelPackage.START_EVENT__LANE && newLane != null))
    {
      if (EcoreUtil.isAncestor(this, newLane))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newLane != null)
        msgs = ((InternalEObject)newLane).eInverseAdd(this, MetamodelPackage.LANE__PROCESS_NODE_REFS, Lane.class, msgs);
      msgs = basicSetLane(newLane, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.START_EVENT__LANE, newLane, newLane));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MetamodelPackage.START_EVENT__INCOMING:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncoming()).basicAdd(otherEnd, msgs);
      case MetamodelPackage.START_EVENT__OUTGOING:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoing()).basicAdd(otherEnd, msgs);
      case MetamodelPackage.START_EVENT__LANE:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetLane((Lane)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MetamodelPackage.START_EVENT__INCOMING:
        return ((InternalEList<?>)getIncoming()).basicRemove(otherEnd, msgs);
      case MetamodelPackage.START_EVENT__OUTGOING:
        return ((InternalEList<?>)getOutgoing()).basicRemove(otherEnd, msgs);
      case MetamodelPackage.START_EVENT__LANE:
        return basicSetLane(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
  {
    switch (eContainerFeatureID())
    {
      case MetamodelPackage.START_EVENT__LANE:
        return eInternalContainer().eInverseRemove(this, MetamodelPackage.LANE__PROCESS_NODE_REFS, Lane.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MetamodelPackage.START_EVENT__NAME:
        return getName();
      case MetamodelPackage.START_EVENT__DESCRIPTION:
        return getDescription();
      case MetamodelPackage.START_EVENT__INCOMING:
        return getIncoming();
      case MetamodelPackage.START_EVENT__OUTGOING:
        return getOutgoing();
      case MetamodelPackage.START_EVENT__LANE:
        return getLane();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MetamodelPackage.START_EVENT__NAME:
        setName((String)newValue);
        return;
      case MetamodelPackage.START_EVENT__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case MetamodelPackage.START_EVENT__INCOMING:
        getIncoming().clear();
        getIncoming().addAll((Collection<? extends SequenceFlow>)newValue);
        return;
      case MetamodelPackage.START_EVENT__OUTGOING:
        getOutgoing().clear();
        getOutgoing().addAll((Collection<? extends SequenceFlow>)newValue);
        return;
      case MetamodelPackage.START_EVENT__LANE:
        setLane((Lane)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MetamodelPackage.START_EVENT__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MetamodelPackage.START_EVENT__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case MetamodelPackage.START_EVENT__INCOMING:
        getIncoming().clear();
        return;
      case MetamodelPackage.START_EVENT__OUTGOING:
        getOutgoing().clear();
        return;
      case MetamodelPackage.START_EVENT__LANE:
        setLane((Lane)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MetamodelPackage.START_EVENT__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MetamodelPackage.START_EVENT__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case MetamodelPackage.START_EVENT__INCOMING:
        return incoming != null && !incoming.isEmpty();
      case MetamodelPackage.START_EVENT__OUTGOING:
        return outgoing != null && !outgoing.isEmpty();
      case MetamodelPackage.START_EVENT__LANE:
        return getLane() != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", description: ");
    result.append(description);
    result.append(')');
    return result.toString();
  }

} //StartEventImpl
