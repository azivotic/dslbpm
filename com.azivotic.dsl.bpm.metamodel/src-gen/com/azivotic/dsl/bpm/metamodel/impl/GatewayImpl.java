/**
 */
package com.azivotic.dsl.bpm.metamodel.impl;

import com.azivotic.dsl.bpm.metamodel.Gateway;
import com.azivotic.dsl.bpm.metamodel.GatewayType;
import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.MetamodelPackage;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gateway</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl#getLane <em>Lane</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl#getGatewayType <em>Gateway Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GatewayImpl extends MinimalEObjectImpl.Container implements Gateway
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getIncoming() <em>Incoming</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIncoming()
   * @generated
   * @ordered
   */
  protected EList<SequenceFlow> incoming;

  /**
   * The cached value of the '{@link #getOutgoing() <em>Outgoing</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutgoing()
   * @generated
   * @ordered
   */
  protected EList<SequenceFlow> outgoing;

  /**
   * The default value of the '{@link #getGatewayType() <em>Gateway Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGatewayType()
   * @generated
   * @ordered
   */
  protected static final GatewayType GATEWAY_TYPE_EDEFAULT = GatewayType.DEFAULT;

  /**
   * The cached value of the '{@link #getGatewayType() <em>Gateway Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGatewayType()
   * @generated
   * @ordered
   */
  protected GatewayType gatewayType = GATEWAY_TYPE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GatewayImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MetamodelPackage.Literals.GATEWAY;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.GATEWAY__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription)
  {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.GATEWAY__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SequenceFlow> getIncoming()
  {
    if (incoming == null)
    {
      incoming = new EObjectWithInverseResolvingEList<SequenceFlow>(SequenceFlow.class, this, MetamodelPackage.GATEWAY__INCOMING, MetamodelPackage.SEQUENCE_FLOW__TARGET_REF);
    }
    return incoming;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SequenceFlow> getOutgoing()
  {
    if (outgoing == null)
    {
      outgoing = new EObjectWithInverseResolvingEList<SequenceFlow>(SequenceFlow.class, this, MetamodelPackage.GATEWAY__OUTGOING, MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF);
    }
    return outgoing;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Lane getLane()
  {
    if (eContainerFeatureID() != MetamodelPackage.GATEWAY__LANE) return null;
    return (Lane)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLane(Lane newLane, NotificationChain msgs)
  {
    msgs = eBasicSetContainer((InternalEObject)newLane, MetamodelPackage.GATEWAY__LANE, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLane(Lane newLane)
  {
    if (newLane != eInternalContainer() || (eContainerFeatureID() != MetamodelPackage.GATEWAY__LANE && newLane != null))
    {
      if (EcoreUtil.isAncestor(this, newLane))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newLane != null)
        msgs = ((InternalEObject)newLane).eInverseAdd(this, MetamodelPackage.LANE__PROCESS_NODE_REFS, Lane.class, msgs);
      msgs = basicSetLane(newLane, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.GATEWAY__LANE, newLane, newLane));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GatewayType getGatewayType()
  {
    return gatewayType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setGatewayType(GatewayType newGatewayType)
  {
    GatewayType oldGatewayType = gatewayType;
    gatewayType = newGatewayType == null ? GATEWAY_TYPE_EDEFAULT : newGatewayType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.GATEWAY__GATEWAY_TYPE, oldGatewayType, gatewayType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MetamodelPackage.GATEWAY__INCOMING:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncoming()).basicAdd(otherEnd, msgs);
      case MetamodelPackage.GATEWAY__OUTGOING:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoing()).basicAdd(otherEnd, msgs);
      case MetamodelPackage.GATEWAY__LANE:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetLane((Lane)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MetamodelPackage.GATEWAY__INCOMING:
        return ((InternalEList<?>)getIncoming()).basicRemove(otherEnd, msgs);
      case MetamodelPackage.GATEWAY__OUTGOING:
        return ((InternalEList<?>)getOutgoing()).basicRemove(otherEnd, msgs);
      case MetamodelPackage.GATEWAY__LANE:
        return basicSetLane(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
  {
    switch (eContainerFeatureID())
    {
      case MetamodelPackage.GATEWAY__LANE:
        return eInternalContainer().eInverseRemove(this, MetamodelPackage.LANE__PROCESS_NODE_REFS, Lane.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MetamodelPackage.GATEWAY__NAME:
        return getName();
      case MetamodelPackage.GATEWAY__DESCRIPTION:
        return getDescription();
      case MetamodelPackage.GATEWAY__INCOMING:
        return getIncoming();
      case MetamodelPackage.GATEWAY__OUTGOING:
        return getOutgoing();
      case MetamodelPackage.GATEWAY__LANE:
        return getLane();
      case MetamodelPackage.GATEWAY__GATEWAY_TYPE:
        return getGatewayType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MetamodelPackage.GATEWAY__NAME:
        setName((String)newValue);
        return;
      case MetamodelPackage.GATEWAY__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case MetamodelPackage.GATEWAY__INCOMING:
        getIncoming().clear();
        getIncoming().addAll((Collection<? extends SequenceFlow>)newValue);
        return;
      case MetamodelPackage.GATEWAY__OUTGOING:
        getOutgoing().clear();
        getOutgoing().addAll((Collection<? extends SequenceFlow>)newValue);
        return;
      case MetamodelPackage.GATEWAY__LANE:
        setLane((Lane)newValue);
        return;
      case MetamodelPackage.GATEWAY__GATEWAY_TYPE:
        setGatewayType((GatewayType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MetamodelPackage.GATEWAY__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MetamodelPackage.GATEWAY__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case MetamodelPackage.GATEWAY__INCOMING:
        getIncoming().clear();
        return;
      case MetamodelPackage.GATEWAY__OUTGOING:
        getOutgoing().clear();
        return;
      case MetamodelPackage.GATEWAY__LANE:
        setLane((Lane)null);
        return;
      case MetamodelPackage.GATEWAY__GATEWAY_TYPE:
        setGatewayType(GATEWAY_TYPE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MetamodelPackage.GATEWAY__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MetamodelPackage.GATEWAY__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case MetamodelPackage.GATEWAY__INCOMING:
        return incoming != null && !incoming.isEmpty();
      case MetamodelPackage.GATEWAY__OUTGOING:
        return outgoing != null && !outgoing.isEmpty();
      case MetamodelPackage.GATEWAY__LANE:
        return getLane() != null;
      case MetamodelPackage.GATEWAY__GATEWAY_TYPE:
        return gatewayType != GATEWAY_TYPE_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", description: ");
    result.append(description);
    result.append(", gatewayType: ");
    result.append(gatewayType);
    result.append(')');
    return result.toString();
  }

} //GatewayImpl
