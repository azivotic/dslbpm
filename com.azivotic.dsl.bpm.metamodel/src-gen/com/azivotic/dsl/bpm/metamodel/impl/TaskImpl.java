/**
 */
package com.azivotic.dsl.bpm.metamodel.impl;

import com.azivotic.dsl.bpm.metamodel.Lane;
import com.azivotic.dsl.bpm.metamodel.MetamodelPackage;
import com.azivotic.dsl.bpm.metamodel.SequenceFlow;
import com.azivotic.dsl.bpm.metamodel.Task;
import com.azivotic.dsl.bpm.metamodel.TaskExecutionType;
import com.azivotic.dsl.bpm.metamodel.TaskType;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl#getLane <em>Lane</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl#getAssignee <em>Assignee</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl#getTaskType <em>Task Type</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl#getTaskExecutionType <em>Task Execution Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TaskImpl extends MinimalEObjectImpl.Container implements Task
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getIncoming() <em>Incoming</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIncoming()
   * @generated
   * @ordered
   */
  protected EList<SequenceFlow> incoming;

  /**
   * The cached value of the '{@link #getOutgoing() <em>Outgoing</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutgoing()
   * @generated
   * @ordered
   */
  protected EList<SequenceFlow> outgoing;

  /**
   * The default value of the '{@link #getAssignee() <em>Assignee</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssignee()
   * @generated
   * @ordered
   */
  protected static final String ASSIGNEE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAssignee() <em>Assignee</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssignee()
   * @generated
   * @ordered
   */
  protected String assignee = ASSIGNEE_EDEFAULT;

  /**
   * The default value of the '{@link #getTaskType() <em>Task Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTaskType()
   * @generated
   * @ordered
   */
  protected static final TaskType TASK_TYPE_EDEFAULT = TaskType.DEFAULT;

  /**
   * The cached value of the '{@link #getTaskType() <em>Task Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTaskType()
   * @generated
   * @ordered
   */
  protected TaskType taskType = TASK_TYPE_EDEFAULT;

  /**
   * The default value of the '{@link #getTaskExecutionType() <em>Task Execution Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTaskExecutionType()
   * @generated
   * @ordered
   */
  protected static final TaskExecutionType TASK_EXECUTION_TYPE_EDEFAULT = TaskExecutionType.DEFAULT;

  /**
   * The cached value of the '{@link #getTaskExecutionType() <em>Task Execution Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTaskExecutionType()
   * @generated
   * @ordered
   */
  protected TaskExecutionType taskExecutionType = TASK_EXECUTION_TYPE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TaskImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MetamodelPackage.Literals.TASK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.TASK__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription)
  {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.TASK__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SequenceFlow> getIncoming()
  {
    if (incoming == null)
    {
      incoming = new EObjectWithInverseResolvingEList<SequenceFlow>(SequenceFlow.class, this, MetamodelPackage.TASK__INCOMING, MetamodelPackage.SEQUENCE_FLOW__TARGET_REF);
    }
    return incoming;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SequenceFlow> getOutgoing()
  {
    if (outgoing == null)
    {
      outgoing = new EObjectWithInverseResolvingEList<SequenceFlow>(SequenceFlow.class, this, MetamodelPackage.TASK__OUTGOING, MetamodelPackage.SEQUENCE_FLOW__SOURCE_REF);
    }
    return outgoing;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Lane getLane()
  {
    if (eContainerFeatureID() != MetamodelPackage.TASK__LANE) return null;
    return (Lane)eInternalContainer();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLane(Lane newLane, NotificationChain msgs)
  {
    msgs = eBasicSetContainer((InternalEObject)newLane, MetamodelPackage.TASK__LANE, msgs);
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLane(Lane newLane)
  {
    if (newLane != eInternalContainer() || (eContainerFeatureID() != MetamodelPackage.TASK__LANE && newLane != null))
    {
      if (EcoreUtil.isAncestor(this, newLane))
        throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
      NotificationChain msgs = null;
      if (eInternalContainer() != null)
        msgs = eBasicRemoveFromContainer(msgs);
      if (newLane != null)
        msgs = ((InternalEObject)newLane).eInverseAdd(this, MetamodelPackage.LANE__PROCESS_NODE_REFS, Lane.class, msgs);
      msgs = basicSetLane(newLane, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.TASK__LANE, newLane, newLane));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAssignee()
  {
    return assignee;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAssignee(String newAssignee)
  {
    String oldAssignee = assignee;
    assignee = newAssignee;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.TASK__ASSIGNEE, oldAssignee, assignee));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TaskType getTaskType()
  {
    return taskType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTaskType(TaskType newTaskType)
  {
    TaskType oldTaskType = taskType;
    taskType = newTaskType == null ? TASK_TYPE_EDEFAULT : newTaskType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.TASK__TASK_TYPE, oldTaskType, taskType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TaskExecutionType getTaskExecutionType()
  {
    return taskExecutionType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTaskExecutionType(TaskExecutionType newTaskExecutionType)
  {
    TaskExecutionType oldTaskExecutionType = taskExecutionType;
    taskExecutionType = newTaskExecutionType == null ? TASK_EXECUTION_TYPE_EDEFAULT : newTaskExecutionType;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.TASK__TASK_EXECUTION_TYPE, oldTaskExecutionType, taskExecutionType));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MetamodelPackage.TASK__INCOMING:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncoming()).basicAdd(otherEnd, msgs);
      case MetamodelPackage.TASK__OUTGOING:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoing()).basicAdd(otherEnd, msgs);
      case MetamodelPackage.TASK__LANE:
        if (eInternalContainer() != null)
          msgs = eBasicRemoveFromContainer(msgs);
        return basicSetLane((Lane)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MetamodelPackage.TASK__INCOMING:
        return ((InternalEList<?>)getIncoming()).basicRemove(otherEnd, msgs);
      case MetamodelPackage.TASK__OUTGOING:
        return ((InternalEList<?>)getOutgoing()).basicRemove(otherEnd, msgs);
      case MetamodelPackage.TASK__LANE:
        return basicSetLane(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs)
  {
    switch (eContainerFeatureID())
    {
      case MetamodelPackage.TASK__LANE:
        return eInternalContainer().eInverseRemove(this, MetamodelPackage.LANE__PROCESS_NODE_REFS, Lane.class, msgs);
    }
    return super.eBasicRemoveFromContainerFeature(msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MetamodelPackage.TASK__NAME:
        return getName();
      case MetamodelPackage.TASK__DESCRIPTION:
        return getDescription();
      case MetamodelPackage.TASK__INCOMING:
        return getIncoming();
      case MetamodelPackage.TASK__OUTGOING:
        return getOutgoing();
      case MetamodelPackage.TASK__LANE:
        return getLane();
      case MetamodelPackage.TASK__ASSIGNEE:
        return getAssignee();
      case MetamodelPackage.TASK__TASK_TYPE:
        return getTaskType();
      case MetamodelPackage.TASK__TASK_EXECUTION_TYPE:
        return getTaskExecutionType();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MetamodelPackage.TASK__NAME:
        setName((String)newValue);
        return;
      case MetamodelPackage.TASK__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case MetamodelPackage.TASK__INCOMING:
        getIncoming().clear();
        getIncoming().addAll((Collection<? extends SequenceFlow>)newValue);
        return;
      case MetamodelPackage.TASK__OUTGOING:
        getOutgoing().clear();
        getOutgoing().addAll((Collection<? extends SequenceFlow>)newValue);
        return;
      case MetamodelPackage.TASK__LANE:
        setLane((Lane)newValue);
        return;
      case MetamodelPackage.TASK__ASSIGNEE:
        setAssignee((String)newValue);
        return;
      case MetamodelPackage.TASK__TASK_TYPE:
        setTaskType((TaskType)newValue);
        return;
      case MetamodelPackage.TASK__TASK_EXECUTION_TYPE:
        setTaskExecutionType((TaskExecutionType)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MetamodelPackage.TASK__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MetamodelPackage.TASK__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case MetamodelPackage.TASK__INCOMING:
        getIncoming().clear();
        return;
      case MetamodelPackage.TASK__OUTGOING:
        getOutgoing().clear();
        return;
      case MetamodelPackage.TASK__LANE:
        setLane((Lane)null);
        return;
      case MetamodelPackage.TASK__ASSIGNEE:
        setAssignee(ASSIGNEE_EDEFAULT);
        return;
      case MetamodelPackage.TASK__TASK_TYPE:
        setTaskType(TASK_TYPE_EDEFAULT);
        return;
      case MetamodelPackage.TASK__TASK_EXECUTION_TYPE:
        setTaskExecutionType(TASK_EXECUTION_TYPE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MetamodelPackage.TASK__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MetamodelPackage.TASK__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case MetamodelPackage.TASK__INCOMING:
        return incoming != null && !incoming.isEmpty();
      case MetamodelPackage.TASK__OUTGOING:
        return outgoing != null && !outgoing.isEmpty();
      case MetamodelPackage.TASK__LANE:
        return getLane() != null;
      case MetamodelPackage.TASK__ASSIGNEE:
        return ASSIGNEE_EDEFAULT == null ? assignee != null : !ASSIGNEE_EDEFAULT.equals(assignee);
      case MetamodelPackage.TASK__TASK_TYPE:
        return taskType != TASK_TYPE_EDEFAULT;
      case MetamodelPackage.TASK__TASK_EXECUTION_TYPE:
        return taskExecutionType != TASK_EXECUTION_TYPE_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", description: ");
    result.append(description);
    result.append(", assignee: ");
    result.append(assignee);
    result.append(", taskType: ");
    result.append(taskType);
    result.append(", taskExecutionType: ");
    result.append(taskExecutionType);
    result.append(')');
    return result.toString();
  }

} //TaskImpl
