/**
 */
package com.azivotic.dsl.bpm.metamodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.azivotic.dsl.bpm'"
 * @generated
 */
public interface MetamodelPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "metamodel";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "com.azivotic.dsl.bpm.metamodel";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "metamodel";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MetamodelPackage eINSTANCE = com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl.init();

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement <em>Graphical Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.GraphicalElement
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getGraphicalElement()
   * @generated
   */
  int GRAPHICAL_ELEMENT = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPHICAL_ELEMENT__NAME = 0;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPHICAL_ELEMENT__DESCRIPTION = 1;

  /**
   * The number of structural features of the '<em>Graphical Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPHICAL_ELEMENT_FEATURE_COUNT = 2;

  /**
   * The number of operations of the '<em>Graphical Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GRAPHICAL_ELEMENT_OPERATION_COUNT = 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.impl.LaneImpl <em>Lane</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.impl.LaneImpl
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getLane()
   * @generated
   */
  int LANE = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LANE__NAME = GRAPHICAL_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LANE__DESCRIPTION = GRAPHICAL_ELEMENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Process Node Refs</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LANE__PROCESS_NODE_REFS = GRAPHICAL_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Lane</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LANE_FEATURE_COUNT = GRAPHICAL_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Lane</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LANE_OPERATION_COUNT = GRAPHICAL_ELEMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode <em>Process Node</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getProcessNode()
   * @generated
   */
  int PROCESS_NODE = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_NODE__NAME = GRAPHICAL_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_NODE__DESCRIPTION = GRAPHICAL_ELEMENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Incoming</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_NODE__INCOMING = GRAPHICAL_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Outgoing</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_NODE__OUTGOING = GRAPHICAL_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Lane</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_NODE__LANE = GRAPHICAL_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Process Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_NODE_FEATURE_COUNT = GRAPHICAL_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>Process Node</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROCESS_NODE_OPERATION_COUNT = GRAPHICAL_ELEMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl <em>Sequence Flow</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getSequenceFlow()
   * @generated
   */
  int SEQUENCE_FLOW = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FLOW__NAME = GRAPHICAL_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FLOW__DESCRIPTION = GRAPHICAL_ELEMENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Source Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FLOW__SOURCE_REF = GRAPHICAL_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Target Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FLOW__TARGET_REF = GRAPHICAL_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Default Line</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FLOW__DEFAULT_LINE = GRAPHICAL_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Condition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FLOW__CONDITION = GRAPHICAL_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Sequence Flow</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FLOW_FEATURE_COUNT = GRAPHICAL_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The number of operations of the '<em>Sequence Flow</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCE_FLOW_OPERATION_COUNT = GRAPHICAL_ELEMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.Activity <em>Activity</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.Activity
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getActivity()
   * @generated
   */
  int ACTIVITY = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVITY__NAME = PROCESS_NODE__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVITY__DESCRIPTION = PROCESS_NODE__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Incoming</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVITY__INCOMING = PROCESS_NODE__INCOMING;

  /**
   * The feature id for the '<em><b>Outgoing</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVITY__OUTGOING = PROCESS_NODE__OUTGOING;

  /**
   * The feature id for the '<em><b>Lane</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVITY__LANE = PROCESS_NODE__LANE;

  /**
   * The number of structural features of the '<em>Activity</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVITY_FEATURE_COUNT = PROCESS_NODE_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Activity</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ACTIVITY_OPERATION_COUNT = PROCESS_NODE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.Event <em>Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.Event
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getEvent()
   * @generated
   */
  int EVENT = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__NAME = PROCESS_NODE__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__DESCRIPTION = PROCESS_NODE__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Incoming</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__INCOMING = PROCESS_NODE__INCOMING;

  /**
   * The feature id for the '<em><b>Outgoing</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__OUTGOING = PROCESS_NODE__OUTGOING;

  /**
   * The feature id for the '<em><b>Lane</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT__LANE = PROCESS_NODE__LANE;

  /**
   * The number of structural features of the '<em>Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_FEATURE_COUNT = PROCESS_NODE_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EVENT_OPERATION_COUNT = PROCESS_NODE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl <em>Gateway</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getGateway()
   * @generated
   */
  int GATEWAY = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GATEWAY__NAME = PROCESS_NODE__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GATEWAY__DESCRIPTION = PROCESS_NODE__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Incoming</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GATEWAY__INCOMING = PROCESS_NODE__INCOMING;

  /**
   * The feature id for the '<em><b>Outgoing</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GATEWAY__OUTGOING = PROCESS_NODE__OUTGOING;

  /**
   * The feature id for the '<em><b>Lane</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GATEWAY__LANE = PROCESS_NODE__LANE;

  /**
   * The feature id for the '<em><b>Gateway Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GATEWAY__GATEWAY_TYPE = PROCESS_NODE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Gateway</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GATEWAY_FEATURE_COUNT = PROCESS_NODE_FEATURE_COUNT + 1;

  /**
   * The number of operations of the '<em>Gateway</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GATEWAY_OPERATION_COUNT = PROCESS_NODE_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl <em>Start Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getStartEvent()
   * @generated
   */
  int START_EVENT = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_EVENT__NAME = EVENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_EVENT__DESCRIPTION = EVENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Incoming</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_EVENT__INCOMING = EVENT__INCOMING;

  /**
   * The feature id for the '<em><b>Outgoing</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_EVENT__OUTGOING = EVENT__OUTGOING;

  /**
   * The feature id for the '<em><b>Lane</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_EVENT__LANE = EVENT__LANE;

  /**
   * The number of structural features of the '<em>Start Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Start Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int START_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.impl.EndEventImpl <em>End Event</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.impl.EndEventImpl
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getEndEvent()
   * @generated
   */
  int END_EVENT = 8;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_EVENT__NAME = EVENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_EVENT__DESCRIPTION = EVENT__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Incoming</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_EVENT__INCOMING = EVENT__INCOMING;

  /**
   * The feature id for the '<em><b>Outgoing</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_EVENT__OUTGOING = EVENT__OUTGOING;

  /**
   * The feature id for the '<em><b>Lane</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_EVENT__LANE = EVENT__LANE;

  /**
   * The number of structural features of the '<em>End Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>End Event</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int END_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl <em>Task</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.impl.TaskImpl
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getTask()
   * @generated
   */
  int TASK = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__NAME = ACTIVITY__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__DESCRIPTION = ACTIVITY__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Incoming</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__INCOMING = ACTIVITY__INCOMING;

  /**
   * The feature id for the '<em><b>Outgoing</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__OUTGOING = ACTIVITY__OUTGOING;

  /**
   * The feature id for the '<em><b>Lane</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__LANE = ACTIVITY__LANE;

  /**
   * The feature id for the '<em><b>Assignee</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__ASSIGNEE = ACTIVITY_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Task Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__TASK_TYPE = ACTIVITY_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Task Execution Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__TASK_EXECUTION_TYPE = ACTIVITY_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Task</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK_FEATURE_COUNT = ACTIVITY_FEATURE_COUNT + 3;

  /**
   * The number of operations of the '<em>Task</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK_OPERATION_COUNT = ACTIVITY_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.impl.CommentImpl <em>Comment</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.impl.CommentImpl
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getComment()
   * @generated
   */
  int COMMENT = 10;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENT__NAME = GRAPHICAL_ELEMENT__NAME;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENT__DESCRIPTION = GRAPHICAL_ELEMENT__DESCRIPTION;

  /**
   * The number of structural features of the '<em>Comment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENT_FEATURE_COUNT = GRAPHICAL_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of operations of the '<em>Comment</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMENT_OPERATION_COUNT = GRAPHICAL_ELEMENT_OPERATION_COUNT + 0;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.GatewayType <em>Gateway Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.GatewayType
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getGatewayType()
   * @generated
   */
  int GATEWAY_TYPE = 11;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.TaskType <em>Task Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.TaskType
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getTaskType()
   * @generated
   */
  int TASK_TYPE = 12;

  /**
   * The meta object id for the '{@link com.azivotic.dsl.bpm.metamodel.TaskExecutionType <em>Task Execution Type</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.azivotic.dsl.bpm.metamodel.TaskExecutionType
   * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getTaskExecutionType()
   * @generated
   */
  int TASK_EXECUTION_TYPE = 13;


  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement <em>Graphical Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Graphical Element</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.GraphicalElement
   * @generated
   */
  EClass getGraphicalElement();

  /**
   * Returns the meta object for the attribute '{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.GraphicalElement#getName()
   * @see #getGraphicalElement()
   * @generated
   */
  EAttribute getGraphicalElement_Name();

  /**
   * Returns the meta object for the attribute '{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.GraphicalElement#getDescription()
   * @see #getGraphicalElement()
   * @generated
   */
  EAttribute getGraphicalElement_Description();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.Lane <em>Lane</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Lane</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Lane
   * @generated
   */
  EClass getLane();

  /**
   * Returns the meta object for the containment reference list '{@link com.azivotic.dsl.bpm.metamodel.Lane#getProcessNodeRefs <em>Process Node Refs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Process Node Refs</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Lane#getProcessNodeRefs()
   * @see #getLane()
   * @generated
   */
  EReference getLane_ProcessNodeRefs();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode <em>Process Node</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Process Node</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode
   * @generated
   */
  EClass getProcessNode();

  /**
   * Returns the meta object for the reference list '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getIncoming <em>Incoming</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Incoming</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode#getIncoming()
   * @see #getProcessNode()
   * @generated
   */
  EReference getProcessNode_Incoming();

  /**
   * Returns the meta object for the reference list '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getOutgoing <em>Outgoing</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Outgoing</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode#getOutgoing()
   * @see #getProcessNode()
   * @generated
   */
  EReference getProcessNode_Outgoing();

  /**
   * Returns the meta object for the container reference '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getLane <em>Lane</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the container reference '<em>Lane</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode#getLane()
   * @see #getProcessNode()
   * @generated
   */
  EReference getProcessNode_Lane();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow <em>Sequence Flow</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sequence Flow</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.SequenceFlow
   * @generated
   */
  EClass getSequenceFlow();

  /**
   * Returns the meta object for the reference '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getSourceRef <em>Source Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Source Ref</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.SequenceFlow#getSourceRef()
   * @see #getSequenceFlow()
   * @generated
   */
  EReference getSequenceFlow_SourceRef();

  /**
   * Returns the meta object for the reference '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getTargetRef <em>Target Ref</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Target Ref</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.SequenceFlow#getTargetRef()
   * @see #getSequenceFlow()
   * @generated
   */
  EReference getSequenceFlow_TargetRef();

  /**
   * Returns the meta object for the attribute '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#isDefaultLine <em>Default Line</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Default Line</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.SequenceFlow#isDefaultLine()
   * @see #getSequenceFlow()
   * @generated
   */
  EAttribute getSequenceFlow_DefaultLine();

  /**
   * Returns the meta object for the attribute '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getCondition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Condition</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.SequenceFlow#getCondition()
   * @see #getSequenceFlow()
   * @generated
   */
  EAttribute getSequenceFlow_Condition();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.Activity <em>Activity</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Activity</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Activity
   * @generated
   */
  EClass getActivity();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.Event <em>Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Event</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Event
   * @generated
   */
  EClass getEvent();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.Gateway <em>Gateway</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Gateway</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Gateway
   * @generated
   */
  EClass getGateway();

  /**
   * Returns the meta object for the attribute '{@link com.azivotic.dsl.bpm.metamodel.Gateway#getGatewayType <em>Gateway Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Gateway Type</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Gateway#getGatewayType()
   * @see #getGateway()
   * @generated
   */
  EAttribute getGateway_GatewayType();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.StartEvent <em>Start Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Start Event</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.StartEvent
   * @generated
   */
  EClass getStartEvent();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.EndEvent <em>End Event</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>End Event</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.EndEvent
   * @generated
   */
  EClass getEndEvent();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.Task <em>Task</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Task</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Task
   * @generated
   */
  EClass getTask();

  /**
   * Returns the meta object for the attribute '{@link com.azivotic.dsl.bpm.metamodel.Task#getAssignee <em>Assignee</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Assignee</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Task#getAssignee()
   * @see #getTask()
   * @generated
   */
  EAttribute getTask_Assignee();

  /**
   * Returns the meta object for the attribute '{@link com.azivotic.dsl.bpm.metamodel.Task#getTaskType <em>Task Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Task Type</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Task#getTaskType()
   * @see #getTask()
   * @generated
   */
  EAttribute getTask_TaskType();

  /**
   * Returns the meta object for the attribute '{@link com.azivotic.dsl.bpm.metamodel.Task#getTaskExecutionType <em>Task Execution Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Task Execution Type</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Task#getTaskExecutionType()
   * @see #getTask()
   * @generated
   */
  EAttribute getTask_TaskExecutionType();

  /**
   * Returns the meta object for class '{@link com.azivotic.dsl.bpm.metamodel.Comment <em>Comment</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Comment</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.Comment
   * @generated
   */
  EClass getComment();

  /**
   * Returns the meta object for enum '{@link com.azivotic.dsl.bpm.metamodel.GatewayType <em>Gateway Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Gateway Type</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.GatewayType
   * @generated
   */
  EEnum getGatewayType();

  /**
   * Returns the meta object for enum '{@link com.azivotic.dsl.bpm.metamodel.TaskType <em>Task Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Task Type</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.TaskType
   * @generated
   */
  EEnum getTaskType();

  /**
   * Returns the meta object for enum '{@link com.azivotic.dsl.bpm.metamodel.TaskExecutionType <em>Task Execution Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Task Execution Type</em>'.
   * @see com.azivotic.dsl.bpm.metamodel.TaskExecutionType
   * @generated
   */
  EEnum getTaskExecutionType();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  MetamodelFactory getMetamodelFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement <em>Graphical Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.GraphicalElement
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getGraphicalElement()
     * @generated
     */
    EClass GRAPHICAL_ELEMENT = eINSTANCE.getGraphicalElement();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GRAPHICAL_ELEMENT__NAME = eINSTANCE.getGraphicalElement_Name();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GRAPHICAL_ELEMENT__DESCRIPTION = eINSTANCE.getGraphicalElement_Description();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.impl.LaneImpl <em>Lane</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.impl.LaneImpl
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getLane()
     * @generated
     */
    EClass LANE = eINSTANCE.getLane();

    /**
     * The meta object literal for the '<em><b>Process Node Refs</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LANE__PROCESS_NODE_REFS = eINSTANCE.getLane_ProcessNodeRefs();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode <em>Process Node</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.ProcessNode
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getProcessNode()
     * @generated
     */
    EClass PROCESS_NODE = eINSTANCE.getProcessNode();

    /**
     * The meta object literal for the '<em><b>Incoming</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROCESS_NODE__INCOMING = eINSTANCE.getProcessNode_Incoming();

    /**
     * The meta object literal for the '<em><b>Outgoing</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROCESS_NODE__OUTGOING = eINSTANCE.getProcessNode_Outgoing();

    /**
     * The meta object literal for the '<em><b>Lane</b></em>' container reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PROCESS_NODE__LANE = eINSTANCE.getProcessNode_Lane();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl <em>Sequence Flow</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.impl.SequenceFlowImpl
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getSequenceFlow()
     * @generated
     */
    EClass SEQUENCE_FLOW = eINSTANCE.getSequenceFlow();

    /**
     * The meta object literal for the '<em><b>Source Ref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEQUENCE_FLOW__SOURCE_REF = eINSTANCE.getSequenceFlow_SourceRef();

    /**
     * The meta object literal for the '<em><b>Target Ref</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEQUENCE_FLOW__TARGET_REF = eINSTANCE.getSequenceFlow_TargetRef();

    /**
     * The meta object literal for the '<em><b>Default Line</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SEQUENCE_FLOW__DEFAULT_LINE = eINSTANCE.getSequenceFlow_DefaultLine();

    /**
     * The meta object literal for the '<em><b>Condition</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SEQUENCE_FLOW__CONDITION = eINSTANCE.getSequenceFlow_Condition();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.Activity <em>Activity</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.Activity
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getActivity()
     * @generated
     */
    EClass ACTIVITY = eINSTANCE.getActivity();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.Event <em>Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.Event
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getEvent()
     * @generated
     */
    EClass EVENT = eINSTANCE.getEvent();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl <em>Gateway</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.impl.GatewayImpl
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getGateway()
     * @generated
     */
    EClass GATEWAY = eINSTANCE.getGateway();

    /**
     * The meta object literal for the '<em><b>Gateway Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GATEWAY__GATEWAY_TYPE = eINSTANCE.getGateway_GatewayType();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl <em>Start Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.impl.StartEventImpl
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getStartEvent()
     * @generated
     */
    EClass START_EVENT = eINSTANCE.getStartEvent();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.impl.EndEventImpl <em>End Event</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.impl.EndEventImpl
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getEndEvent()
     * @generated
     */
    EClass END_EVENT = eINSTANCE.getEndEvent();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.impl.TaskImpl <em>Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.impl.TaskImpl
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getTask()
     * @generated
     */
    EClass TASK = eINSTANCE.getTask();

    /**
     * The meta object literal for the '<em><b>Assignee</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TASK__ASSIGNEE = eINSTANCE.getTask_Assignee();

    /**
     * The meta object literal for the '<em><b>Task Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TASK__TASK_TYPE = eINSTANCE.getTask_TaskType();

    /**
     * The meta object literal for the '<em><b>Task Execution Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TASK__TASK_EXECUTION_TYPE = eINSTANCE.getTask_TaskExecutionType();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.impl.CommentImpl <em>Comment</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.impl.CommentImpl
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getComment()
     * @generated
     */
    EClass COMMENT = eINSTANCE.getComment();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.GatewayType <em>Gateway Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.GatewayType
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getGatewayType()
     * @generated
     */
    EEnum GATEWAY_TYPE = eINSTANCE.getGatewayType();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.TaskType <em>Task Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.TaskType
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getTaskType()
     * @generated
     */
    EEnum TASK_TYPE = eINSTANCE.getTaskType();

    /**
     * The meta object literal for the '{@link com.azivotic.dsl.bpm.metamodel.TaskExecutionType <em>Task Execution Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.azivotic.dsl.bpm.metamodel.TaskExecutionType
     * @see com.azivotic.dsl.bpm.metamodel.impl.MetamodelPackageImpl#getTaskExecutionType()
     * @generated
     */
    EEnum TASK_EXECUTION_TYPE = eINSTANCE.getTaskExecutionType();

  }

} //MetamodelPackage
