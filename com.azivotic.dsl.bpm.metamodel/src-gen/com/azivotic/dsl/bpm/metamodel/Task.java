/**
 */
package com.azivotic.dsl.bpm.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.Task#getAssignee <em>Assignee</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.Task#getTaskType <em>Task Type</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.Task#getTaskExecutionType <em>Task Execution Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getTask()
 * @model
 * @generated
 */
public interface Task extends Activity
{
  /**
   * Returns the value of the '<em><b>Assignee</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Assignee</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Assignee</em>' attribute.
   * @see #setAssignee(String)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getTask_Assignee()
   * @model unique="false"
   * @generated
   */
  String getAssignee();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.Task#getAssignee <em>Assignee</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Assignee</em>' attribute.
   * @see #getAssignee()
   * @generated
   */
  void setAssignee(String value);

  /**
   * Returns the value of the '<em><b>Task Type</b></em>' attribute.
   * The literals are from the enumeration {@link com.azivotic.dsl.bpm.metamodel.TaskType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Task Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Task Type</em>' attribute.
   * @see com.azivotic.dsl.bpm.metamodel.TaskType
   * @see #setTaskType(TaskType)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getTask_TaskType()
   * @model unique="false"
   * @generated
   */
  TaskType getTaskType();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.Task#getTaskType <em>Task Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Task Type</em>' attribute.
   * @see com.azivotic.dsl.bpm.metamodel.TaskType
   * @see #getTaskType()
   * @generated
   */
  void setTaskType(TaskType value);

  /**
   * Returns the value of the '<em><b>Task Execution Type</b></em>' attribute.
   * The literals are from the enumeration {@link com.azivotic.dsl.bpm.metamodel.TaskExecutionType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Task Execution Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Task Execution Type</em>' attribute.
   * @see com.azivotic.dsl.bpm.metamodel.TaskExecutionType
   * @see #setTaskExecutionType(TaskExecutionType)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getTask_TaskExecutionType()
   * @model unique="false"
   * @generated
   */
  TaskExecutionType getTaskExecutionType();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.Task#getTaskExecutionType <em>Task Execution Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Task Execution Type</em>' attribute.
   * @see com.azivotic.dsl.bpm.metamodel.TaskExecutionType
   * @see #getTaskExecutionType()
   * @generated
   */
  void setTaskExecutionType(TaskExecutionType value);

} // Task
