/**
 */
package com.azivotic.dsl.bpm.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Start Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getStartEvent()
 * @model
 * @generated
 */
public interface StartEvent extends Event
{
} // StartEvent
