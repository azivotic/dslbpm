/**
 */
package com.azivotic.dsl.bpm.metamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graphical Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement#getName <em>Name</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement#getDescription <em>Description</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getGraphicalElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface GraphicalElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getGraphicalElement_Name()
   * @model unique="false"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' attribute.
   * @see #setDescription(String)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getGraphicalElement_Description()
   * @model unique="false"
   * @generated
   */
  String getDescription();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.GraphicalElement#getDescription <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' attribute.
   * @see #getDescription()
   * @generated
   */
  void setDescription(String value);

} // GraphicalElement
