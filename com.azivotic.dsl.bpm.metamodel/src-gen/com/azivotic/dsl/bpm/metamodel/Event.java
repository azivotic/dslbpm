/**
 */
package com.azivotic.dsl.bpm.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getEvent()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Event extends ProcessNode
{
} // Event
