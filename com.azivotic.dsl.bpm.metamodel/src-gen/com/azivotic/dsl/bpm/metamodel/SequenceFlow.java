/**
 */
package com.azivotic.dsl.bpm.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequence Flow</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getSourceRef <em>Source Ref</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getTargetRef <em>Target Ref</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#isDefaultLine <em>Default Line</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getCondition <em>Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getSequenceFlow()
 * @model
 * @generated
 */
public interface SequenceFlow extends GraphicalElement
{
  /**
   * Returns the value of the '<em><b>Source Ref</b></em>' reference.
   * It is bidirectional and its opposite is '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getOutgoing <em>Outgoing</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Source Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Source Ref</em>' reference.
   * @see #setSourceRef(ProcessNode)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getSequenceFlow_SourceRef()
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode#getOutgoing
   * @model opposite="outgoing"
   * @generated
   */
  ProcessNode getSourceRef();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getSourceRef <em>Source Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Source Ref</em>' reference.
   * @see #getSourceRef()
   * @generated
   */
  void setSourceRef(ProcessNode value);

  /**
   * Returns the value of the '<em><b>Target Ref</b></em>' reference.
   * It is bidirectional and its opposite is '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getIncoming <em>Incoming</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Target Ref</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Target Ref</em>' reference.
   * @see #setTargetRef(ProcessNode)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getSequenceFlow_TargetRef()
   * @see com.azivotic.dsl.bpm.metamodel.ProcessNode#getIncoming
   * @model opposite="incoming"
   * @generated
   */
  ProcessNode getTargetRef();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getTargetRef <em>Target Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Target Ref</em>' reference.
   * @see #getTargetRef()
   * @generated
   */
  void setTargetRef(ProcessNode value);

  /**
   * Returns the value of the '<em><b>Default Line</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Default Line</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Default Line</em>' attribute.
   * @see #setDefaultLine(boolean)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getSequenceFlow_DefaultLine()
   * @model unique="false"
   * @generated
   */
  boolean isDefaultLine();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#isDefaultLine <em>Default Line</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Default Line</em>' attribute.
   * @see #isDefaultLine()
   * @generated
   */
  void setDefaultLine(boolean value);

  /**
   * Returns the value of the '<em><b>Condition</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Condition</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Condition</em>' attribute.
   * @see #setCondition(String)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getSequenceFlow_Condition()
   * @model unique="false"
   * @generated
   */
  String getCondition();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getCondition <em>Condition</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Condition</em>' attribute.
   * @see #getCondition()
   * @generated
   */
  void setCondition(String value);

} // SequenceFlow
