/**
 */
package com.azivotic.dsl.bpm.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends GraphicalElement
{
} // Comment
