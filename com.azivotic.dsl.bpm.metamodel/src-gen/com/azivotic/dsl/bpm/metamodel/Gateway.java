/**
 */
package com.azivotic.dsl.bpm.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gateway</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.Gateway#getGatewayType <em>Gateway Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getGateway()
 * @model
 * @generated
 */
public interface Gateway extends ProcessNode
{
  /**
   * Returns the value of the '<em><b>Gateway Type</b></em>' attribute.
   * The literals are from the enumeration {@link com.azivotic.dsl.bpm.metamodel.GatewayType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Gateway Type</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Gateway Type</em>' attribute.
   * @see com.azivotic.dsl.bpm.metamodel.GatewayType
   * @see #setGatewayType(GatewayType)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getGateway_GatewayType()
   * @model unique="false"
   * @generated
   */
  GatewayType getGatewayType();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.Gateway#getGatewayType <em>Gateway Type</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Gateway Type</em>' attribute.
   * @see com.azivotic.dsl.bpm.metamodel.GatewayType
   * @see #getGatewayType()
   * @generated
   */
  void setGatewayType(GatewayType value);

} // Gateway
