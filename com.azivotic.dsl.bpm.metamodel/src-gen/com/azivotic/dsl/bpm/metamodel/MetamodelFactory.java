/**
 */
package com.azivotic.dsl.bpm.metamodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage
 * @generated
 */
public interface MetamodelFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MetamodelFactory eINSTANCE = com.azivotic.dsl.bpm.metamodel.impl.MetamodelFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Lane</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Lane</em>'.
   * @generated
   */
  Lane createLane();

  /**
   * Returns a new object of class '<em>Sequence Flow</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sequence Flow</em>'.
   * @generated
   */
  SequenceFlow createSequenceFlow();

  /**
   * Returns a new object of class '<em>Gateway</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Gateway</em>'.
   * @generated
   */
  Gateway createGateway();

  /**
   * Returns a new object of class '<em>Start Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Start Event</em>'.
   * @generated
   */
  StartEvent createStartEvent();

  /**
   * Returns a new object of class '<em>End Event</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>End Event</em>'.
   * @generated
   */
  EndEvent createEndEvent();

  /**
   * Returns a new object of class '<em>Task</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Task</em>'.
   * @generated
   */
  Task createTask();

  /**
   * Returns a new object of class '<em>Comment</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Comment</em>'.
   * @generated
   */
  Comment createComment();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  MetamodelPackage getMetamodelPackage();

} //MetamodelFactory
