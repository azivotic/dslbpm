/**
 */
package com.azivotic.dsl.bpm.metamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getLane <em>Lane</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getProcessNode()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ProcessNode extends GraphicalElement
{
  /**
   * Returns the value of the '<em><b>Incoming</b></em>' reference list.
   * The list contents are of type {@link com.azivotic.dsl.bpm.metamodel.SequenceFlow}.
   * It is bidirectional and its opposite is '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getTargetRef <em>Target Ref</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Incoming</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Incoming</em>' reference list.
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getProcessNode_Incoming()
   * @see com.azivotic.dsl.bpm.metamodel.SequenceFlow#getTargetRef
   * @model opposite="targetRef"
   * @generated
   */
  EList<SequenceFlow> getIncoming();

  /**
   * Returns the value of the '<em><b>Outgoing</b></em>' reference list.
   * The list contents are of type {@link com.azivotic.dsl.bpm.metamodel.SequenceFlow}.
   * It is bidirectional and its opposite is '{@link com.azivotic.dsl.bpm.metamodel.SequenceFlow#getSourceRef <em>Source Ref</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Outgoing</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Outgoing</em>' reference list.
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getProcessNode_Outgoing()
   * @see com.azivotic.dsl.bpm.metamodel.SequenceFlow#getSourceRef
   * @model opposite="sourceRef"
   * @generated
   */
  EList<SequenceFlow> getOutgoing();

  /**
   * Returns the value of the '<em><b>Lane</b></em>' container reference.
   * It is bidirectional and its opposite is '{@link com.azivotic.dsl.bpm.metamodel.Lane#getProcessNodeRefs <em>Process Node Refs</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lane</em>' container reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lane</em>' container reference.
   * @see #setLane(Lane)
   * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getProcessNode_Lane()
   * @see com.azivotic.dsl.bpm.metamodel.Lane#getProcessNodeRefs
   * @model opposite="processNodeRefs" transient="false"
   * @generated
   */
  Lane getLane();

  /**
   * Sets the value of the '{@link com.azivotic.dsl.bpm.metamodel.ProcessNode#getLane <em>Lane</em>}' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lane</em>' container reference.
   * @see #getLane()
   * @generated
   */
  void setLane(Lane value);

} // ProcessNode
