/**
 */
package com.azivotic.dsl.bpm.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.azivotic.dsl.bpm.metamodel.MetamodelPackage#getActivity()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Activity extends ProcessNode
{
} // Activity
